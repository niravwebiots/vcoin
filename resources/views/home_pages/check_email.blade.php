<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="universal admin is super flexible, powerful, clean & modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, universal admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="{{ URL::asset('/assets/home/images/favicon.png') }}" type="image/x-icon"/>
    <link rel="shortcut icon" href="{{ URL::asset('/assets/home/images/favicon.png') }}" type="image/x-icon"/>
    <title> Vancoin | Forgot Password </title>

    <!--Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/back/css/fontawesome.css') }}">
    <!-- iconfont -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/back/css/icofont.css') }}">
    <!-- Themify icon -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/back/css/themify.css') }}">
    <!-- Flag icon -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/back/css/flag-icon.css') }}">
    <!-- Owl css -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/back/css/owlcarousel.css') }}">
    <!-- Bootstrap css -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/back/css/bootstrap.css') }}">
    <!-- App css -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/back/css/style.css') }}">
    <!-- Responsive css -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/back/css/responsive.css') }}">
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">

</head>

<body>

<!--page-wrapper Start-->
<div class="page-wrapper">
    <div class="container-fluid">
        <!--sign up page start-->
        <div class="authentication-main">
            <div class="row">
                <div class="col-md-4 p-0">
                    <div class="auth-innerleft">
                        <div class="text-center">
                            <a href="{{ url('/') }}"><img src="http://127.0.0.1:8000/assets/home/images/logo.png" alt="logo"></a>
                            <hr>
                            <div class="social-media">
                                <ul class="list-inline">
                                    <li class="list-inline-item"><i class="fa fa-facebook" aria-hidden="true"></i></li>
                                    <li class="list-inline-item"><i class="fa fa-google-plus" aria-hidden="true"></i></li>
                                    <li class="list-inline-item"><i class="fa fa-twitter" aria-hidden="true"></i></li>
                                    <li class="list-inline-item"><i class="fa fa-instagram" aria-hidden="true"></i></li>
                                    <li class="list-inline-item"><i class="fa fa-rss" aria-hidden="true"></i></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 p-0">
                    <div class="auth-innerright">
                        <div class="authentication-box">
                            <h3>Forgot Password</h3>
                            <h6>Enter your Email for set your password</h6>

                            @if(session('error'))
                                <h6 class="text-danger">{{ session('error') }}</h6>
                            @endif

                            <div class="card mt-4 p-4">
                                <form class="theme-form" method="post" action="{{ url('check-email') }}">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="col-form-label">Email</label>
                                        <input type="text" name="email" class="form-control" placeholder="User Email" autocomplete="off">
                                        @if ($errors->has('email'))
                                            <span class="error text-danger">{{ $errors->first('email') }}</span>
                                        @endif
                                    </div>
                                    <div class="form-row mb-5">
                                        <div class="col-sm-3">
                                            <button type="submit" class="btn btn-red sign-up-btn ">Send Reset Link</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!--sign up page Ends-->
    </div>

</div>
<!--page-wrapper Ends-->

<!-- latest jquery-->
<script src="{{ URL::asset('/assets/back/js/jquery-3.2.1.min.js') }}" ></script>
<!-- Bootstrap js-->
<script src="{{ URL::asset('/assets/back/js/bootstrap/popper.min.js') }}" ></script>
<script src="{{ URL::asset('/assets/back/js/bootstrap/bootstrap.js') }}" ></script>
<!-- owlcarousel js-->
<script src="{{ URL::asset('/assets/back/js/owlcarousel/owl.carousel.js') }}" ></script>
<!-- Theme js-->
<script src="{{ URL::asset('/assets/back/js/script.js') }}" ></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script>
  @if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;

        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
  @endif
</script>
</body>

</html>
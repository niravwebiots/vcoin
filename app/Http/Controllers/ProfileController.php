<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Hash;
use File;
use App\User;
use Sentinel;

class ProfileController extends Controller
{
    public function user_profile()
    {        
        $imgurl ="";
        $user = Sentinel::getUser();
        if($user->google2fa_enable == 0)
        {
            $imgurl = app('App\Http\Controllers\Google2FAController')->enableTwoFactor();
        }

    	
        return view('dashboard.profile',compact(array('user','imgurl')));
    }

    public function update_user_profile(Request $request)
    {
    	$this->validate($request,[
    		'first_name' => 'required|max:255',
    		'last_name' => 'required|max:255'
    	]);

    	$user_id = Sentinel::getUser()->id;

        $user = User::find($user_id);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;

        if($user->update())
        {
            $notification = array(
                'message' => 'Successfull Your Profile is Updated.', 
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        }
        else
        {
            $notification = array(
                'message' => 'Sorry Your Profile is Not Updated.', 
                'alert-type' => 'error'
            );
            return redirect()->back()->with($notification);
        }
    }

    public function change_password(Request $request)
    {
        $this->validate($request,[
            'old_password' => 'required|max:254',
            'new_password' => 'required|min:6|max:254|same:confirm_password',
            'confirm_password' => 'required|min:6|max:254',
        ]);

    	$old_password = $request->old_password;

    	$user_id = Sentinel::getUser()->id;

    	$user = User::find($user_id);

    	if(Hash::check($old_password,$user->password))
        {
            $user->password = bcrypt($request->new_password);
            
        	if($user->update())
	        {
                $notification = array(
                    'message' => 'Successfull Your Password is Updated.', 
                    'alert-type' => 'success'
                );
	            return redirect()->back()->with($notification)->with('tab','chg_pwd');
	        }
	        else
	        {
                $notification = array(
                    'message' => 'Sorry Your Password is Not Updated.', 
                    'alert-type' => 'error'
                );
	            return redirect()->back()->with($notification)->with('tab','chg_pwd');
	        }
        }
        else
        {
            $notification = array(
                'message' => 'Your Old Password is Wrong. Please Try Again.', 
                'alert-type' => 'error'
            );
        	return redirect()->back()->with($notification)->with('tab','chg_pwd');
        }
    }

    public function profile_update(Request $request)
    {
    	$user_id = Sentinel::getUser()->id;
        $user = User::find($user_id);

    	$this->validate($request,[
            'profile_image'=>'required|mimes:jpeg,jpg,png',
        ]);

        if($user->profile_pic != "")
        {
            $image_path = public_path("assets/back/profile/".$user->profile_pic);  // Value is not URL but directory file path
            if(File::exists($image_path)) {
                File::delete($image_path);
            }
        }

        $image = $request->file('profile_image');

        $user->profile_pic = date('Ymd').time().$image->getClientOriginalName();
        $destinationPath = public_path('assets/back/profile/');
        
        if($image->move($destinationPath,$user->profile_pic))
        {
           $user->save();
           $notification = array(
                'message' => 'Your profile image is updated.', 
                'alert-type' => 'success'
            );
           return redirect()->back()->with($notification);
        }
        else
        {
            $notification = array(
                'message' => 'Sorry Your Profile Image Is Not Updated. Please Try Again.', 
                'alert-type' => 'error'
            );
            return redirect('user-profile')->with($notification);
        }
    }
}

@extends('dashboard_layouts.master')

@section('title') User Manage | Management @endsection

@section('content')

<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <h3>Users List</h3>
                </div>
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Management</li>
                        <li class="breadcrumb-item active">User Manage</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5>User Manage</h5>
            </div>
            <div class="card-body table-responsive" data-intro="This is the name of this site">
                <div class="user-status">
                    <table id="dashboard-table" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>User Name</th>
                                <th>Email</th>
                                <th>Status</th>
                                <!-- <th>Kyc Status</th>
                                <th>Kyc</th> -->
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1;?>
                            @foreach($users as $user)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $user->user_name }}</td>
                                <td>{{ $user->email }}</td>
                                @if($user->status == 0)
                                <td> <span class="badge badge-danger"> Block </span> </td>
                                @elseif($user->status == 1)
                                <td> <span class="badge badge-info"> Active </span> </td>
                                @endif
                                <!-- <td>
                                @if($user->kyc_status == 0)
                                <span class="text-warning"><i class = "fa fa-spinner fa-spin"></i> Pending </span>
                                @elseif($user->kyc_status == 1)
                                <span class="text-success"><i class="fa fa-check" aria-hidden="true"></i> Approved </span>
                                @elseif($user->kyc_status == 2)
                                <span class="text-danger"><i class="fa fa-times" aria-hidden="true"></i> Canceled </span>
                                @elseif($user->kyc_status == 3)
                                <span class="text-info"><i class="fa fa-file-text-o" aria-hidden="true"></i>  KYC Upload </span>
                                @endif
                                </td>
                                <td><a href="{{ url('kyc') }}/{{ $user->id }}" class="btn btn-secondary"> Kyc </a></td> -->
                                <td>@if($user->status == 0)
                                <a href="{{ url('user-status') }}/{{ $user->id }}" class="btn btn-success"> Active </a>
                                @elseif($user->status == 1)
                                <a href="{{ url('user-status') }}/{{ $user->id }}" class="btn btn-danger"> Block </a>
                                @endif  
                                <a href="{{ url('user-delete') }}/{{ $user->id }}" class="btn btn-warning"> Delete </a>
                                <!-- <a href="" class="btn btn-info"> Details </a></td> -->
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#dashboard-table').DataTable();
} );
</script>
@endsection
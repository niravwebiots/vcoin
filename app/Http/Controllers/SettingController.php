<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Setting;


class SettingController extends Controller
{
    public function settings()
    {
        $settings = Setting::get()->first();
        return view('admin.settings',compact('settings'));
    }

    public function update_settings(Request $request)
    {
        $this->validate($request,[
            'ico_start_date'=>'required',
            'ico_end_date'=>'required',
            'total_coins'=>'required',
            //'sold_coins'=>'required|numeric',
            'vcoin_rate'=>'required',
            // 'min_buy_token'=>'required',
            // 'max_buy_token'=>'required',
            'bonus'=>'required',
            // 'referral_bonus'=>'required',
        ]);

        $settings = Setting::find('1');
        $settings->total_coins = $request->total_coins;
        //$settings->sold_coins = $request->sold_coins;

        $var = $request->ico_start_date;
        $date = str_replace('/', '-', $var);
        $start_date = date('Y-m-d', strtotime($date));
        $settings->ico_start_date = $start_date;


        $var = $request->ico_end_date;
        $date = str_replace('/', '-', $var);
        $end_date = date('Y-m-d', strtotime($date));
        $settings->ico_end_date = $end_date;

        // $settings->min_buy_token = $request->min_buy_token;
        // $settings->max_buy_token = $request->max_buy_token;
        $settings->bonus = $request->bonus;
        // $settings->referral_bonus = $request->referral_bonus;
        $settings->coin_rate = $request->vcoin_rate;
        $settings->update();
        
        $notification = array(
            'message' => 'Setting data is save successfully', 
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
}

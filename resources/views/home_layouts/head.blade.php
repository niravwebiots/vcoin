<!--Common Styles Plugins-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/jquery-ui.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/magnific-popup.css')}}">
    
    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Antic+Slab|Kaushan+Script|Open+Sans" rel="stylesheet">

   
    <!-- Main Style -->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/space.css')}}" media="all">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/style.css')}}" media="all">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/style1.css')}}" media="all">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/responsive.css')}}" media="all">
    <link href="https://fonts.googleapis.com/css?family=Merienda" rel="stylesheet">
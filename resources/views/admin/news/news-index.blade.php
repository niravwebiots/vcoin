@extends('dashboard_layouts.master')


@section('title') News Manage | Vancoin @endsection

@section('content')

<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <h3>News Manage</h3>
                </div>
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Setting</li>
                        <li class="breadcrumb-item active">News Manage</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
            	<a href="{{ route('news-manage.create') }}" title="Latest News Add" class="btn btn-primary">Add News</a>
            </div>
            <div class="card-body table-responsive" data-intro="This is the name of this site">
                <div class="user-status">
                    <table id="news-table" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Sub Title</th>
                                <th>Content</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        	@php $i=1; @endphp
                        	@foreach($news as $new)
                        		<tr>
                        			<td>{{ $i++ }}</td>
                        			<td>{{ $new->title }}</td>
                        			<td>{{ $new->sub_title }}</td>
                        			<td>{!! substr($new["content"], 0, 340) !!}</td>
                        			<td><img src="{{ URL::asset('assets/back/news') }}/{{ $new->img }}" height="40px" width="40px" alt=""></td>
                        			<td><a href="{{ route('news-manage.edit',$new->id) }}" title="Edit" class="btn btn-info">Edit</a>
                        				<form action="{{ route('news-manage.destroy',$new->id) }}" method="post" accept-charset="utf-8">
                        					{{ csrf_field() }}
                        					{{ method_field('DELETE') }}
                        					<button type="delete" class="btn btn-danger mt-2">Delete</button>
                        				</form>
                        			</td>
                        		</tr>
                        	@endforeach                        	
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
	<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
	    $('#news-table').DataTable();
	} );
	</script>
@endsection
@extends('dashboard_layouts.master')


@section('title') Phases | Settings @endsection

@section('content')

<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <h3>Phases list</h3>
                </div>
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                            <li class="breadcrumb-item active">Settings</li>
                        <li class="breadcrumb-item active">Phases</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="float-left">Phases</h5>
                        <a href="{{ url('add-phase') }}" class="btn btn-success float-right">Add New Phase</a>
                    </div>
                    <div class="card-body table-responsive" data-intro="This is the name of this site">
                        <div class="user-status">
                            <table id="dashboard-table" class="display">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Phase Name</th>
                                        <th>USD Price</th>
                                        <th>Coins</th>
                                        <th>Sold Coins</th>
                                        <th>Bonus</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Status</th>
		                                <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php($i=1)
                                    @foreach($phases as $p)
                                    <tr @if($p->status == 1) class="bg-info" @endif>
                                        <td>{{ $i }}</td>
                                        <td>{{ $p->phase_name }}</td>
		                                <td>{{ $p->usd_price }}</td>
                                        <td>{{ $p->total_coins }}</td>
                                        <td>{{ $p->sold_coins }}</td>
                                        <td>{{ $p->bonus }}</td>
                                        <td>{{ date("d/m/Y", strtotime($p->start_date)) }}</td>
                                        <td>{{ date("d/m/Y", strtotime($p->end_date)) }}</td>
                                        <td>
                                            @if($p->status == 0)
                                                <span class="badge badge-primary">Inactive</span>
                                            @elseif($p->status == 1)
                                                <span class="badge badge-success">Active</span>
                                            @elseif($p->status == 2)
                                                <span class="badge badge-info">Completed</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if($p->status == 0)
                                            <a href="{{ url('edit-phase/'.$p->id) }}" class="btn btn-info" title="Edit" onclick="return confirm('Are You Sure. You Want to Edit this Phase!')">
                                                Edit
                                            </a>
                                            @endif

                                            @if($p->status == 0)
                                            <a href="{{ url('do-delete-phase/'.$p->id) }}" class="btn btn-danger" title="Delete" onclick="return confirm('Are You Sure. You Want to Delete this Phase!')">
                                                Delete
                                            </a>
                                            @endif

                                            @if($p->status == 1)
                                            <a  class="btn btn-success">Active Phase</a>
                                            @endif

                                            @if($p->status == 0)
                                                <a href="{{ url('do-active-phase/'.$p->id) }}" class="btn btn-primary" onclick="return confirm('Are You Sure. You Want to Active this Phase!')">Active</a>
                                            @endif
                                        </td>
                                    </tr>
                                    @php($i++)
                                    @endforeach
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#dashboard-table').DataTable();
} );
</script>

@endsection
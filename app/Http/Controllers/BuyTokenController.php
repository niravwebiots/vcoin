<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use App\User;
use App\Models\BuyToken;
use App\Models\Setting;
use App\Models\Currency;
use App\Models\Phase;

class BuyTokenController extends Controller
{
	public function icoInformation()
	{
		$setting = Setting::first();
		$user = Sentinel::getUser();
		$buytoken = BuyToken::where('user_id',$user->id)->with('buytoken_user')->orderBy('id','desc')->get();

		return view('user.ico.buy_token_coin',compact('user','setting','buytoken'));

	}

	public function coinBalance(Request $request)
	{
		$select = $request->value;
		// return $request->all();
		$userId = Sentinel::getUser()->id;
		$User = User::where('id',$userId)->first();
		$setting = Setting::first();
		if($select == 'BTC'){
			return $array = array('balance' => $User->btc_balance,'coin_rate' => $setting->btc_rate );
		}
		elseif ($select == 'ETH') {
			return $array = array('balance' => $User->eth_balance,'coin_rate' => $setting->eth_rate );
		}
		elseif ($select == 'BCH') {
			return $array = array('balance' => $User->bch_balance,'coin_rate' => $setting->bch_rate );
		}
		elseif ($select == 'LTC') {
			return $array = array('balance' => $User->ltc_balance,'coin_rate' => $setting->ltc_rate );
		}
		elseif ($select == 'ETC') {
			return $array = array('balance' => $User->etc_balance,'coin_rate' => $setting->etc_rate );
		}
		else{
			
		}
	}


	public function buyToken(Request $request,$id)
	{

		$this->validate($request,[
			'tokens' => 'required|numeric',
		]);
	   
		$user = Sentinel::getUser();
			
		$total_tokens = $request->tokens;
		$coin = $request->selectCoin;
		$total_price = $request->total_price;

		$setting = Setting::first();
		$total_coins = $setting->total_coins;
		$sold_coins = $setting->sold_coins;

		$user_bonus = $total_tokens * $setting->bonus / 100;
		$referral_bonus = $total_tokens * $setting->referral_bonus / 100;
		$total_bouns = $user_bonus +  $referral_bonus;

		$buytoken = new BuyToken;
		$buytoken->user_id = $user->id;
		$buytoken->coin_type = $coin;
		$buytoken->tokens = $total_tokens;
		$buytoken->amount = $total_price;
		$buytoken->bonus = $user_bonus;
		$buytoken->status = 1;
		$buytoken->txid = mt_rand(100000, 999999999);

		$phase = Phase::where('status',1)->first();
		$phase->total_coins = $phase->total_coins-$total_tokens;
		$phase->sold_coins = $phase->sold_coins+$total_tokens;
		$phase->update();

		$reff_id=$user->refferral_id;
		if ($reff_id) { 
	
		   $reffer_user = User::find($reff_id);

			$reffer_user->referral_bonus = $reffer_user->referral_bonus + $referral_bonus;
			$reffer_user->wallet_balance = $reffer_user->wallet_balance + $referral_bonus;
			$reffer_user->update();

			$buytoken->referral_bonus = $referral_bonus;
			$buytoken->refferral_id = $reff_id;
			$buytoken->save();

			$setting->total_coins = $total_coins - $total_tokens - $total_bouns;
			$setting->sold_coins = $total_tokens + $sold_coins + $total_bouns;
			$setting->update();
		}
		else{

			$setting->total_coins = $total_coins - $total_tokens;
			$setting->sold_coins = $total_tokens + $sold_coins; 
			$setting->update();
		}

		$buytoken->save();

		$coin = strtolower($coin);
		$balance = $coin.'_balance';

		$user_coin_blance = $user->$balance;
		$user->$balance =  $user_coin_blance - $total_price;
		$user->wallet_balance = $user->wallet_balance + $user_bonus + $total_tokens;
		$user->update();

		$notification = array(
			'message' =>  ''.strtoupper($coin).' '.'To Token Buy Successfully.', 
			'alert-type' => 'success'
		);
		return redirect()->back()->with($notification);
	}
}

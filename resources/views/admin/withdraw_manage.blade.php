@extends('dashboard_layouts.master')

@section('title') Withdraw | Management @endsection


@section('content')

<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <h3>Withdraws Management</h3>
                </div>
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Management</li>                        
                        <li class="breadcrumb-item active">Withdraw Management</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5>Withdrwals</h5>
            </div>

            <div class="card-body">
                <div class="table-responsive">
                    <table id="withdrawal-table" class="display">
                        <thead>
                        <tr>
                            <th scope="col">Sr.</th>
                            <th scope="col">User name</th>
                            <th scope="col">Coin name</th>
                            <th scope="col">Address</th>
                            <th scope="col">Amount</th>
                            <th scope="col">Txid</th>
                            <th scope="col">Status</th>
                            <th scope="col">Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        @php($i=1)
                        @foreach($withdrawals as $w)
                         
                        <tr>
                         <th scope="row">{{$i++}}</th>
                         <td>{{$w->users->user_name}}</td>
                         <td>@if($w->coin_type == 'wallet')<span>VNJA</span>@else<span style="text-transform: uppercase;">{{$w->coin_type}}</span>@endif</td>
                         <td>@if($w->address)
                            {{$w->address}}</td>
                            @else
                            <span class="text-info">Bank Payment</span>
                            @endif
                         <td>{{$w->amount}}</td>
                         <td>
                            @if($w->txid)<span>{{$w->txid}}</span>@else<span>Null</span>@endif
                         </td>
                         <td> 
                            @if($w->status == 0)
                                <span class="badge badge-warning"> Pending </span>
                            @elseif($w->status == -1 )
                                <span class="badge badge-danger"> Rejected </span>
                            @elseif($w->status == 3)
                                <span class="badge badge-info"> Approved </span>
                            @elseif($w->status == 1)
                                <span class="badge badge-success"> Completed </span>
                            @elseif($w->status == -2)
                                <span class="badge badge-danger"> Cancelled </span>
                            @endif
                         </td>
                         <td>
                            @if($w->coin_type == 'bank')
                                @if($w->status == 0)
                                <a href="{{ url('withdraw-approval-bank') }}/{{$w->id}}" class="btn btn-info"> Details </a>
                                <a href="{{ url('withdraw-reject-bank') }}/{{$w->id}}" class="btn btn-danger"> Reject </a>
                                @elseif($w->status == -1)
                                    <span class="text-danger"><b><i class="fa fa-ban" aria-hidden="true"></i> Rejected </b></span>
                                @elseif($w->status == 1)
                                    <h6><span class="text-success"><b><i class="fa fa-check-circle" aria-hidden="true"></i> Completed </b></span></h6>
                                @endif
                            @else
                                @if($w->status == 0)
                                 <a href="{{ url('withdraw-approval') }}/{{$w->id}}" class="btn btn-success" style="margin-bottom: 4px;"> Approval </a>
                                 <a href="{{ url('withdraw-reject') }}/{{$w->id}}" class="btn btn-danger"> Reject </a>
                                @elseif($w->status == -1 )
                                    <h6><span class="text-danger"><b> <i class="fa fa-ban" aria-hidden="true"></i> Rejected </b></span></h6>
                                @elseif($w->status == 3)
                                    <h6><span class="text-info"><b><i class="fa fa-check" aria-hidden="true"></i> Approved </b></span></h6>
                                @elseif($w->status == 1)
                                    <h6><span class="text-success"><b><i class="fa fa-check-circle" aria-hidden="true"></i> Completed </b></span></h6>
                                @elseif($w->status == -2)
                                    <span class="text-danger"><b><i class="fa fa-close" aria-hidden="true"></i> Cancelled </b></span>
                                @endif
                            @endif

                         </td>
                        </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#withdrawal-table').DataTable();
} );
</script>
@endsection
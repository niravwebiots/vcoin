<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/','HomeController@home');
Route::get('/home','HomeController@home');
Route::get('news','HomeController@news_page');
Route::get('blogs','HomeController@blog_page');
Route::get('contact-us','HomeController@contact_us');
Route::get('news/{id}','HomeController@news');
Route::get('blog/{id}','HomeController@blog');


Route::get('Ccavenue','CCAvenueController@index');
Route::post('Ccavenue','CCAvenueController@Ccavenue');

// Registration and Activation
Route::view('register','home_pages.registration');
Route::post('postregister','RegistrationController@postRegister');
Route::get('activate/{email}/{code}','RegistrationController@activateMail');


// User Login
Route::get('login','LoginController@login');
Route::post('do-user-login','LoginController@vcoinLogin');
Route::get('do-user-logout','LoginController@dologout');

// Forgot Password
Route::get('forgot-password','LoginController@forgot_password');
Route::post('check-email','LoginController@check_email');
Route::get('reset-password/{email}/{token}','LoginController@reset_password');
Route::post('do-reset-password','LoginController@do_reset_password');


// ipn handler
// Route::post('ipn-handler','DepositController@IpnHandler');
Route::get('ipn-handler','DepositController@IpnHandler');
Route::post('withdrawal-handler','UserController@withdrawal_IpnHandler');
// User Routes
Route::group(['middleware' => 'user'],function(){

	Route::get('user-dashboard','DashboardController@user_dashboard');
	//user profile change password
	Route::get('user-profile','ProfileController@user_profile');
	Route::post('update-user-profile','ProfileController@update_user_profile');
	Route::post('change-password','ProfileController@change_password');
	Route::post('profile_update','ProfileController@profile_update');
	//user kyc
	// Route::get('user-kyc','KycController@index');
	// Route::post('user-kyc-upload','KycController@kyc_upload');
	//user wallet
	Route::get('user-wallet','UserController@user_wallet');
	Route::get('deposit/{coin}','DepositController@deposit');
	Route::get('withdraw/{coin}','UserController@withdraw');
	Route::post('do-withdraw','UserController@do_withdraw');
	//deposit bank
	Route::get('deposit-bank','UserController@bankDeposit');
	Route::post('amount-deposit-bank','UserController@amountDepositBank');
	//withdrawal bank
	// Route::get('withdraw-bank','UserController@bankWithdrawal');
	// Route::post('amount-withdrawal-bank','UserController@amountWithdrawalBank');

	// ico / buy-token
	Route::get('buy-token-list','BuyTokenController@icoInformation');
	Route::post('coin-balance','BuyTokenController@coinBalance');
	Route::post('buy-token/{id}','BuyTokenController@buyToken');

	Route::get('ico-information','UserController@ico_information');
	Route::get('network','UserController@network');
	Route::get('getUserDataNetwork','UserController@getUserDataNetwork');
	// buy with paypal
	Route::post('buy-paypal','PaypalController@postPaymentWithpaypal');
	Route::get('success-payment','PaypalController@paymentSucces');
	Route::get('cancel-payment','PaypalController@paymentCancel');
	Route::post('cancel-payment','PaypalController@paymentCancel');
	Route::get('callback','PaypalController@paypalCallback');
	Route::post('currency-price','PaypalController@currencyPrice');
	

});
// User Login

//Route::get('administrator','LoginController@login');
Route::post('do-admin-login','LoginController@dologin');
Route::get('do-admin-logout','LoginController@dologout');

// Admin Routes
Route::group(['middleware' => 'admin'],function(){
	// dashboard
	Route::get('admin-dashboard','DashboardController@admin_dashboard');
	// user manage
	Route::get('user-manage','AdminController@user_manage');
	Route::get('user-status/{id}','AdminController@userStatus');
	Route::get('user-delete/{id}','AdminController@userDelete');
	// profile
	Route::get('admin-profile','ProfileController@user_profile');
	Route::post('update-admin-profile','ProfileController@update_user_profile');
	Route::post('admin-change-password','ProfileController@change_password');
	Route::post('admin-profile-update','ProfileController@profile_update');
	// user kyc approve and regect
	// Route::get('kyc/{id}','AdminController@userKyc');
	// Route::get('kyc-approve/{id}','AdminController@kyc_approve');
	// Route::get('kyc-reject/{id}','AdminController@kyc_reject');
	// history
	Route::get('deposit-history','HistoryController@depositHistory');
	Route::get('withdraw-history','HistoryController@withdrawHistory');
	Route::get('token-history','HistoryController@tokenHistory');
	// setting ico
	Route::get('settings','SettingController@settings');
	Route::post('update-settings','SettingController@update_settings');

	Route::get('phases','PhaseController@phases');
	Route::get('add-phase','PhaseController@add_phase');
	Route::post('do-add-phase','PhaseController@do_add_phase');
	Route::get('edit-phase/{id}','PhaseController@edit_phase');
	Route::post('do-edit-phase','PhaseController@do_edit_phase');
	Route::get('do-delete-phase/{id}','PhaseController@do_delete_phase');
	Route::get('do-active-phase/{id}','PhaseController@do_active_phase');
	// Withdrawal
	Route::get('withdraw-manage','WithdrawalController@withdraw_manage');
	Route::get('withdraw-approval/{id}','WithdrawalController@withdraw_approval');
	Route::get('withdraw-reject/{id}','WithdrawalController@withdraw_reject');
	//withdrawal bank
	// Route::get('withdraw-approval-bank/{id}','WithdrawalController@withdrawBank');
	// Route::post('withdraw-approval-bank/{id}','WithdrawalController@withdrawApproveBank');
	// Route::get('withdraw-reject-bank/{id}','WithdrawalController@withdrawRejectBank');
	//bank
	Route::resource('bank-details','BankDetailsController');
	//deposit bank
	Route::get('deposit-approve-bank/{id}','DepositController@approveddepositBank');
	Route::get('deposit-reject-bank/{id}','DepositController@rejecteddepositBank');



	// News
	Route::resource('news-manage','NewsController');
	Route::resource('blog-manage','BlogController');

});

Route::get('exchange','PaypalController@currency');
//google2fa
Route::get('/2fa/enable', 'Google2FAController@enableTwoFactor');
Route::post('/2fa/save', 'Google2FAController@saveSecretKey');
Route::post('/2fa/disable', 'Google2FAController@disableTwoFactor');
Route::get('/2fa/validate', 'Auth\AuthController@getValidateToken');
Route::post('/2fa/validate', ['uses' => 'Auth\AuthController@postValidateToken']);
Route::post('/2fa/validate-disabletime', ['uses' => 'Auth\AuthController@postValidateTokenDesable']);
Route::post('2fa/validate-enabletime', 'Auth\AuthController@postValidateTokenenable');
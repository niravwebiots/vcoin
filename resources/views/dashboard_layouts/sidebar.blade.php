<!-- page sidebar -->
<?php
    $slug = Sentinel::getUser()->roles()->first()->slug;
?>

<div class="page-sidebar custom-scrollbar">
    <div class="sidebar-user text-center">
        <div>
            @if(Sentinel::getUser()->profile_pic != "")
                <img class="img-50 rounded-circle" src="{{ url('assets/back/profile/'.Sentinel::getUser()->profile_pic) }}" alt="user">
            @else
                <img class="img-50 rounded-circle" src="{{ URL::asset('/assets/back/images/avtar/1.jpg') }}" alt="user">
            @endif
        </div>
        <h6 class="mt-3 f-12">{{Sentinel::getUser()->user_name}}</h6>
    </div>
    @if($slug == 'user')
    <ul class="sidebar-menu">

        <div class="sidebar-title">General</div>
        <li>
            <a href="{{ url('user-dashboard') }}" class="sidebar-header" class="active">
                <i class="icon-blackboard"></i> <span>Dashboard</span>
            </a>
        </li>
        <!-- <li>
            <a href="{{ url('user-kyc') }}" class="sidebar-header">
                <i class="fa fa-id-card-o" aria-hidden="true"></i><span>Kyc Document</span>
            </a>
        </li> -->
        <li>
            <div class="sidebar-title">ICO</div>
            <a href="#" class="sidebar-header">
                <i class="fa fa-eercast" aria-hidden="true"></i><span>ico</span>
                <i class="fa fa-angle-right pull-right"></i>
            </a>
            <ul class="sidebar-submenu">

                <li><a href="{{ url('ico-information') }}"><i class="fa fa-angle-right"></i>ICO Information</a></li>
                <li><a href="{{ url('buy-token-list') }}"><i class="fa fa-angle-right"></i>Buy token</a></li>
            </ul>
        </li>
         <li>
            <a href="{{ url('user-wallet') }}" class="sidebar-header">
                <i class="icon-credit-card"></i><span>Wallet</span>
            </a>
        </li>
        <!-- <li>
            <a href="{{ url('network') }}" class="sidebar-header">
                <i class="fa fa-sitemap" aria-hidden="true"></i><span>Network</span>
            </a>
        </li>   -->      
    </ul>
    <div class="sidebar-widget text-center">
       <div class="sidebar-widget-top">
           <h6 class="mb-2 fs-14">Need Help</h6>
           <i class="fa fa-bell"></i>
       </div>
       <div class="sidebar-widget-bottom p-20 m-20">
           <p>(713)898-6818 ( USA)
               <br>vanjiagroup@gmail.com
               <br>Contact With VNJA
           </p>
       </div>
   </div>
    @elseif($slug == 'admin')
    <ul class="sidebar-menu">
        <div class="sidebar-title">General</div>
        <li>
            <a href="{{ url('admin-dashboard') }}" class="sidebar-header" class="active">
                <i class="icon-blackboard"></i> <span>Dashboard</span>
            </a>
        </li>
        <div class="sidebar-title">Management</div>
        <li>
            <a href="{{ url('user-manage') }}" class="sidebar-header">
                <i class="icon-user"></i><span>Users</span>
            </a>
        </li>
        <li>

            <a href="{{ url('withdraw-manage') }}" class="sidebar-header">
                <i class="icon-credit-card"></i><span>Withdrwal</span>

            </a>
        </li>
        <!-- <li>
            <a href="#" class="sidebar-header">
                <i class="icon-credit-card"></i><span>Transfer Token</span>
            </a>
        </li> -->
        <div class="sidebar-title"><i class="fa fa-history" aria-hidden="true"></i> Historys</div>
        <li>
            <a href="{{ url('deposit-history') }}" class="sidebar-header">
                <i class="fa fa-clock-o" aria-hidden="true"></i><span>Deposit History</span>
            </a>
        </li>
        <li>
            <a href="{{ url('withdraw-history') }}" class="sidebar-header">
                <i class="fa fa-clock-o" aria-hidden="true"></i><span>withdraw History</span>
            </a>
        </li>
        <li>
            <a href="{{ url('token-history') }}" class="sidebar-header">
                <i class="fa fa-clock-o" aria-hidden="true"></i><span>Token History</span>
            </a>
        </li>
        <div class="sidebar-title">Settings</div>
        <li>
            <a href="{{ url('settings') }}" class="sidebar-header">
                <i class="icon-settings"></i><span>Settings</span>
            </a>
        </li>
        <li>
            <a href="{{ url('phases') }}" class="sidebar-header">
                <i class="icon-ruler"></i><span>Phase Settings</span>
            </a>
        </li> 
        <li>
            <a href="{{ route('news-manage.index') }}" class="sidebar-header">
                <i class="fa fa-newspaper-o" aria-hidden="true"></i><span>News Feed</span>
            </a>
        </li>
        <li>
            <a href="{{ route('blog-manage.index') }}" class="sidebar-header">
                <i class="fa fa-newspaper-o" aria-hidden="true"></i><span>Blogs Feed</span>
            </a>
        </li>
        <li>
            <a href="{{ route('bank-details.index') }}" class="sidebar-header">
                <i class="fa fa-university" aria-hidden="true"></i><span>Bank Details</span>
            </a>
        </li>       
    </ul>
    @endif
</div>
<!--End page sidebar -->
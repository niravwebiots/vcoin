@extends('home_layouts.master')

@section('title') Blogs | Vancoin @endsection

@section('style')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/home/css/news.css') }}">
@endsection

@section('content')
<div class="container">
<div class="projcard-container">
		
	@foreach($blog as $blo)	
	<div class="projcard projcard-blue">
		<div class="projcard-innerbox">
			<img class="projcard-img" src="{{ URL::asset('assets/back/blog') }}/{{ $blo->img }}" />
			<div class="projcard-textbox">
				<div class="projcard-title">{{ $blo->title }}</div>
				<div class="projcard-subtitle">{{ $blo->sub_title }}</div>
				<div class="projcard-bar"></div>
				<div class="projcard-description">{!! substr($blo["content"], 0, 340) !!} <a href="{{ url('blog',$blo->id) }}" title="">Read More..</a> </div>
				<div class="projcard-tagbox">
					<span class="projcard-tag pull-right">{{ $blo->created_at->format('d M y') }}</span>
					<!-- <span class="projcard-tag">CSS</span> -->
				</div>
			</div>
		</div>
	</div>

	@endforeach
	{{ $blog->links() }}
</div>
</div>
@endsection

@section('script')

<script type="text/javascript">
	// document.querySelectorAll(".projcard-description").forEach(function(box) {
	
// });

</script>
@endsection

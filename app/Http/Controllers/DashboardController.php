<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Sentinel;
use App\Models\BuyToken;
use App\Models\Setting;
use App\Models\Deposit;
use DB;

class DashboardController extends Controller
{
    public function user_dashboard()
    {
        $settings = Setting::first();

    	return view('dashboard.index',compact('settings'));
    }

    public function admin_dashboard()
    {
    	$role = Sentinel::findRoleBySlug('user');
    	$users = $role->users()->with('roles')->count();
        $active_users = $role->users()->with('roles')->where('status','1')->count();

        $settings = Setting::first();

        $total_deposits = Deposit::select(DB::raw('coin_type,sum(amount) as deposit'))->groupBy('coin_type')->orderBy('coin_type','asc')->where('status','1')->get();

        $buy_history = BuyToken::with('buytoken_user')->orderBy('created_at','desc')->get();

    	return view('dashboard.index',compact('users','active_users','settings','total_deposits','buy_history'));
    }

    
}

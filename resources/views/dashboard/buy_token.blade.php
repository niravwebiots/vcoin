	@extends('dashboard_layouts.master')


	@section('title') Dashboard | Vcoin @endsection

	@section('content')


	<div class="page-body">
	    <!-- Container-fluid starts -->
	    <div class="container-fluid">
	        <div class="page-header">
	            <div class="row">
	                <div class="col-lg-6" data-intro="This is the name of this site">
	                    <h3>Buy Coin
	                        <small>Vcoin Admin panel</small>
	                    </h3>
	                </div>
	                <div class="col-lg-6" data-intro="This is the name of this site">
	                    <ol class="breadcrumb pull-right">
	                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
	                        <li class="breadcrumb-item active">Buy Vcoin</li>
	                    </ol>
	                </div>
	            </div>
	        </div>
	    </div>
	<div class="container-fluid">
	   <div class="row">
	      
	       
	        <div class="col-sm-6 col-md-4">
	            <div class="card">
	                <div class="card-header">
	                    <h5>Total Vcoin </h5>
	                </div>
	                <div class="card-body d-flex time-body text-center">
	                    <h3>{{$active_phase->total_coins}}</h3>
	                </div>
	            </div>
	        </div>

	        <div class="col-sm-6 col-md-4">
	            <div class="card">
	                <div class="card-header">
	                    <h5>Total Sold Vcoin</h5>
	                </div>
	                <div class="card-body d-flex time-body text-center">
	                    <h3>{{$active_phase->sold_coins}}</h3>
	                    
	                </div>
	            </div>
	        </div>

	        <div class="col-sm-12 col-md-4">
            	<div class="card">
	                <div class="card-header">
	                    <h5>Buy ICO time</h5>
	                </div>
	                <div class="card-body text-center">
	                    <div class="timer">
							<ul>
								<li><span id="days"></span><span class="timer-cal">D</span></li>
								<li><span id="hours"></span><span class="timer-cal">H</span></li>
								<li><span id="minutes"></span><span class="timer-cal">M</span></li>
								<li><span id="seconds"></span><span class="timer-cal">S</span></li>
							</ul>
	              		</div>
	                </div>
	            </div>
	        </div>

	        <div class="col-md-4 buy-coin">
	        	<div class="card">
	            	<div class="card-header p-3">
	                    <h5 class="card-title">0.00000000 VCoin</h5>
	                    <!-- <a href="#" class="btn btn-success btn-all">Buy All</a> -->
	                </div>
	                <div class="card-body buy-token">
	                	<form class="form-horizontal theme-form row" action="" method="post">
	                        <div class="form-group col-md-12">
	                            <label>Coin</label>
	                            <input type="text" name="coin" class="form-control coin">
	                        </div>
	                        <div class="form-group col-md-12">
	                            <label>Price</label>
	                            <input type="text" name="usd" class="form-control usd">
	                            <div class="input-group-prepend">
		                            <span class="input-group-text">BTC</span>
		                        </div>
	                        </div>
	                        <div class="form-group col-md-12">
	                            <label>Total</label>
	                            <input type="email" name="tokens" class="form-control tokens">
	                            <div class="input-group-prepend">
		                            <span class="input-group-text">BTC</span>
		                        </div>
	                        </div>
	                        <div class="form-group col-md-12 text-right">
	                            <button type="submit" class="btn btn-success mt-4">Buy</button>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>

	        <div class="col-md-4 buy-coin">
	        	<div class="card">
	            	<div class="card-header p-3">
	                    <h5 class="card-title">0.00000000 VCoin</h5>
	                    <!-- <a href="#" class="btn btn-success btn-all">Buy All</a> -->
	                </div>
	                <div class="card-body buy-token">
	                	<form class="form-horizontal theme-form row" action="" method="post">
	                        <div class="form-group col-md-12">
	                            <label>Coin</label>
	                            <input type="text" name="coin" class="form-control coin">
	                        </div>
	                        <div class="form-group col-md-12">
	                            <label>Price</label>
	                            <input type="text" name="usd" class="form-control usd">
	                            <div class="input-group-prepend">
		                            <span class="input-group-text">BTC</span>
		                        </div>
	                        </div>
	                        <div class="form-group col-md-12">
	                            <label>Total</label>
	                            <input type="email" name="tokens" class="form-control tokens">
	                            <div class="input-group-prepend">
		                            <span class="input-group-text">BTC</span>
		                        </div>
	                        </div>
	                        <div class="form-group col-md-12 text-right">
	                            <button type="submit" class="btn btn-success mt-4">Buy</button>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	        
	        <div class="col-sm-12">
	                <div class="card">
	                    <div class="card-header">
	                        <h5>Buy Coin History</h5>
	                    </div>
	                    <div class="card-body">
	                        <div class="table-responsive">
	                            <table id="ico-info" class="display">
	                                <thead>
		                                <tr>
		                                    <th>From date</th>
		                                    <th>To Date</th>
		                                    <th>Coin</th>
		                                    <th>Sold</th>
		                                    <th>Price(USD)</th>
		                                    <th>Bonus</th>
		                                    <th>Status</th>
		                                </tr>
	                                </thead>
	                                <tbody>
		                                <tr>
		                                    <td>2018-08-06 12:00 am</td>
		                                    <td>2018-08-20 11:59 pm</td>
		                                    <td>200000000</td>
		                                    <td>0</td>
		                                    <td>0.5</td>
		                                    <td>60</td>
		                                    <td>Live now</td>
		                                </tr>
	                                  <tr>
	                                    <td>2018-08-06 12:00 am</td>
	                                    <td>2018-08-20 11:59 pm</td>
	                                    <td>200000000</td>
	                                    <td>0</td>
	                                    <td>0.5</td>
	                                    <td>90</td>
	                                    <td>Ended</td>
	                                </tr>

	                                 <tr>
	                                    <td>2018-08-06 12:00 am</td>
	                                    <td>2018-08-20 11:59 pm</td>
	                                    <td>200000000</td>
	                                    <td>0</td>
	                                    <td>0.5</td>
	                                    <td>500</td>
	                                    <td>12day</td>
	                                </tr>
	                                 <tr>
	                                    <td>2018-08-06 12:00 am</td>
	                                    <td>2018-08-20 11:59 pm</td>
	                                    <td>200000000</td>
	                                    <td>0</td>
	                                    <td>0.5</td>
	                                    <td>500</td>
	                                    <td>12day</td>
	                                </tr>
	                                 <tr>
	                                    <td>2018-08-06 12:00 am</td>
	                                    <td>2018-08-20 11:59 pm</td>
	                                    <td>200000000</td>
	                                    <td>0</td>
	                                    <td>0.5</td>
	                                    <td>500</td>
	                                    <td>12day</td>
	                                </tr>
	                                 <tr>
	                                    <td>2018-08-06 12:00 am</td>
	                                    <td>2018-08-20 11:59 pm</td>
	                                    <td>200000000</td>
	                                    <td>0</td>
	                                    <td>0.5</td>
	                                    <td>500</td>
	                                    <td>12day</td>
	                                </tr>
	                                 <tr>
	                                    <td>2018-08-06 12:00 am</td>
	                                    <td>2018-08-20 11:59 pm</td>
	                                    <td>200000000</td>
	                                    <td>0</td>
	                                    <td>0.5</td>
	                                    <td>500</td>
	                                    <td>12day</td>
	                                </tr>

	                                </tbody>
	                            </table>
	                        </div>
	                    </div>
	                </div>
	            </div>
	   </div>
	</div>

	</div>

	@endsection

@section('script')
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
    	$('#ico-info').DataTable();
	});

    const second = 1000,
      minute = second * 60,
      hour = minute * 60,
      day = hour * 24;

@if($active_phase['status'] == 1)

let countDown = new Date('{{ $active_phase->end_date }}').getTime(),
    x = setInterval(function() {

      let now = new Date().getTime(),
          distance = countDown - now;

    document.getElementById('days').innerText = Math.floor(distance / (day)),
    document.getElementById('hours').innerText = Math.floor((distance % (day)) / (hour)),
    document.getElementById('minutes').innerText = Math.floor((distance % (hour)) / (minute)),
    document.getElementById('seconds').innerText = Math.floor((distance % (minute)) / second);

}, second)

@endif

</script>

<script type="text/javascript">
	$(".buy-coin .coin").keyup(function(){

		//var id = $(this).find(".usd").attr('id');
		
		$.ajax({
	        type: "GET",
	        url: "{{url('get-rate')}}",
	        success: function(rates){

	        	var rate = JSON.parse(rates);

	        	console.log(rate.btc);

	        	$(this).find(".usd").val(rate.btc);
	        }
	    });

	});
</script>


@endsection
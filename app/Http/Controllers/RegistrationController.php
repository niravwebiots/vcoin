<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Sentinel;
use App\User;
use App\Referral;
use Activation;
use Mail;

class RegistrationController extends Controller
{
    public function postRegister(Request $request)
    {
        // return $request->all();
 		$this->validate($request,[
 			'first_name' => 'required|string|max:255',
 			'last_name' => 'required|string|max:255',
 			'user_name' => 'required|unique:users|string|max:255',
 			'email' => 'required|email|unique:users|max:255',
 			'password' =>	'required|min:8|max:20',
 			'confirm_password' =>	'required|min:8|max:20|same:password',
 		]);


        $referral_user = 0;

         if($request->referral_code != "")
        {
         $referral_user = User::where('referral_code',$request->referral_code)->first()->id;

            $limit = User::where('refferral_id',$referral_user)->count();

            if($limit >= 2)
            {
                $notification = array(
                    'message' => 'Sorry this referral code is already used by 2 peoples!    ', 
                    'alert-type' => 'warning'
                );

                return redirect('register')->with($notification);
            }  
        }

        $user = Sentinel::register(array(
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'user_name' => $request->user_name,
            'email' => $request->email,
            'password' => $request->password,
            'referral_code' => str_random(12),
        ));

        User::where('email',$request->email)->update(array('refferral_id' => $referral_user));

        $reffer = new Referral;
        $reffer->user_id =  $user->id;
        $reffer->ref_user_id = $referral_user;
        $reffer->save();

        $role = Sentinel::findRoleBySlug('user');
        $role->users()->attach($user);

        $activate = Activation::create($user);
        $email =urldecode($user->email);

        $link = url('activate').'/'.$email.'/'.$activate->code;
        $text = 'Vancoin Activation Mail.';

        // return view('emails.activate',compact('user','link','text'));

        $this->sendActivateMail($user,$link,$text);

 			$notification = array(
	            'message' => 'Successfull register activate to check your email.', 
	            'alert-type' => 'success'
        	);

 		return redirect('register')->with($notification);

    }

    public function sendActivateMail($user,$link,$text)
    {
    	Mail::send('emails.activate', 
    		['user' => $user,
    		 'link' => $link,
    		 'text'	=> $text,
    		], 
    		function ($m) use ($user,$text) {
            	$m->to($user->email, $user->user_name)->subject('VNJA Activation Mail.');
        });
    }

    public function activateMail($email,$code)
    {
    	$user = User::whereEmail($email)->first();
    	$user_id =  Sentinel::findById($user->id);

    	if(Activation::complete($user_id,$code))
    	{
    		$user->status = 1;
    		$user->save();

    		 $notification = array(
	            'message' => 'Successfull activation your email.', 
	            'alert-type' => 'success'
        	);

    		return redirect('login')->with($notification); 
    	}
    	$notification = array(
            'message' => 'Link is expired please try-again.', 
            'alert-type' => 'error'
        );
    	return redirect('login')->with($notification);
    }
}

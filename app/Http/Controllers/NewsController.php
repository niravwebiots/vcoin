<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::orderBy('id','desc')->get();
        return view('admin.news.news-index',compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request,[
        'title' =>  'required',
        'content' => 'required',
        'img' =>  'required|mimes:jpeg,png,jpg,gif,svg'  
       ]);

        $image = $request->file('img');
        $img_name = rand(11111, 99999);
        $path = public_path('/assets/back/news/');
        $image->move($path, $img_name);

        $news = new News;
        $news->title = $request->title;
        $news->sub_title = $request->sub_title;
        $news->content = $request->content;
        $news->img = $img_name;
        $news->link = $request->link;
        $news->date = date("Y-m-d");
        $news->save();

        $notification = array(
            'message' => 'Successfull created news.', 
            'alert-type' => 'success'
        );
        return redirect('news-manage')->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news,$id)
    {
        $news = News::find($id);
        return view('admin.news.edit',compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,News $news, $id)
    {
        $this->validate($request,[
            'title' =>  'required',
            'content' =>  'required'
        ]);

        $news = News::find($id);
        $news->title = $request->title;
        $news->sub_title = $request->sub_title;
        $news->content = $request->content;
        $news->link = $request->link;


        if ($request->file('img')) {
            try{
            $file_path = public_path().'/assets/back/news/'.$news->img;
            unlink($file_path);
                }
            catch (\ErrorException  $e) {
            // return $e->getMessage();
            }

            $image = $request->file('img');
            $img_name = rand(11111, 99999).'_'.$image->getClientOriginalName();
            $path = public_path('/assets/back/news/');
            $image->move($path, $img_name);
            $news->img = $img_name;
        }

        $news->save();

        $notification = array(
            'message' => 'Successfull updated news.', 
            'alert-type' => 'success'
        );
        return redirect('news-manage')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news,$id)
    {
        $news = News::find($id);
        $news->delete();

        $notification = array(
            'message' => 'Successfull deleted news.', 
            'alert-type' => 'success'
        );
        return redirect('news-manage')->with($notification);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Sentinel;
use Cookie;
use App\Models\Deposit;
use App\Models\BuyToken;
use App\Models\Currency;
use App\Models\Setting;
use App\User;
use App\Models\Phase;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

class PaypalController extends Controller
{

    
    public function postPaymentWithpaypal(Request $request)
    {

        $this->validate($request,[
            'token' => 'required',
        ]);
        $user = Sentinel::getUser();
        
        $currency = $request->currency;
        $tokens = $request->token;
        $amount = $request->total_price;

        $buytoken = new BuyToken;     
        $buytoken->user_id = $user->id;
        $buytoken->tokens = $tokens;
        $buytoken->coin_type = $currency;
        $buytoken->amount = $amount;
        $buytoken->status = 0;
        $buytoken->txid = 0;
        $tid=str_random(30);
        $buytoken->tid = $tid;
        $buytoken->save();

        Cookie::queue('tid',$tid,45);
        // Set the PayPal account.
        // $data['business'] = 'dp_buyer_pp_GB_1535117210577472@paypal.com';
        // $data['business'] = 'info@codexworld.com';
        $data['business'] = 'angeltian816-facilitator@gmail.com';
        $data['return'] = url('callback');
        $data['cancel_return'] = url('cancel-payment');
        $data['notify_url'] = url('callback');
        $data['item_name'] = $tid;
        $data['amount'] = $amount;
        $data['currency_code'] = $currency;
        
        // $_SESSION['status'] = 0;
        // if($_SESSION['status']){ //save loggedin customer ref in session.
        //        $settings->cancelurl .= '&status='.$_SESSION['status'];
        //     }
         $queryString = http_build_query($data);
        
        return redirect('https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_xclick&'.$queryString);
    }

    public function paypalCallback(Request $request)
    {
        // return $request->all();
        $tid = $request->item_name;
        $buytoken = BuyToken::where('tid',$tid)->first();
        $total_tokens = $buytoken->tokens;
        
        if ($request->st == 'Completed') {
            $buytoken->status = 1;
            $buytoken->amount = $request->amt;
            $buytoken->txid = $request->tx;
            $buytoken->update();
        }

        $setting = Setting::first();
        $total_coins = $setting->total_coins;
        $sold_coins = $setting->sold_coins;

        $user_bonus = $total_tokens * $setting->bonus / 100;
        $referral_bonus = $total_tokens * $setting->referral_bonus / 100;
        $total_bouns = $user_bonus +  $referral_bonus;

        $buytoken->bonus = $user_bonus;

        $phase = Phase::where('status',1)->first();
        $phase->total_coins = $phase->total_coins-$total_tokens;
        $phase->sold_coins = $phase->sold_coins+$total_tokens;
        $phase->update();

        $user = Sentinel::getUser();
        $reffe_id = $user->refferral_id;

        if ($reffe_id) {

            $reffer_user = User::find($reffe_id);

            $reffer_user->referral_bonus = $reffer_user->referral_bonus + $referral_bonus;
            $reffer_user->wallet_balance = $reffer_user->wallet_balance + $referral_bonus;
            $reffer_user->update();

            $buytoken->referral_bonus = $referral_bonus;
            $buytoken->refferral_id = $reffe_id;
            $buytoken->save();

            $setting->total_coins = $total_coins - $total_tokens - $total_bouns;
            $setting->sold_coins = $total_tokens + $sold_coins + $total_bouns;
            $setting->update();
        }else{

            $setting->total_coins = $total_coins - $total_tokens;
            $setting->sold_coins = $total_tokens + $sold_coins; 
            $setting->update();
        }

        $user->wallet_balance = $user->wallet_balance + $user_bonus + $total_tokens;
        $user->update();
        // return $casH_payment = Session()->get('casH_payment');

        $notification = array(
            'message' =>  'Token Buy Successfully.', 
            'alert-type' => 'success'
        );
       return redirect('buy-token-list')->with($notification);
    }

    public function paymentSucces()
    {
         $notification = array(
            'message' =>  'Token Buy Successfully.', 
            'alert-type' => 'success'
        );
        return redirect('buy-token-list')->with($notification);
    }

    public function paymentCancel(Request $request)
    {
         // return $request->all();
        $value = Cookie::get('tid');
        $tid = $request->item_name;
        $buytoken = BuyToken::where('tid',$value)->first();
        $buytoken->status = 2;
        $buytoken->update();    

         $notification = array(
            'message' =>  'BuyToken payment cancelled.', 
            'alert-type' => 'error'
        );
        return redirect('buy-token-list')->with($notification);
    }

    public function currencyPrice(Request $request)
    {
        $currency = $request->value;
        
        return $currency_price = Currency::where('currency',$currency)->first();
    }
    
    public function currency(Request $request)
    {
        
        $ch = file_get_contents('https://v3.exchangerate-api.com/bulk/1398a271a0c617cd5f0f6672/USD');  
        $ch = (array)json_decode($ch);
    
        $ch = $ch['rates'];

        foreach ($ch as $key => $value) {

            // echo "{$key} => {$value} <br>";
           

           $currency = Currency::where('currency',$key)->first();
               if($currency){
                $currency->price = $value;
                $currency->list = 2;
                $currency->update();
            }

        }
    }
    
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Phase;
use App\Models\Blog;
use App\Models\Setting;
use App\News;

class HomeController extends Controller
{
    public function home()
    {
    	$news = News::orderBy('id','desc')->limit(3)->get();
        $blogs = Blog::orderBy('id','desc')->limit(3)->get();
        $phase = Phase::where('status',1)->first();
        $total_tokens = Setting::first()->total_coins;
        return view('welcome',compact('phase','news','blogs','total_tokens'));
	}

	public function news_page()
	{
		$news = News::orderBy('id','desc')->Paginate(5);
		return view('home_pages.news_page',compact('news'));
	}

	public function contact_us()
	{
		return view('home_pages.contact_us');
	}

	public function news($id)
	{
		$news = News::find($id);
		return view('home_pages.news',compact('news'));
	}

	public function blog_page()
	{
		$blog = Blog::orderBy('id','desc')->Paginate(5);
		return view('home_pages.blog_page',compact('blog'));
	}

	public function blog($id)
	{
		$blog = Blog::find($id);
		return view('home_pages.blog',compact('blog'));
	}
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Phase;
use App\Models\Setting;

class PhaseController extends Controller
{
    public function phases()
    {
        $phases = Phase::where('is_delete','=','0')->get();
        return view('admin.phases',compact('phases'));
    }

    public function add_phase()
    {
        return view('admin.add_phase');
    }

    public function do_add_phase(Request $request)
    {
        $this->validate($request,[
            'phase_name'=> 'required',
            'usd_price'=> 'required|numeric',
            'total_coins'=> 'required|numeric',
            'bonus'=> 'required|numeric',
            'start_date'=> 'required',
            'end_date'=> 'required'
        ]);

        $phase = new Phase();
        $phase->phase_name = $request->phase_name;
        $phase->usd_price = $request->usd_price;
        $phase->total_coins = $request->total_coins;
        $phase->bonus = $request->bonus;

        $var = $request->start_date;
        $date = str_replace('/', '-', $var);
        $phase->start_date = date('Y-m-d', strtotime($date));

        $var = $request->end_date;
        $date = str_replace('/', '-', $var);
        $phase->end_date = date('Y-m-d', strtotime($date));

        if($phase->save())
        {
            $notification = array(
                'message' => 'Phase data is Successfully Save', 
                'alert-type' => 'success'
            );

            return redirect('phases')->with($notification);
        }
    }

    public function edit_phase($id)
    {
        $phase = Phase::where('id',$id)->first();
        return view('admin.edit_phase',compact('phase'));
    }

    public function do_edit_phase(Request $request)
    {
        $this->validate($request,[
            'phase_name'=> 'required',
            'usd_price'=> 'required|numeric',
            'total_coins'=> 'required|numeric',
            'bonus'=> 'required|numeric',
            'start_date'=> 'required',
            'end_date'=> 'required'
        ]);

        $phase = Phase::find($request->phase_id);
        $phase->phase_name = $request->phase_name;
        $phase->usd_price = $request->usd_price;
        $phase->total_coins = $request->total_coins;
        $phase->bonus = $request->bonus;
        
        $var = $request->start_date;
        $date = str_replace('/', '-', $var);
        $phase->start_date = date('Y-m-d', strtotime($date));

        $var = $request->end_date;
        $date = str_replace('/', '-', $var);
        $phase->end_date = date('Y-m-d', strtotime($date));

        if($phase->update())
        {
            $notification = array(
                'message' => 'Phase data is Successfully Updated', 
                'alert-type' => 'success'
            );

            return redirect('phases')->with($notification);
        }
    }

    public function do_delete_phase($id)
    {
        /*$phase = new Phase();
        $r = $phase->where('id',$id)->delete($id);*/
        
        $phase = Phase::find($id);
        $phase->is_delete = 1;
    
        if($phase->update())
        {
            $notification = array(
                'message' => 'Phase data is Successfully deleted', 
                'alert-type' => 'success'
            );

            return redirect('phases')->with($notification);
        }
        else
        {
            $notification = array(
                'message' => 'Sorry Phase data is not deleted!', 
                'alert-type' => 'error'
            );
            return redirect('phases')->with($notification);
        }
    }

    public function do_active_phase($pid)
    {
        $deactive_phase = Phase::where('status',1)->update(['status' => 2]);

        // $active_phase = Phase::where('id',$pid)->update(['status' => 1]);
        $active_phase = Phase::find($pid);
        $active_phase->status = 1;
        $active_phase->update();

        $setting = Setting::first();
        $setting->coin_rate = $active_phase->usd_price;
        $setting->bonus = $active_phase->bonus;
        $setting->Update();

        if($deactive_phase || $active_phase)
        {
            $notification = array(
                'message' => 'Phase Status is Successfully Updated', 
                'alert-type' => 'success'
            );

            return redirect('phases')->with($notification);
        }
        else
        {
            $notification = array(
                'message' => 'Sorry Phase Status is not Updated!', 
                'alert-type' => 'error'
            );
            return redirect('phases')->with($notification);
        }
    }
}

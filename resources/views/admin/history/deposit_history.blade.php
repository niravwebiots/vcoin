@extends('dashboard_layouts.master')


@section('title') Deposit | History @endsection

@section('content')

<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <h3>Users Deposits History</h3>
                </div>
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">History</li>
                        <li class="breadcrumb-item active">Deposit</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5>Deposit History</h5>
            </div>
            <div class="card-body table-responsive" data-intro="This is the name of this site">
                <div class="user-status">
                    <table id="deposit-table" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>User Name</th>
                                <th>Coin Type</th>
                                <th>Address</th>
                                <th>Amount</th>
                                <th>Transaction id</th>
                                <th>Status</th>
                                <th>action</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php $i=1; ?>
                        	@foreach($deposit as $depo)
                            <tr>
                               <td>{{ $i++ }}</td>
                               <td>{{ $depo->users->user_name }}</td>
                               <td>{{ $depo->coin_type }}</td>
                               <td>@if($depo->address){{ $depo->address }}
                                    @else
                                    No address
                                    @endif
                                </td>
                                <td>{{ $depo->amount }}</td>
                               <td>@if($depo->txid){{ $depo->txid }}
                                    @else
                                    No Transaction id
                                    @endif</td>
                               <td>                                    
                                    @if($depo->status == 0)
                                        <p class="badge badge-warning"> Pending </p>
                                    @elseif($depo->status == 1)
                                        <p class="badge badge-success"> Completed </p>
                                    @elseif($depo->status == 2)
                                        <p class="badge badge-danger"> Cancelled </p>
                                    @elseif($depo->status == -1)
                                        <p class="badge badge-danger"> Rejected </p>    
                                    @endif
                                </td>
                                <td>@if($depo->coin_type == 'bank')
                                        @if($depo->status == 0)
                                        <a href="{{ url('deposit-approve-bank') }}/{{ $depo->id }}" class="btn btn-success">Approved</a>
                                        <a href="{{ url('deposit-reject-bank') }}/{{ $depo->id }}" class="btn btn-danger">Reject</a>
                                        @elseif($depo->status == 1)<span class="text-success"><b><i class="fa fa-check-circle" aria-hidden="true"></i> Completed </b></span>
                                        @elseif($depo->status == -1)<span class="text-danger"><b><i class="fa fa-ban" aria-hidden="true"></i> Rejected </b></span>
                                        @endif
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#deposit-table').DataTable();
} );
</script>
@endsection
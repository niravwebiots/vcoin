<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="universal admin is super flexible, powerful, clean & modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, universal admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="{{ URL::asset('/assets/home/images/favicon.png') }}" type="image/x-icon"/>
    <link rel="shortcut icon" href="{{ URL::asset('/assets/home/images/favicon.png') }}" type="image/x-icon"/>
    <title>@yield('title')</title>

    @include('dashboard_layouts.head')

    @yield('style')
</head>
<body>

	<div class="page-wrapper">

		@include('dashboard_layouts.header')
		
		<div class="page-body-wrapper">

			@include('dashboard_layouts.sidebar')

			@yield('content')
		
		</div>
	</div>

	@include('dashboard_layouts.footer_script')
	
	@yield('script')
</body>
</html>
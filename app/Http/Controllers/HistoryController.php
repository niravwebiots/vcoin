<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Deposit;
use App\Models\Withdrawal;;
use App\Models\BuyToken;
use App\User;

class HistoryController extends Controller
{
    public function depositHistory()
    {
    	$deposit = Deposit::with('users')->orderBy('created_at','desc')->get();
    	return view('admin.history.deposit_history',compact('deposit'));
    }

    public function withdrawHistory()
    {
    	$withdraw = Withdrawal::with('users')->orderBy('created_at','desc')->get();

    	return view('admin.history.withdraw_history',compact('withdraw'));
    }

    public function tokenHistory()
    {
    	$token = BuyToken::with('buytoken_user')->orderBy('id','desc')->get();
        
    	return view('admin.history.token_history',compact('token'));
    }
}

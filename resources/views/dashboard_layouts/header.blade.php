<?php
    $slug = Sentinel::getUser()->roles()->first()->slug;
?>
<div class="page-main-header">
    <div class="main-header-left">
        <div class="logo-wrapper">
            <a href="{{ url('/home') }}">
                <img src="{{ URL::asset('assets/home/images/logo.png') }}"  alt=""/>
            </a>
        </div>
    </div>
    <div class="main-header-right row">
        <div class="mobile-sidebar">
            <div class="media-body text-right switch-sm">
                <label class="switch">
                    <input type="checkbox" id="sidebar-toggle" checked>
                    <span class="switch-state"></span>
                </label>
            </div>
        </div>
        <div class="nav-right col">
            <ul class="nav-menus">
                <!--  @if(Sentinel::getUser()->roles()->first()->slug == 'user')
                <div class="col-lg-4" style="margin-top: 18px;">
                      <form class="form-copy theme-form">
                          <div class="form-group">
                              <div class="input-group">                                
                                  <input type="text" id="copy" class="form-control" value="{{url('/register')}}/?referral_code={{ Sentinel::getUser()->referral_code }}" readonly="" aria-label="Search for...">
                                 <span class="input-group-btn">
                                 <button class="btn btn-success" type="button" data-copytarget="#lets_copy" onclick="copyFunction()" title="Copy"><i class="fa fa-copy" style="font-size:30;"></i></button>
                                 </span>
                              </div>
                          </div>
                      </form>
                  </div>
                  @endif -->
                <li class="onhover-dropdown">
                    <div class="media  align-items-center">
                        <img class="align-self-center pull-right mr-2" src="{{ URL::asset('assets/back/images/dashboard/user.png') }}" alt="header-user"/>
                        <div class="media-body">
                            <h6 class="m-0 txt-dark f-16">
                                My Account
                                <i class="fa fa-angle-down pull-right ml-2"></i>
                            </h6>
                        </div>
                    </div>
                    
                    <ul class="profile-dropdown onhover-show-div p-20">
                        @if($slug == 'user')
                        <li>
                            <a href="{{url('user-profile')}}">
                                <i class="icon-user"></i>
                                Edit Profile
                            </a>
                        </li>
                        @else
                        <li>
                            <a href="{{url('admin-profile')}}">
                                <i class="icon-user"></i>
                                Edit Profile
                            </a>
                        </li>
                        @endif
                        
                        <li>
                            
                            @if($slug == 'admin')
                                <a href="{{url('do-admin-logout')}}">
                                    <i class="icon-power-off"></i>
                                    Logout
                                </a>
                            @else
                                <a href="{{url('do-user-logout')}}">
                                    <i class="icon-power-off"></i>
                                    Logout
                                </a>
                            @endif
                        </li>
                    </ul>
                </li>
            </ul>
            <div class="d-lg-none mobile-toggle">
                <i class="icon-more"></i>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function copyFunction() {
    /* Get the text field */
    var copyText = document.getElementById("copy");
    /* Select the text field */
    copyText.select();
    /* Copy the text inside the text field */
    document.execCommand("copy");
  }


</script>
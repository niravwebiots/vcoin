@extends('dashboard_layouts.master')

@section('title') Deposit | Vcoin @endsection

@section('content')

<div class="page-body">
	    <!-- Container-fluid starts -->
	    <div class="container-fluid">
	        <div class="page-header">
	            <div class="row">
	                <div class="col-lg-6" data-intro="This is the name of this site">
	                    <h3>Deposit
	                        <!-- <small>Vcoin Admin panel</small> -->
	                    </h3>
	                </div>
	                <div class="col-lg-6" data-intro="This is the name of this site">
	                    <ol class="breadcrumb pull-right">
	                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
	                        <li class="breadcrumb-item active">Deposit</li>
	                    </ol>
	                </div>
	            </div>
	        </div>
	    </div>
		<div class="container-fluid">
		   <div class="row">
		        <div class="col-md-4">
		        	<div class="card">
		            	<div class="card-header p-3">
		                    <h5 class="card-title">Deposit Amount</h5>
		                </div>
		                <div class="card-body buy-token">
		                	<form class="form-horizontal theme-form row" action="" method="post">
		                        
		                        <div class="form-group col-md-12" align="center">
		                            <img src="{{ $qrcode }}">
		                        </div>

		                        <div class="form-group col-md-12 mt-4">
		                            <input type="text" name="address" class="form-control" value="{{ $deposit->address }}" readonly="">
		                        </div>
		                    </form>
		                </div>
		            </div>
		        </div>
		        <div class="col-sm-12">
		                <div class="card">
		                    <div class="card-header">
		                        <h5>List Buy Token</h5>
		                    </div>
		                    <div class="card-body">
		                        <div class="table-responsive">
		                            <table id="deposit-table" class="display">
		                                <thead>
		                                <tr>
		                                    <th scope="col">Sr.</th>
											<th scope="col">Address</th>
											<th scope="col">Txid</th>            
											<th scope="col">Amount</th>            
											<th scope="col">Status</th>
		                                </tr>
		                                </thead>
		                                <tbody>
		                                @php $i = 1; @endphp

		                                @foreach($deposit_history as $dp_ht)
		                                <tr>
		                                	<td>{{ $i++ }}</td>
		                                	<td>{{ $dp_ht->address }}</td>
		                                	<td>{{ $dp_ht->amount }}</td>
		                                	<td>{{ $dp_ht->txid }}</td>
		                                	<td>
		                                		@if($dp_ht->status == 0)<span class="badge badge-warning"> Pending </span>
		                                		@elseif($dp_ht->status == 1)<span class="badge badge-success"> Completed </span>
		                                		@elseif($dp_ht->status == 2)<span class="badge badge-danger"> Cancelled </span>
		                                		@endif
		                                	</td>
		                                </tr>
		                                @endforeach
		                                </tbody>
		                            </table>
		                        </div>
		                    </div>
		                </div>
		        </div>
		   </div>
		</div>
</div>

@endsection

@section('script')
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#deposit-table').DataTable();
} );
</script>
@endsection
@extends('home_layouts.master')

@section('title') News | Vancoin @endsection

@section('style')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/home/css/news.css') }}">
@endsection

@section('content')
<div class="container">
<div class="projcard-container">
		
	@foreach($news as $new)	
	<div class="projcard projcard-blue">
		<div class="projcard-innerbox">
			<img class="projcard-img" src="{{ URL::asset('assets/back/news') }}/{{ $new->img }}" />
			<div class="projcard-textbox">
				<div class="projcard-title">{{ $new->title }}</div>
				<div class="projcard-subtitle">{{ $new->sub_title }}</div>
				<div class="projcard-bar"></div>
				<div class="projcard-description">{!! substr($new["content"], 0, 340) !!} <a href="{{ url('news',$new->id) }}" title="">Read More..</a> </div>
				<div class="projcard-tagbox">
					<span class="projcard-tag">{{ $new->created_at->format('d M y') }}</span>
					<!-- <span class="projcard-tag">CSS</span> -->
				</div>
			</div>
		</div>
	</div>

	@endforeach
</div>
	{{ $news->links() }}
</div>
@endsection

@section('script')

<script type="text/javascript">
	// document.querySelectorAll(".projcard-description").forEach(function(box) {
	
// });

</script>
@endsection

 <!--Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/back/css/fontawesome.css') }}">
    <!-- iconfont -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/back/css/icofont.css') }}">
    <!-- Themify icon -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/back/css/themify.css') }}">
    <!-- Flag icon -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/back/css/flag-icon.css') }}">
    <!-- Tour css -->
    <!-- <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/back/css/tour.css') }}"> -->
    <!-- Owl css -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/back/css/owlcarousel.css') }}">
    <!-- Bootstrap css -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/back/css/bootstrap.css') }}">
    <!-- App css -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/back/css/style.css') }}">
    <!-- Responsive css -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/back/css/responsive.css') }}">
    <!-- datatble css -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/back/css/datatables.css') }}">
    <!-- Toastr message -->
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">\

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
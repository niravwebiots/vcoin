<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Softon\Indipay\Facades\Indipay;  

class CCAvenueController extends Controller
{
	public function index(){
		return view('ccevenue');
	}

    public function Ccavenue(Request $request)
    {
    	 $parameters = [
      
        'tid' => '1233221223322',
        
        'order_id' => '1232212',
        
        'amount' => '1200.00',
        
      ];
      
      $order = Indipay::prepare($parameters);
      return Indipay::process($order);
    }
}

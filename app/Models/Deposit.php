<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Deposit extends Model
{
	public function users()
    {
    	return  $this->belongsTo('App\User','user_id','id');
    }
}

@extends('dashboard_layouts.master')


@section('title') Profile | Dashboard @endsection

@section('content')

<?php
    $slug = Sentinel::getUser()->roles()->first()->slug;
?>

<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    @if($slug == 'user')
                    <h3>Profile</h3>
                    @else
                    <h3>Profile </h3>
                    @endif
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <!-- <li class="breadcrumb-item">Users </li> -->
                        @if($slug == 'user')
                        <li class="breadcrumb-item active">Dashboard</li>
                        <li class="breadcrumb-item active">Profile</li>
                        @else
                        <li class="breadcrumb-item active">Dashboard</li>
                        <li class="breadcrumb-item active">Profile</li>
                        @endif
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-6 col-lg-6 col-xl-4">
                <div class="card custom-card">
                    
                    <ul class="card-social">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-rss"></i></a></li>
                    </ul>
                    <div class="card-header">
                        <img src="{{ URL::asset('/assets/back/images/user-card/1.jpg') }}" class="img-fluid" alt="" />
                    </div>
                    @if($slug == 'user')
                        <form action="{{ url('profile_update')}}" method="post" enctype="multipart/form-data">
                    @elseif($slug == 'admin')
                        <form action="{{ url('admin-profile-update')}}" method="post" enctype="multipart/form-data">
                    @endif
                        {{ csrf_field() }}
	                    <div class="card-profile">
	                        @if(Sentinel::getUser()->profile_pic != "")
                                <img src="{{ url('assets/back/profile/'.Sentinel::getUser()->profile_pic) }}" class="rounded-circle" alt=""  id="profile"/>
                            @else
                                <img src="{{ URL::asset('/assets/back/images/avtar/1.jpg') }}" class="rounded-circle" alt=""  id="profile" />
                            @endif
                            <div class="image-upload">
                                <label for="file-input">
                                    <div class="profile-edit custom-profile">
                                       <!--  <img style="width: 60px; height: 60px; margin-top: 200px;" src="{{ URL::asset('/assets/back/images/edit.png') }}"> -->
                                       <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    </div>
                                </label>
                                <input id="file-input" type="file" accept="image/x-png,image/gif,image/jpeg" name="profile_image" onchange="readURL(this);">
                            </div>
	                    </div>
	                    <div class="text-center profile-details">
	                        <h4>{{Sentinel::getUser()->user_name}}</h4>
	                        <!-- <h6>Manager</h6> -->
	                    </div>
                        @if($errors->has('profile_image'))
                            <span class="text-danger">
                                <strong>{{ $errors->first('profile_image') }}</strong>
                            </span>
                        @endif
	                    <div class="card-footer row">
	                        <div class="col-12 col-sm-12">
	                            <button type="submit" class="btn btn-success">Submit</button>
	                        </div>                        
	                    </div>
                    </form>
                </div>
            </div>

            <div class="col-md-12 col-lg-12 col-xl-8">
             <div class="card custom-card">
                <ul class="nav nav-pills nav-fill theme-tab profile" id="myTab">
                    <li class="nav-item">
                        <a class="nav-link @if(!session('tab') && session('tab') == '') {{'active show'}} @endif" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Profile setting</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if(session('tab') == 'two-fact') {{'active show'}} @endif" id="two-fact-tab" data-toggle="tab" href="#two-fact" role="tab" aria-controls="two-fact" aria-selected="false">Two factor auth</a>
                    </li>
                    <li class="nav-item @if(session('tab') == 'chg_pwd') {{'active show'}} @endif">
                        <a class="nav-link" id="chg-pwd-tab" data-toggle="tab" href="#chg-pwd" role="tab" aria-controls="chg-pwd" aria-selected="false">Change password</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade @if(!session('tab')) {{'show active'}} @endif" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="col-md-12 pb-2">
                            @if($slug == 'user')
                                <form class="form-horizontal theme-form mt-5 row" action="{{ url('update-user-profile') }}" method="post">
                            @elseif($slug == 'admin')
                                <form class="form-horizontal theme-form mt-5 row" action="{{ url('update-admin-profile') }}" method="post">
                            @endif
                                {{ csrf_field() }}
                                    <div class="form-group col-md-6">
                                        <label for="exampleInputfirstname">First Name</label>
                                        <input type="text" name="first_name" class="form-control" id="exampleInputfirstname" value="{{$user->first_name}}">
                                        @if($errors->has('first_name'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('first_name') }}</strong>
                                        </span> @endif
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="exampleInputlastname">Last Name</label>
                                        <input type="text" name="last_name" class="form-control" id="exampleInputlastname" value="{{$user->last_name}}">
                                        @if ($errors->has('last_name'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('last_name') }}</strong>
                                        </span> @endif
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="exampleInputEmail1">Email address</label>
                                        <input type="email" class="form-control" id="exampleInputEmail1" value="{{$user->email}}" readonly="">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="exampleInputusername">User Name</label>
                                        <input type="text" class="form-control" id="exampleInputusername" value="{{$user->user_name}}" readonly="">
                                    </div>
                                    <div class="form-group col-md-12 text-right">
                                        <button type="submit" class="btn btn-success mt-4">Update</button>
                                    </div>
                                </form>
                        </div>
                    </div>

                    <div class="tab-pane fade mt-5" id="two-fact" role="tabpanel"
                                 aria-labelledby="two-fact-tab">
                                @if($user->google2fa_enable == 0)
                                    <div class="row two-fact">

                                        <div class="col-md-12 text-center">
                                            <h3>Enlable Google Authenticator</h3>
                                        </div>
                                        <div class="col-md-6 offset-md-3">
                                            <ul class="install-step">
                                                <li>1.Install Google Authenticator on your phone.</li>
                                                <li>2.Open the Google Authenticator app.</li>
                                                <li>3.Tab menu, then tab "Set up Account", then "Scan a barcode" or
                                                    "Enter key provided" is <strong
                                                            class="colors">3KQD7ED2B5A3CX3M</strong></li>
                                                <li>4.Your phone will now be in "scanning" mode. When you are in this
                                                    mode, scan the barcode below:
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-12 text-center">
                                            <img src="{{$imgurl['imgurl']}}" class="img-fluid">
                                            {{ session()->put('2fa:user:id', Sentinel::getUser()->id) }}
                                            <h3>{{$imgurl['secret']}}</h3>
                                        </div>
                                        <div id="google_auth_msg"></div>
                                        <div class="col-md-6 offset-md-3 text-center">
                                            <div id="alert-msg-enable"></div>
                                            <h5>Once you have scanned the barcode, enter the 6-digit code below from
                                                application:</h5>
                                            <form class="mt-4 theme-form" action="{{'2fa/save'}}" method="post">
                                                {{csrf_field()}}
                                                <div class="row margin12" id="match-otp-2fa_enable">
                                                    <div class="form-group  col-md-12">
                                                        <input type="text" class="form-control col-md-12"
                                                               id="google_2fa_otp_enable_new"
                                                               onkeyup="checkOTPEnable()">
                                                    </div>
                                                </div>
                                                <div class="form-group  ">
                                                    <input id="enable-2fa" type="submit" class="btn btn-success" disabled
                                                           value="Enable Google 2FA Authenticator">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                @else
                                    <div id="google_auth_msg"></div>
                                    <div class="col-md-6 offset-md-3 text-center">

                                        <h5> Enter the 6-digit code below from application:</h5>
                                        <form class="mt-4 theme-form" action="{{'2fa/disable'}}" method="post">
                                            {{ session()->put('2fa:user:id', Sentinel::getUser()->id) }}
                                            {{csrf_field()}}
                                            <div class="row margin12" id="match-otp-2fa_enable">
                                                <div class="form-group col-md-12 ">
                                                    <input type="text" class="form-control "
                                                           id="google_2fa_otp_enable_new" onkeyup="checkOTP()">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <input id="enable-2fa" type="submit" class="btn btn-success" disabled
                                                       value="Yes Disable Google 2FA Authenticator">
                                            </div>
                                        </form>
                                    </div>
                                @endif
                            </div>
                   
                    <div class="tab-pane fade @if(session('tab') == 'chg_pwd') {{ 'show active' }} @endif" id="chg-pwd" role="tabpanel" aria-labelledby="chg-pwd-tab">
                         
                        <div class="col-md-12">
                        @if($slug == 'user') 
                            <form class="form-horizontal theme-form mt-5 row" action="{{ url('change-password') }}" method="post">
                        @elseif($slug == 'admin')
                            <form class="form-horizontal theme-form mt-5 row" action="{{ url('admin-change-password') }}" method="post">
                        @endif
                                {{ csrf_field() }}
                                <div class="form-group col-md-12">
                                    <label>Old password</label>
                                    <input type="password" class="form-control" name="old_password" autocomplete="off"> 
                                    @if($errors->has('old_password'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('old_password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-6">
                                    <label>New password</label>
                                    <input type="password" class="form-control" name="new_password" autocomplete="off">
                                    @if($errors->has('new_password'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('new_password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Retype password</label>
                                    <input type="password" class="form-control" name="confirm_password" autocomplete="off">
                                    @if($errors->has('confirm_password'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('confirm_password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-12 text-right">
                                    <button type="submit" class="btn btn-success mt-4">Change</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div> 
</div>

@endsection

@section('script')

<script type="text/javascript">
    function checkOTP()
    {

        
        $("#google_auth_msg").html('');
        var token = '{{csrf_token()}}';
        var code= $("#google_2fa_otp_enable_new").val();
        if(code.length == 6)
        {
            $.ajax({
                type: 'post',
                url:"{{url('2fa/validate-disabletime')}}",
                data: "totp="+code+'&_token='+token,
                success: function (responseData) {
                    if(responseData==1)
                    {
                        $("#match-otp-2fa").hide();
                        $("#google_auth_msg").html("<div class='alert alert-success'>OTP match successfully</div>");
                        $("#google_2fa_otp").val('');
                        $("#enable-2fa").prop('disabled', false);
                    }
                },
                error: function (responseData) {
                    $("#google_auth_msg").html("<div class='alert alert-danger'>Nice try but OTP not match, Please try again.</div>");
                    console.log(responseData);
                    return false;
                }
            });
        }
    }
    function checkOTPEnable()
    {
        $("#alert-msg-enable").html('');

        var code= $("#google_2fa_otp_enable_new").val();
//        alert(code);
        var token = "{{csrf_token()}}";

        if(code.length == 6)
        {
            $.ajax({
                type: 'post',
                url:"{{url('2fa/validate-enabletime')}}",
                data: "totp="+code+"&_token="+token,
                success: function (responseData) {
                    console.log(responseData);
                    if(responseData==1)
                    {
                        $("#match-otp-2fa_enable").hide();
                        $("#alert-msg-enable").html("<div class='alert alert-success'>OTP match successfully</div>");
                        $("#google_2fa_otp_enable").val('');
                        $("#enable-2fa").prop('disabled', false);
                    }
                },
                error: function (responseData) {
                    $("#alert-msg-enable").html("<div class='alert alert-danger'>Nice try but OTP not match, Please try again.</div>");
                    console.log(responseData);
                    return false;
                }
            });
        }
        else {
            $("#alert-msg-enable").html("<div class='alert alert-danger'>Enter 6 digit only, Please try again.</div>");
            console.log(responseData);
            return false;

        }
    }

    function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#profile').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
</script>
 
@endsection

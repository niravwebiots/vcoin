@extends('dashboard_layouts.master')


@section('title') ICO | Dashboard @endsection

@section('content')


<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <h3>ICO Information</small>
                    </h3>
                </div>
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                        <li class="breadcrumb-item active">ICO Information</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
<div class="container-fluid">
   <div class="row">
      
       
       <!--  <div class="col-sm-6 col-md-4">
            <div class="card">
                <div class="card-header">
                    <h5>Total Vcoin </h5>
                </div>
                <div class="card-body d-flex time-body text-center">
                    <h3>{{$setting->total_coins}}</h3>
                </div>
            </div>
        </div> -->

        <div class="col-sm-6 col-md-4">
            <div class="card">
                <div class="card-header">
                    <h5>Total Sold Vcoin</h5>
                </div>
                <div class="card-body d-flex time-body text-center">
                    <h3>{{$setting->sold_coins}}</h3>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-4">
            <div class="card">
                <div class="card-header">
                    <h5>Buy ICO time</h5>
                </div>
                <div class="card-body text-center">
                    <div class="timer">
                <ul>
                    <li><span id="days"></span><span class="timer-cal">D</span></li>
                    <li><span id="hours"></span><span class="timer-cal">H</span></li>
                    <li><span id="minutes"></span><span class="timer-cal">M</span></li>
                    <li><span id="seconds"></span><span class="timer-cal">S</span></li>
                  </ul>
              </div>

                </div>
            </div>
        </div>

        <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>ICO Phases</h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="ico-info" class="display">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>From date</th>
                                        <th>To Date</th>
                                        <th>Coin</th>
                                        <th>Sold</th>
                                        <th>Price(USD)</th>
                                        <!-- <th>Bonus</th> -->
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php($i=1)
                                    @foreach($phases as $p)
                                    <tr @if($p->status == 1) class="bg-info" @endif>
                                        <td>{{$i}}</td>
                                        <td>{{$p->start_date}}</td>
                                        <td>{{$p->end_date}}</td>
                                        <td>{{$p->total_coins}}</td>
                                        <td>{{$p->sold_coins}}</td>
                                        <td>{{$p->usd_price}}</td>
                                        <!-- <td>{{$p->bonus}}</td> -->
                                        <td>
                                            @if($p->status == 0)
                                                <span class="badge badge-primary">Inactive</span>
                                            @elseif($p->status == 1)
                                                <span class="badge badge-success">Active</span>
                                            @elseif($p->status == 2)
                                                <span class="badge badge-info">Completed</span>
                                            @endif
                                        </td>
                                    </tr>
                                    @php($i++)
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
   </div>
</div>

</div>

@endsection

@section('script')
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#ico-info').DataTable();
});

    const second = 1000,
      minute = second * 60,
      hour = minute * 60,
      day = hour * 24;

@if($active_phase['status'] == 1)

let countDown = new Date('{{ $active_phase->end_date }}').getTime(),
    x = setInterval(function() {

      let now = new Date().getTime(),
          distance = countDown - now;

    document.getElementById('days').innerText = Math.floor(distance / (day)),
    document.getElementById('hours').innerText = Math.floor((distance % (day)) / (hour)),
    document.getElementById('minutes').innerText = Math.floor((distance % (hour)) / (minute)),
    document.getElementById('seconds').innerText = Math.floor((distance % (minute)) / second);

}, second)

@endif


</script>
@endsection
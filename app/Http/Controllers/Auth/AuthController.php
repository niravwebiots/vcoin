<?php

namespace App\Http\Controllers\Auth;

use Cache;
use Sentinel;
use App\Models\Setting;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use App\Http\Requests\ValidateSecretRequest;

class AuthController extends Controller
{
    private function authenticated(Request $request, Authenticatable $user)
    {
        if ($user->google2fa_secret) {
            Auth::logout();

            $request->session()->put('2fa:user:id', $user->id);

            return redirect('2fa/validate');
        }

        return redirect()->intended($this->redirectTo);
    }

    public function getValidateToken()
    {
        $setting_data = Setting::get();
        if (session('2fa:user:id')) {
            return view('home_pages/validate',compact('setting_data'));
        }
        return redirect('/',compact('setting_data'));
    }

    public function postValidateToken(ValidateSecretRequest $request)
    {
        //get user id and create cache key
        $userId = $request->session()->pull('2fa:user:id');
        $key    = $userId . ':' . $request->totp;

        //use cache to store token to blacklist
        Cache::add($key, true, 4);

        //login and redirect user
        Auth::loginUsingId($userId);

        return redirect('user-dashboard');
    }

    public function postValidateTokenDesable(ValidateSecretRequest $request)
    {
        //get user id and create cache key
        $userId = $request->session()->pull('2fa:user:id');
        $key    = $userId . ':' . $request->totp;

        //use cache to store token to blacklist
        Cache::add($key, true, 4);

        //login and redirect user
        $sentinel = app('sentinel');
        $user = $sentinel->findById($userId);
        $sentinel->login($user);
        return 1;
    }

    public function postValidateTokenenable(ValidateSecretRequest $request)
    {
        //get user id and create cache key
        $userId = $request->session()->pull('2fa:user:id');
        $key    = $userId. ':' .$request->totp;

        //use cache to store token to blacklist
        Cache::add($key, true, 4);

        //login and redirect user
        $sentinel = app('sentinel');
        $user = $sentinel->findById($userId);
        $sentinel->login($user);
        return 1;
        
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BuyToken extends Model
{
   public function buytoken_user()
   {
   	return $this->hasOne('App\User','id','user_id');
   }
}

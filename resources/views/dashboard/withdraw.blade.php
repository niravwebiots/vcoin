@extends('dashboard_layouts.master')


@section('title') Dashboard | Vcoin @endsection

@section('content')


<div class="page-body">
	    <!-- Container-fluid starts -->
	    <div class="container-fluid">
	        <div class="page-header">
	            <div class="row">
	                <div class="col-lg-6" data-intro="This is the name of this site">
	                    <h3>Wallet
	                        <small>Vancoin Admin panel</small>
	                    </h3>
	                </div>
	                <div class="col-lg-6" data-intro="This is the name of this site">
	                    <ol class="breadcrumb pull-right">
	                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
	                        <li class="breadcrumb-item active">ICO Information</li>
	                    </ol>
	                </div>
	            </div>
	        </div>
	    </div>
		<div class="container-fluid">
		   <div class="row">
		        <div class="col-md-4">
		        	<div class="card">
		            	<div class="card-header p-3">
		                    <h5 class="card-title">Withdrawal ( {{$coin_balance}} {{ $coin }})</h5>
		                </div>
		                <div class="card-body buy-token">
		                	<form class="form-horizontal theme-form row" action="{{ url('do-withdraw') }}" method="post">
		                		{{ csrf_field() }}
		                		<input type="hidden" name="coin" value="{{$coin}}">
		                        <div class="form-group col-md-12">
		                        	<label>wallet Address</label>
		                            <input type="text" name="withdrawal_address" class="form-control">
		                            @if($errors->first('withdrawal_address'))
						              <span class="text-danger">{{$errors->first('withdrawal_address')}}</span>
						            @endif
		                        </div>
		                        <span class="error-balance"></span>
		                        <div class="form-group col-md-12 mt-4">
		                        	<label>Amount</label>
		                            <input type="text" name="amount" id="amount" class="form-control" onkeyup="balanceCheked()" autocomplete="off">
		                            @if($errors->first('amount'))
						              <span class="text-danger">{{$errors->first('amount')}}</span>
						            @endif
		                        </div>
		                        
		                        <div class="form-group col-md-12">
		                            <button type="submit" class="btn btn-success mt-4 submit" id="submit">Withdraw</button>
		                        </div>
		                    </form>
		                </div>
		            </div>
		        </div>
		        <div class="col-sm-12">
		                <div class="card">
		                    <div class="card-header">
		                        <h5>List Buy Token</h5>
		                    </div>
		                    <div class="card-body">
		                        <div class="table-responsive">
		                            <table id="withdraw-table" class="display">
		                                <thead class="bg-theme">
						                  <tr>
						                      <th scope="col">Sr.</th>
						                      <th scope="col">Coin Name</th>
						                      <th scope="col">Address</th>
						                      <th scope="col">Txid</th>
						                      <th scope="col">Amount</th>
						                      <th scope="col">Status</th>
						                  </tr>
						                  </thead>
		                                <tbody>
		                                
		                                @php($i=1)

		                                @foreach($history as $h)
		                                <tr>
											<th scope="row">{{$i}}</th>
											<td>@if($h->coin_type == 'wallet')<span>VNJA</span>@else<span style="text-transform: uppercase;">{{$h->coin_type}}</span>@endif</td>
											<td>{{$h->address}}</td>
											<td>
												@if($h->txid)<span>{{ $h->txid }}</span> @else<span>Null</span>@endif 
											</td>
											<td>{{$h->amount}}</td>
											<td>
												@if($h->status == 0)
													<span class="text-warning"><b><i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Pending </b></span>
												@elseif($h->status == -1)
													<span class="text-danger"><b><i class="fa fa-ban" aria-hidden="true"></i> Rejected By Admin</b> </span>
												@elseif($h->status == -2)
													<span class="text-danger"><b><i class="fa fa-close" aria-hidden="true"></i> Cancelled </b></span>
												@elseif($h->status == 3)
													<span class="text-info"><b><i class="fa fa-check" aria-hidden="true"></i> Approveed By Addmin </b></span>
												@elseif($h->status == 1)
													<span class="text-success"><b><i class="fa fa-check-circle" aria-hidden="true"></i> Completed </b></span>
												@endif
											</td>
					                   	</tr>
					                   	@php($i++)
					                   	@endforeach

		                                </tbody>
		                            </table>
		                        </div>
		                    </div>
		                </div>
		            </div>
		   </div>
		</div>

</div>

@endsection

@section('script')
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#withdraw-table').DataTable();
} );
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#submit').attr('disabled',true);		
	} );

	var coin_blance = ({{ $coin_balance }});
	function balanceCheked(){
	var amount = $('#amount').val();
	if (0 < amount) {
		if (amount > coin_blance) {
			$('#submit').attr('disabled',true);
			$('.error-balance').html('<div class="text-danger"><b>You dont have sufficient balance to Withdraw.</b></div>');
			
		}
		else{
			$('#submit').attr('disabled',false);
			$('.error-balance').html('');
		}
	}		
	}
</script>
@endsection
<!--Common JS Plugin-->
<script type="text/javascript" src="{{ url('assets/home/js/jquery-1.12.4.min.js')}}"></script>
<script type="text/javascript" src="{{ url('assets/home/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/home/js/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/home/js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/home/js/jquery.counterup.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/home/js/imagesloaded.pkgd.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/home/js/jquery.magnific-popup.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/home/js/jquery.easing.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/home/js/scrolling-nav.js') }}"></script>
<!-- Waypoints -->
<script type="text/javascript" src="{{ url('assets/home/js/waypoints.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/home/js/waypoint.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/home/js/jquery.waypoints.js') }}"></script>
<!-- wow -->
<script type="text/javascript" src="{{ url('assets/home/js/wow.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/home/js/form-validator.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/home/js/contact-form-script.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/home/js/jquery.scrollUp.min.js') }}"></script>



<a id="scrollUp" href="#" style="position: fixed; z-index: 2147483647; display: none;">
	<i class="fa fa-arrow-up"></i>
</a>

<script>
  @if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
        
        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
  @endif
</script>


<!-- <script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
}

// $(document).ready(function(){
//     $('#:2.container')
// })

</script>

<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script> -->


<script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
  $(".google_translate_element").css("display:none");
}
</script>

<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<script type="text/javascript">
//   for (var i = 0; i < document.links.length; i++) {
//     if (document.links[i].href == document.URL) {
//         document.links[i].className += ' active';
//         document.links[i].parentElement.parentElement.parentElement.className += ' active';
//     }
// }
</script>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>

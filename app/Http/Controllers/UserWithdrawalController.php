<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserWithdrawalController extends Controller
{
    public function withdraw($coin)
    {
    	$coin_info['coin'] = $coin;

    	$user_id = Sentinel::getUser()->id;

    	if($coin == 'btc')
		{
			$coin_info['amount'] = Sentinel::getUser()->btc_balance;

			$history = Withdrawal::where('coin_type',$coin)->where('user_id',$user_id)->get();
		}
		elseif($coin == 'eth')
		{
			$coin_info['amount'] = Sentinel::getUser()->eth_balance;

			$history = Withdrawal::where('coin_type',$coin)->where('user_id',$user_id)->get();
		}
		elseif($coin == 'ltc')
		{
			$coin_info['amount'] = Sentinel::getUser()->ltc_balance;

			$history = Withdrawal::where('coin_type',$coin)->where('user_id',$user_id)->get();
		}
		elseif($coin == 'bch')
		{
			$coin_info['amount'] = Sentinel::getUser()->bch_balance;

			$history = Withdrawal::where('coin_type',$coin)->where('user_id',$user_id)->get();
		}
		elseif($coin == 'etc')
		{
			$coin_info['amount'] = Sentinel::getUser()->etc_balance;

			$history = Withdrawal::where('coin_type',$coin)->where('user_id',$user_id)->get();
		}
		elseif($coin == 'vcoin')
		{
			$coin_info['amount'] = Sentinel::getUser()->wallet_balance;

			$history = Withdrawal::where('coin_type',$coin)->where('user_id',$user_id)->get();
		}

		return view('dashboard.withdraw',compact('coin_info','history'));
    }

    public function do_withdraw(Request $request)
    {
    	$this->validate($request,[
            'withdrawal_address'=>'required',
            'amount'=>'required',
        ]);

        if($request->amount > 0)
        {
            $withdrawal = new Withdrawal();
            $withdrawal->user_id = Sentinel::getUser()->id;
            $withdrawal->coin_type = $request->coin;
            $withdrawal->address = $request->withdrawal_address;
            $withdrawal->amount = $request->amount;

            $user = User::find($withdrawal->user_id);

            if($request->coin == 'btc')
            {
                $user->btc_balance = $user->btc_balance - $request->amount;
            }
            elseif($request->coin == 'eth')
            {
                $user->eth_balance = $user->eth_balance - $request->amount;
            }
            elseif($request->coin == 'ltc')
            {
                $user->ltc_balance = $user->ltc_balance - $request->amount;
            }
            elseif($request->coin == 'bch')
            {
                $user->bch_balance = $user->bch_balance - $request->amount;
            }
            elseif($request->coin == 'etc')
            {
                $user->etc_balance = $user->etc_balance - $request->amount;
            }
            elseif($request->coin == 'vcoin')
            {
                $user->wallet_balance = $user->wallet_balance - $request->amount;
            }

            if($withdrawal->save() && $user->update())
            {
            	$notification = array(
                'message' => 'Your Withdrawal Request is generated', 
                'alert-type' => 'success'
            	);
            	
            	return redirect('user-wallet')->with($notification);
            }
            else
            {
            	$notification = array(
                'message' => 'Sorry Your Withdrawal Request is not generated!', 
                'alert-type' => 'error'
            	);

                return redirect()->back()->with($notification);
            }
        }
        else
        {
        	$notification = array(
            'message' => 'Please Enter Amount!', 
            'alert-type' => 'error'
        	);

            return redirect()->back()->with($notification);
        }
    }

    public function withdrawal_IpnHandler(Request $request)
    {
        Storage::disk('local')->put($request->item_name.'-'.$request->item_number.'.txt', json_encode($request->all()));

        $withdrawal = Withdrawal::where('address',$request->address);
      
        if($request->status >= 100)
        {
            if($request->item_number)
            {
                if($withdrawal)
                {
                    if($request->status >=100)
                    {
                        $withdrawal = Withdrawal::where('id',$request->item_number);
                        $userid = $withdrawal->user_id;
                    
                        $dp = Withdrawal::find($request->item_number);
                        $dp->status = 100;
                        $dp->amount = $request->received_amount;
                        $dp->save();
                        
                        $this->sendWithdrawalEmail($user,$request->received_amount);
                        die('IPN OK');
                    }
                }
            }
        }else if($request->status == -1){
             Withdrawal::where('txid',$request->item_number)->update(['status' => -1]);
        }
        else
        {
            
        }
    }

    public function sendWithdrawalEmail($user,$withdraw_amount)
    {
        Mail::send('emails.withdraw_success', 
            ['user' => $user,
             'withdraw_amount' => $withdraw_amount,
            ], 
            function ($m) use ($user,$withdraw_amount) {
                $m->to($user->email, $user->user_name)->subject('Vancoin Withdrawal Successfully');
        });
    }
}

@extends('dashboard_layouts.master')

@section('title') Deposit Bank | Vancoin @endsection

@section('style')
<style type="text/css" media="screen">
.error {
    margin: 0px!important;
    color: #ff2b2b!important;
}
p {
    font-size: 16px!important;
}   
</style>
@endsection

@section('content')

<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <h3>Deposit Bank</h3>
                </div>
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item">Dashboard</li>
                        <li class="breadcrumb-item">Wallet</li>
                        <li class="breadcrumb-item active">Deposit Bank</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row"> 
         @foreach($bank_details as $bank)
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header p-3">
                        <h5 class="card-title">Bank Details</h5>
                    </div>
                    <div class="card-body buy-token">
                        <div class="form-group col-md-12">
                            <label>Bank Name : <strong>{{ $bank->name }}</strong></label>
                        </div>
                        <div class="form-group col-md-12">
                            <label>Account No : <strong>{{ $bank->account_no }}</strong></label>
                        </div>
                        <div class="form-group col-md-12">
                            <label>SWIPT Code : <strong>{{ $bank->swipt_code }}</strong></label>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">  
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header p-3">
                        <h5 class="card-title">Deposit to Bank</h5>
                    </div>
                    <div class="card-body buy-token">
                        <form class="form-horizontal theme-form row" id="W-form" action="{{ url('amount-deposit-bank') }}" method="post">
                            {{ csrf_field() }}

                            <div class="form-group col-md-12">
                                <label for="paypal_token">Amount (USD)</label>
                                <input type="text" name="amount" class="form-control" autocomplete="off" placeholder="Enter Deposit USD Amount" maxlength="5">
                                @if($errors->has('amount'))
                                    <span class="text-danger">{{$errors->first('amount')}}</span>
                                @endif
                            </div>
                            <div class="form-group col-md-12">
                                <label for="currency_price">Transaction Id</label>
                                <input type="text" name="transaction_id" class="form-control" id="transaction_id" placeholder="Please type Transaction Id">
                                @if($errors->has('transaction_id'))
                                    <span class="text-danger">{{$errors->first('transaction_id')}}</span>
                                @endif
                            </div>
                            <div class="form-group col-md-12">
                                <p class="text-info"><b>Note:</b> Please give correct transaction id details.</p>
                            </div>
                            <div class="form-group col-md-12 text-right">
                                <button type="submit" class="btn btn-success mt-4" id="paypal-btn">Deposit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h5>Withdrawal History</h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="deposit" class="display">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Amount</th>
                                    <th>Tokens</th>
                                    <th>Transaction Id</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                   @php $i=1;  @endphp
                                   @foreach($deposit as $depos)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $depos->amount }}</td>
                                        <td>{{ $depos->amount / $usd_price }}</td>
                                        <td>{{ $depos->txid }}</td>
                                        <td>@if($depos->status == 0)<span class="badge badge-warning">Pending</span>
                                            @elseif($depos->status == 1)<span class="badge badge-success">Completed</span>
                                            @elseif($depos->status == -1)<span class="badge badge-danger">Rejected</span>
                                            @endif
                                        </td>
                                    </tr>
                                   @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection

@section('script')
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
$(document).ready(function() {
    $('#deposit').DataTable();
} );

// just for the demos, avoids form submit

$( "#W-form" ).validate({
  rules: {
    amount: {
      required: true,
      digits: true
    }
  }
});
</script>

@endsection
@extends('dashboard_layouts.master')

@section('title') Withdrawal | Vancoin @endsection

@section('style')
<style type="text/css" media="screen">
.error {
    margin: 0px!important;
    color: #ff2b2b!important;
}
p {
    font-size: 16px!important;
}	
</style>
@endsection

@section('content')

<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <h3>Withdrawal</h3>
                </div>
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                        <li class="breadcrumb-item active"> Withdrawal</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
   	<div class="container-fluid">
  	 	<div class="row">  
		    <div class="col-md-4">
		        <div class="card">
		            <div class="card-header p-3">
		                <h5 class="card-title">Withdrawal to Bank</h5>
		                <label for="balance" class="pull-right">Balance :  <strong>{{ $balance }} USD</strong> </label>
		            </div>
		            <div class="card-body buy-token">
		                <form class="form-horizontal theme-form row" id="W-form" action="{{ url('amount-withdrawal-bank') }}" method="post">
		                    {{ csrf_field() }}

		                    <div class="form-group col-md-12">
		                        <label for="paypal_token">Amount (USD)</label>
		                        <input type="text" name="amount" class="form-control" autocomplete="off" placeholder="Enter Withdrawal USD Amount" maxlength="5">
		                        @if($errors->has('amount'))
		                            <span class="text-danger">{{$errors->first('amount')}}</span>
		                        @endif
		                        <br>
		                        <span class="max-min-tokens"></span>
		                    </div>
		                    <div class="form-group col-md-12">
		                        <label for="currency_price">Bank Name</label>
		                        <input type="text" name="bank_name" class="form-control" id="bank_name" placeholder="Enter Bank Name">
		                        @if($errors->has('bank_name'))
		                            <span class="text-danger">{{$errors->first('bank_name')}}</span>
		                        @endif
		                    </div>
		                    <div class="form-group col-md-12">
		                        <label for="currency_price">Holder Name</label>
		                        <input type="text" name="holder_name" class="form-control" id="holder_name" placeholder="Enter Holder Name" autocomplete="off">
		                        @if($errors->has('holder_name'))
		                            <span class="text-danger">{{$errors->first('holder_name')}}</span>
		                        @endif
		                    </div>
		                    <div class="form-group col-md-12">
		                        <label for="paypal_total_price">Account No</label>
		                        <input type="text" class="form-control" name="account_no" id="account_no" placeholder="Enter Bank Account No" maxlength="25">
		                         @if($errors->has('account_no'))
		                            <span class="text-danger">{{$errors->first('account_no')}}</span>
		                        @endif
		                    </div>
		                    <div class="form-group col-md-12">
		                        <label for="currency_price">SWIPT Code</label>
		                        <input type="text" name="swipt_code" class="form-control " id="swipt_code" placeholder="Enter SWIPT Code" maxlength="11">
		                        @if($errors->has('swipt_code'))
		                            <span class="text-danger">{{$errors->first('swipt_code')}}</span>
		                        @endif
		                    </div>
		                    <div class="form-group col-md-12">
		                        <p class="text-info"><b>Note:</b> Please give correct bank details.</p>
		                    </div>
		                    <div class="form-group col-md-12 text-right">
		                        <button type="submit" class="btn btn-success mt-4" id="paypal-btn">Withdrawal</button>
		                    </div>
		                </form>
		            </div>
		        </div>
		    </div>
		    <div class="col-md-8">
		        <div class="card">
		            <div class="card-header">
		                <h5>Withdrawal History</h5>
		            </div>
		            <div class="card-body">
		                <div class="table-responsive">
		                    <table id="withdrawal" class="display">
		                        <thead>
		                        <tr>
		                            <th>#</th>
		                            <th>Bank Name</th>
		                            <th>Holder Name</th>
		                            <th>Account No</th>
		                            <th>SWIPT Code</th>
		                            <th>Amount</th>
		                            <th>Transaction Id</th>
		                            <th>Date</th>
		                            <th>Status</th>
		                        </tr>
		                        </thead>
		                        <tbody>
		                           @php $i=1; @endphp
		                           @foreach($withdraw as $withdr)
		                           		<tr>
		                           			<td>{{ $i++ }}</td>
		                           			<td>{{ $withdr->bank_name }}</td>
		                           			<td>{{ $withdr->holder_name }}</td>
		                           			<td>{{ $withdr->account_no }}</td>
		                           			<td>{{ $withdr->swipt_code }}</td>
		                           			<td>{{ $withdr->amount }}</td>
		                           			<td>@if($withdr->tid)
		                           					{{ $withdr->tid }}
		                           				@else
		                           					********
		                           				@endif
		                           			</td>
		                           			<td>{{ $withdr->created_at }}</td>
		                           			<td>@if($withdr->status == 0)<span class="badge badge-warning">Pending</span>
		                           				@elseif($withdr->status == 1)<span class="badge badge-success">Completed</span>
		                           				@elseif($withdr->status == -1)<span class="badge badge-danger">Rejected</span>
		                           				@endif
		                           			</td>
		                           		</tr>
		                           @endforeach
		                        </tbody>
		                    </table>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
</div>

@endsection

@section('script')
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
$(document).ready(function() {
	$('#withdrawal').DataTable();
} );

// just for the demos, avoids form submit

$( "#W-form" ).validate({
  rules: {
    amount: {
      digits: true
    },account_no: {
      digits: true
    }
  }
});
</script>

@endsection
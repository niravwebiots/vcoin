@extends('dashboard_layouts.master')


@section('title') Network | Dashboard @endsection

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ url('assets/back/css/orgchart.css') }}">
@endsection

@section('content')

<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <h3>Network
                        <!-- <small>Vcoin Admin panel</small> -->
                    </h3>
                </div>
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="{{ url('user-dashboard') }}"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                        <li class="breadcrumb-item active">Network</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Network</h5>
                    </div>
                    <div class="card-body bg-network">
                        <div id="chart-container1"></div>
                    </div>
                </div>
            </div>
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-header">
                            <h5>Referral list</h5>
                        </div>
                        <div class="card-body table-responsive" data-intro="This is the name of this site">
                            <div class="user-status">
                                <table id="referral-table" class="display">
                                    <thead>
                                        <tr>
                                            <th width="20%">#</th>
                                            <th>Name</th>
                                            <th>Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php($i=1)
                                        @foreach($referrals as $ru)
                                            <tr>
                                                <td>{{$i}}</td>
                                                <td class="text-uppercase">{{ $ru[0]->buytoken_user->user_name }}</td>
                                                <td>{{ $ru->sum('referral_bonus')}}</td>
                                            </tr>
                                            @php($i++)
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script src="{{ url('assets/back/js/jquery.orgchart.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/back/js/tree.js') }}" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('#referral-table').DataTable();
} );
</script>

<script type="text/javascript">
      'use strict';

    (function($){

    $(function() {

        var user = '';
        var ref = '';
        var temp = '';

        $.get("{{url('getUserDataNetwork')}}", function(data, status){
          console.log(data);
          temp = jQuery.parseJSON(data);
          useNextLoad(); // dont use the value till the ajax promise resolves here
        });


        function useNextLoad(){
          var datascource = {
            'name': "{{ Sentinel::getUser()->email }}",
            'title': 'Parent User',
            'children':  temp
          };

          var oc = $('#chart-container1').orgchart({
              'data' : datascource,
              'nodeContent': 'title'
          });
        }

    });

})(jQuery);

</script>

@endsection
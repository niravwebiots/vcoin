
<div id="home" class="header-area">
	<!-- nav bar -->
	<nav id="mainNav" class="navbar navbar-default navbar-default-2 fixed-header">
    <div>
      
    <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/home') }}">
              <img src="{{ URL::asset('assets/home/images/logo.png') }}" alt="logo">
            </a>
          </div>
        <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
          
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li class="menu-item-has-children {{ (Request::is('/') ? 'active ' : '') }}"><a class="nav-link js-scroll-trigger" href="/">Home</a></li>
              <li class="menu-item"><a class="nav-link js-scroll-trigger" href="{{ URL::asset('assets/home/Accredited_Investors.pdf') }}" download="" title="Accredited Investors">Accredited-Investors</a></li>
              <li class="nav-item {{ (Request::is('contact-us') ? 'active ' : '') }}"><a class="nav-link js-scroll-trigger" href="{{ url('contact-us') }}">Contact Us</a></li>
              <li class="nav-item {{ (Request::is('/#feature') ? 'active ' : '') }}"><a class="nav-link js-scroll-trigger" href="{{ url('/') }}#feature">FAQ</a></li>

              <li class="nav-item {{ (Request::is('/#news') ? 'active ' : '') }}"><a class="nav-link js-scroll-trigger" href="{{ url('/') }}#news">News</a></li>

              <li class="nav-item {{ (Request::is('/#blogs') ? 'active ' : '') }}"><a class="nav-link js-scroll-trigger" href="{{ url('/') }}#blogs">Blog</a></li>

              <li class="nav-item {{ (Request::is('/#roadmap') ? 'active ' : '') }}"><a class="nav-link js-scroll-trigger" href="{{ url('/') }}#roadmap">Roadmap</a></li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  White Paper <i class="fa fa-angle-down"></i>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" target="_blank" href="{{ URL::asset('assets/home/Whitepaper/ICO_english.pdf') }}">English</a>
                  <a class="dropdown-item" target="_blank" href="{{ URL::asset('assets/home/Whitepaper/ICO_arabic.pdf') }}">Arabic</a>
                  <a class="dropdown-item" target="_blank" href="{{ URL::asset('assets/home/Whitepaper/ICO_chinese_simplified.pdf') }}">Chinese (Simpl)</a>
                  <a class="dropdown-item" target="_blank" href="{{ URL::asset('assets/home/Whitepaper/ICO_chinese_trad.pdf') }}">Chinese (Trad)</a>
                  <a class="dropdown-item" target="_blank" href="{{ URL::asset('assets/home/Whitepaper/ICO_german.pdf') }}">German</a>
                  <a class="dropdown-item" target="_blank" href="{{ URL::asset('assets/home/Whitepaper/ICO_french.pdf') }}">French</a>
                  <a class="dropdown-item" target="_blank" href="{{ URL::asset('assets/home/Whitepaper/ICO_hebrew.pdf') }}">Hebrew</a>
                  <a class="dropdown-item" target="_blank" href="{{ URL::asset('assets/home/Whitepaper/ICO_japanese.pdf') }}">Japanese</a>
                  <a class="dropdown-item" target="_blank" href="{{ URL::asset('assets/home/Whitepaper/ICO_korean.pdf') }}">Korean</a>
                  <a class="dropdown-item" target="_blank" href="{{ URL::asset('assets/home/Whitepaper/ICO_portuguese.pdf') }}">Portuguese</a>
                  <a class="dropdown-item" target="_blank" href="{{ URL::asset('assets/home/Whitepaper/ICO_russian.pdf') }}">Russian</a>
                  <a class="dropdown-item" target="_blank" href="{{ URL::asset('assets/home/Whitepaper/ICO_spanish.pdf') }}">Spanish</a>
                  <a class="dropdown-item" target="_blank" href="{{ URL::asset('assets/home/Whitepaper/ICO_vietnamese.pdf') }}">Vietnamese</a>
                </div>
              </li>
              
               <!-- <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#">White Paper</a></li> -->
               
              @if(Sentinel::check())
                @if(Sentinel::getUser()->roles()->first()->slug == 'user')
              <li class="nav-item sign-in-btn" title="Dashboard"><a class="nav-link btn btn-red js-scroll-trigger" href="{{url('user-dashboard')}}" data-toggle="tooltip" title="Dashboard"> <i class="fa fa-tachometer"></i></a>


              </li>

                @elseif(Sentinel::getUser()->roles()->first()->slug == 'admin')
              <li class="nav-item sign-in-btn" title="Dashboard"><a class="nav-link btn btn-red js-scroll-trigger" href="{{url('admin-dashboard')}}" data-toggle="tooltip" title="Dashboard"><i class="fa fa-tachometer"></i> </a></li>@endif
              @else
              <li class="nav-item sign-in-btn"><a class="nav-link btn btn-red js-scroll-trigger" href="{{url('login')}}">Login</a></li>
              @endif
               
              <li>
                <span id="google_translate_element"></span>
              </li>
            </ul>                                             
          </div>
        </div>
    </div>
  </nav>
</div>

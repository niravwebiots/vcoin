<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use Mail;
use App\User;
use App\CoinPaymentsAPI;
use App\Models\Setting;
use App\Models\CoinAddress;
use App\Models\Deposit;
use App\Models\Withdraw;
use App\Models\Phase;

class DepositController extends Controller
{
    public function deposit($coin)
    {
    	// if(Sentinel::getUser()->kyc_status == 1)
    	// {
    		$setting = Setting::first();

            $user_id = Sentinel::getUser()->id;
            $deposit = CoinAddress::where('user_id',$user_id)->where('coin_type',$coin)->first();
            $deposit_history = Deposit::where('user_id',$user_id)->orderBy('id','desc')->get();

            if(!$deposit){
        		$cp_helper = new CoinPaymentsAPI();
                $setup = $cp_helper->Setup($setting->private_key,$setting->public_key);
                $req = array(
                    'currency' => $coin,
                    'ipn_url' => url('/ipn-handler'),
                    'buyer_email' => Sentinel::getUser()->email,
                );

             	$result=  $cp_helper->api_call('get_callback_address', $req);
              	$deposit = new CoinAddress();

             	if($result['error'] == 'ok') 
                {
                    $data = $result["result"];
                    $data['error'] = "success";
                    $deposit->address = $data['address'];
                    $deposit->user_id = $user_id;
                    $deposit->coin_type = $coin;
                    $deposit->save();

                    $qrcode = $this->generateGoogleQRCodeUrl('https://chart.googleapis.com/', 'chart', 'chs=200x200&chld=M|0&cht=qr&chl=',$data['address']);
                }
            }
                $qrcode = $this->generateGoogleQRCodeUrl('https://chart.googleapis.com/', 'chart', 'chs=200x200&chld=M|0&cht=qr&chl=',$deposit->address);
            
                return view('dashboard.deposit',compact('qrcode','deposit','deposit_history'));
            //}
    	// }
    	// else
    	// {
    	// 	$notification = array(
     //            'message' => 'Sorry Your kyc is pending.Please complete your kyc first!', 
     //            'alert-type' => 'error'
     //        );
     //        return redirect()->back()->with($notification);
    	// }
    }

    public function generateGoogleQRCodeUrl($domain, $page, $queryParameters, $qrCodeUrl) {
        $url = $domain .
        rawurlencode($page) .
        '?' . $queryParameters .
        urlencode($qrCodeUrl);

        return $url;
    }

    public function IpnHandler(Request $request)
    {
        if ($request->ipn_type == 'deposit') {
            if ($request->address) {
                $deposit= Deposit::where('txid',$request->txid)->first();

                $coin_add = CoinAddress::where('address',$request->address)->first();
                if ($coin_add) {

                  $user = User::where('id',$coin_add->user_id)->first();  
                    if(empty($deposit) && $user)
                    {
                        $deposit = new Deposit;
                        $deposit->user_id = $user->id;
                        $deposit->coin_type = $request->currency;
                        $deposit->address = $request->address;
                        $deposit->amount = $request->amount;
                        $deposit->txid = $request->txn_id;
                        $deposit->status = 0;
                        $deposit->save();
                    }
                }
           
                if ($request->status >= 100) {
                    if ($deposit) {
                        $user = User::where('id',$deposit->user_id)->first();

                        $coin = strtolower($request->currency);
                        $coins_balance = $coin.'_balance';

                        $user[$coins_balance] = $user[$coins_balance] + $request->amount;
                        $user->update();

                        Deposit::where('id',$deposit->id)->update(['status' => 2]);

                        $this->sendDepositEmail($user,$request->amount);
                    }
                }
                elseif ($request->status == -1) {
                    Deposit::where('id',$deposit->id)->update(['status' => -1]);
                }
            }
        }
       else if ($request->ipn_type == 'withdrawal') {
         $withdraw=Withdraw::where('callback_id',$request->id)->first();
         if($withdraw)
           {
             $user=User::where('id',$withdraw->user_id)->first();
             $coin = strtolower($request->currency);
             $coin_balance = $coin.'_balance';
             if($request->status==2 && $withdraw->status==0)///Approve withdraw order from coinpayment
             {
                $Withdraw = Withdraw::find($withdraw->id);
                $withdraw->status == 1;
                $withdraw->save();
             }
             else if($request->status==-1 && $withdraw->status==0){    ///cancel withdraw order from coinpayment
                $user = User::find($withdraw->user_id);
                $user[$coin_balance] = $user[$coin_balance] + $withdraw->amount;
                $user->update();
                $withdraw->status = -2;
                $withdraw->update();
             }
           }
       }   
    }


    // public function IpnHandler(Request $request)
    // {
    //     Storage::disk('local')->put($request->currency.'-------'.$request->txn_id.'.txt', json_encode($request->all()));

    //     if ($request->ipn_type = 'deposit') {
        
    //         $deposit = Deposit::where('address',$request->address)->where('txid','NULL')->where('status',0);
          
    //         if($request->status >= 100)
    //         {
    //             if($request->txn_id)
    //             {
    //                 if($deposit)
    //                 {
    //                     $deposit->status = 1;
    //                     $deposit->amount = $request->amount;
    //                     $deposit->address = $request->address;
    //                     $deposit->coin_type = $request->currency;
    //                     $deposit->txid = $request->txn_id;
    //                     $deposit->save();

    // 					$coin = strtolower($request->currency);
    //                     $coins_balance = $coin.'_balance';
    //                     $user = User::find($deposit->user_id);

    //                     $user[$coins_balance] = $user[$coins_balance] + $request->amount;
    //                     $user->save();
                                                
    //                     $this->sendDepositEmail($user,$request->amount); 
    //                 }
    //             }
    //         }else if($request->status == -1){
    //             Deposit::where('id',$request->txn_id)->update(['status' => 2]);
    //     }

    //         elseif ($request->ipn_type = 'withdrawal') {
              
    //         }
        
    //     }
    // }

    public function sendDepositEmail($user,$deposit_amount)
    {
        Mail::send('emails.deposit_success', 
            ['user' => $user,
             'deposit_amount' => $deposit_amount,
            ], 
            function ($m) use ($user,$deposit_amount) {
                $m->to($user->email, $user->user_name)->subject('Vancoin Deposit Successfully Add');
        });
    }


    public function approveddepositBank($id)
    {
        $deposit = Deposit::find($id);
        $deposit->status = 1;
        $deposit->update();

        $setting = Setting::first();

        $tokes = $deposit->amount / $setting->coin_rate; 
        $user = User::where('id',$deposit->user_id)->first();
        $user->wallet_balance = $user->wallet_balance + $tokes;
        $user->update();

        $phase = Phase::where('status',1)->first();
        $phase->total_coins = $phase->total_coins-$tokes;
        $phase->sold_coins = $phase->sold_coins+$tokes;
        $phase->update();

        $setting->total_coins = $setting->total_coins - $tokes; 
        $setting->sold_coins = $setting->sold_coins + $tokes; 
        $setting->update();

        // return view('emails.deposit_bank_approve',compact('user','deposit'));
        Mail::send('emails.deposit_bank_approve', 
            ['user' => $user,
             'deposit' => $deposit,
            ], 
            function ($m) use ($user,$deposit) {
                $m->to($user->email, $user->user_name)->subject('VNJA Deposit Approved Mail.');
        });

        $notification = array(
                'message' => 'Deposit approved successfull.', 
                'alert-type' => 'success'
            );
        return redirect()->back()->with($notification);
    }

    public function rejecteddepositBank($id)
    {
        $deposit = Deposit::find($id);
        $deposit->status = -1;
        $deposit->update();

        $user = User::find($deposit->user_id);
        // return view('emails.deposit_bank_reject',compact('user','deposit'));
        Mail::send('emails.deposit_bank_reject', 
            ['user' => $user,
             'deposit' => $deposit,
            ], 
            function ($m) use ($user,$deposit) {
                $m->to($user->email, $user->user_name)->subject('VNJA Deposit Reject Mail.');
        });

        $notification = array(
                'message' => 'Deposit rejected successfull.', 
                'alert-type' => 'success'
            );
        return redirect()->back()->with($notification);
    }

}

@extends('dashboard_layouts.master')

@section('title') Buy Token | Dashboard @endsection

@section('content')

<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <h3>Buy Token
                        <!-- <small>Vcoin Admin panel</small> -->
                    </h3>
                </div>
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                        <li class="breadcrumb-item active">Buy Token</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
       <div class="row"> 
           <!--  <div class="col-sm-6 col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h5>Total vancoin coin </h5>
                    </div>
                    <div class="card-body d-flex time-body text-center">
                        <h3>{{ $setting->total_coins }}</h3>
                    </div>
                </div>
            </div> -->

            <div class="col-sm-6 col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h5>Total Sold vancoin  coin</h5>
                    </div>
                    <div class="card-body d-flex time-body text-center">
                        <h3>{{ $setting->sold_coins }}</h3>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            
            <div class="col-md-6">
            	<div class="card">
                    <div class="card-header p-3">
                        <h5 class="card-title">Buy Token With Crypto Currency</h5>                    
                    </div>
                    <div class="card-body buy-token">
                        <form class="form-horizontal theme-form row" action="{{ url('buy-token') }}/{{$user->id}}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group col-md-12">
                                <label for="balance">Balance :- <span class="coinBalance text-danger">0</span> </label>
                                <input type="hidden" name="balance" id="balance" value="">
                                <select class="col-md-2 pull-right form-control" name="selectCoin" id="selectCoin" style="padding-left: 7px;">
                                  <option value="" selected=""> Select </option>
                                  <option value="BTC">BTC</option>
                                  <option value="ETH">ETH</option>
                                  <option value="BCH">BCH</option>
                                  <option value="LTC">LTC</option>
                                  <option value="ETC">ETC</option>
                                </select>
                            </div>
                            <br>
                            <span class="error-coin"></span>

                            <div class="form-group col-md-12">
                                <label for="purchase_token">Tokens</label>
                                <input type="text" name="tokens" class="form-control" id="purchasetoken" onkeyup="totalBalance()" onchange="checkBalance()" autocomplete="off">
                                @if($errors->has('tokens'))
                                    <span class="text-danger">{{ $errors->first('tokens') }}</span>
                                @endif
                                <br>
                                <span class="error-balance"></span>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="price">1 VNJA Price</label>
                                <input type="text" name="price" class="form-control" id="price" disabled="">
                                <div class="input-group-prepend">
    	                            <span class="input-group-text setcoinname" id="setcoinname">Coin</span>
    	                        </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="total_price">Total</label>
                                <input type="text" readonly="" class="form-control total" name="total_price" id="total_price">
                                <div class="input-group-prepend">
    	                            <span class="input-group-text setcoinname">Coin</span>
    	                        </div>
                            </div>
                                
                            <div class="form-group col-md-12 text-right">
                                <button type="submit" id="button-tok" class="btn btn-success mt-4">Buy</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card">
                    <div class="card-header p-3">
                        <h5 class="card-title">Buy Token With Fiat Currency</h5>
                    </div>
                    <div class="card-body buy-token">
                        <form class="form-horizontal theme-form row" action="{{ url('buy-paypal') }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group col-md-12">
                                <select class="col-md-2 pull-right form-control" name="currency" id="selectCurrency" style="padding-left: 7px;">
                                  <option value="" selected=""> Select </option>
                                  <option value="USD">USD</option>
                                  <option value="EUR">EUR</option>
                                  <option value="AUD">AUD</option>
                                  <option value="GBP">GBP</option>
                                  <option value="INR">INR</option>
                                  <option value="BRL">BRL</option>
                                  <option value="CAD">CAD</option>
                                  <option value="CZK">CZK</option>
                                  <option value="DKK">DKK</option>
                                  <option value="HKD">HKD</option>
                                  <option value="HUF">HUF</option>
                                  <option value="ILS">ILS</option>
                                  <option value="JPY">JPY</option>
                                  <option value="MYR">MYR</option>
                                  <option value="MXN">MXN</option>
                                  <option value="NOK">NOK</option>
                                  <option value="NZD">NZD</option>
                                  <option value="PHP">PHP</option>
                                  <option value="PLN">PLN</option>
                                  <option value="RUB">RUB</option>
                                  <option value="SGD">SGD</option>
                                  <option value="SEK">SEK</option>
                                  <option value="CHF">CHF</option>
                                  <option value="TWD">TWD</option>
                                  <option value="THB">THB</option>
                                </select>
                            </div>
                            <br>
                            <span class="error-currency"></span>
                            <div class="form-group col-md-12">
                                <label for="paypal_token">Tokens</label>
                                <input type="text" name="token" class="form-control" id="paypal_token" autocomplete="off" onkeyup ="totalprice()" maxlength="18">
                                @if($errors->has('token'))
                                    <span class="text-danger">{{$errors->first('token')}}</span>
                                @endif
                                <br>
                                <span class="max-min-tokens"></span>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="currency_price">1 VNJA Price</label>
                                <input type="text" name="price" class="form-control currencyPrice" id="currency_price" disabled="">

                                <div class="input-group-prepend">
                                    <span class="input-group-text setCurrency">Currency</span>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="paypal_total_price">Total</label>
                                <input type="text" class="form-control" name="total_price" id="paypal_total_price" readonly="">
                                <div class="input-group-prepend">
                                    <span class="input-group-text setCurrency">Currency</span>
                                </div>
                            </div>
                            <div class="form-group col-md-12 text-right">
                                <button type="submit" class="btn btn-success mt-4" id="paypal-btn">Buy With Paypal</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Buy Coin History</h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="ico-info" class="display">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Tokens</th>
                                    <th>Coin</th>
                                    <th>Amount</th>
                                    <th>Bonus</th>
                                    <th>Status</th>
                                    <th>Transaction Id</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1; ?>
                                    @foreach( $buytoken as $token )
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $token->tokens }}</td>
                                            <td>{{ $token->coin_type }}</td>
                                            <td>{{ $token->amount }}</td>
                                            <td>{{ $token->bonus }}</td>
                                            <td>
                                                @if($token->status == 0 )<span class="text-warning"><i class = "fa fa-spinner fa-spin"></i> Pending </span>
                                                @elseif($token->status == 1)<span class="text-success"><i class="fa fa-check" aria-hidden="true"></i> Complete </span>
                                                @elseif($token->status == 2)<span class="text-danger"><i class="fa fa-close" aria-hidden="true"></i> Cancelled </span>
                                                @endif
                                            </td>
                                            <td>{{ $token->txid }}</td>
                                            <td><?php $date = $token->created_at;
                                           echo $newDate = date("d-m-Y", strtotime($date));  ?></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('script')
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $('#paypal_token,#purchasetoken').keypress(function(event) {
      if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
      }
    });
   
$(document).ready(function() {
    $('#ico-info').DataTable();
    $('.error-balance').html(" ");

    $('#button-tok').attr('disabled',true);
    $('#paypal-btn').attr('disabled',true);
} );

</script>

<script>
    var coin_rate = ({{$setting->coin_rate}});
    var min_token = ({{$setting->min_buy_token}});
    var max_token = ({{$setting->max_buy_token}});

function totalBalance()
{
   var tokens = $('#purchasetoken').val();
   var price = $('#price').val();
   
       var total_price = 0;
       var input = document.getElementById ("purchasetoken");
            input.maxLength = 18; 
       total_price = price * tokens;
       $('#total_price').val(total_price.toFixed(8));
       $('.error-balance').html('');
       $('.error-coin').html('');
       $('#button-tok').attr('disabled',false);
       checkBalance();
}

function checkBalance(){
    var balance = parseFloat($('#balance').val());
    var total_bal = balance.toFixed(8);
    var total_amt = $('#total_price').val();
    var coin = $('#selectCoin').val();

    if(coin == '')
    {
        $('#button-tok').attr('disabled',true);
        $('.error-coin').html('<div class="text-danger"><b>Please Select coin.</b></div>');        
    }
    else{
        if(balance < total_amt)
        {
            $('#button-tok').attr('disabled',true);
            $('.error-balance').html('<div class="text-danger"><b>You dont have sufficient'+' '+ coin+ ' ' +'balance to buy !!</b></div>');
        }
    }
}

$("#selectCoin").change(function(){   // 1st
    var value = $(this).val();
    
    $.ajax({    //create an ajax request to display.php
        type: "post",
        url: "coin-balance",             
        dataType: "html",
        data: {
            'value': value,
            '_token': '{{ csrf_token() }}',
           //expect html to be returned                
        },
        success: function(response){ 
            if(response.length > 0){
                var res = $.parseJSON(response);
                $(".coinBalance").html(res['balance']);
                $(".setcoinname").html(value);
                $('#balance').val(res['balance']);
                var price =  coin_rate / res['coin_rate'];
               $('#price').val(price.toFixed(8));
               totalBalance();
            }else{
                $(".coinBalance").html(0);
                $(".setcoinname").html('Coin');
                $('#balance').val(0);
               $('#price').val(0);
               $('#total_price').val(0);
            }
        }
    }); 
});
</script>


<!--  Buy Token with Paypal -->
<script type="text/javascript">

function selectcurrency(){ 
    var selectCurrency = $('#selectCurrency').val();
    var tokens = $('#paypal_token').val();
    if (selectCurrency == '') {

        $('#paypal-btn').attr('disabled',true);
        $('.error-currency').html('<div class="text-danger"><b>Please Select Currency.</b></div>');
    }else{
        $('.max-min-tokens').html('');
        $('.error-currency').html('');
        $('#paypal-btn').attr('disabled',false);
    }

}

function totalprice(){
    var tokens = $('#paypal_token').val();
    var buytoken = $('#currency_price').val();
    var totalPrice = 0;

    
        totalPrice =  tokens * buytoken;
        $('#paypal_total_price').val(totalPrice.toFixed(18));
        selectcurrency();
}

$("#selectCurrency").change(function(){   // 1st
    var value = $(this).val();

    $.ajax({    //create an ajax request to display.php
        type: "post",
        url: "currency-price",             
        dataType: "html",
        data: {
            'value': value,
            '_token': '{{ csrf_token() }}',               
        },
        success: function(response){
        if(response.length > 0){
            var res = $.parseJSON(response);
            var price =  coin_rate * res['price'];
            $('.currencyPrice').val(price.toFixed(8));
            $(".setCurrency").html(value);
            totalprice();
        }else{
                $("#currency_price").html(0);
                $(".setCurrency").html('Currency');
                $("#paypal_total_price").html(0);
            }
        }
    });
});

</script>
 

@endsection
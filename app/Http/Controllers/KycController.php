<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use App\User;
use Sentinel;

class KycController extends Controller
{
    public function index()
    {
    	return view('dashboard.kyc');
    }

    public function kyc_upload(Request $request)
    {
    	$this->validate($request,[
    		'kyc_front' => 'required|mimes:jpeg,jpg,png',
    		'kyc_back' => 'required|mimes:jpeg,jpg,png',
    		'kyc_selfie' => 'required|mimes:jpeg,jpg,png'
    	]);

    	$current_user = Sentinel::getUser()->id;
        $user = User::find($current_user);

        if($user->kyc_front != "")
        {
            $image_path = public_path("assets/back/kyc/".$user->kyc_front);  // Value is not URL but directory file path
            if(File::exists($image_path)) {
                File::delete($image_path);
            }
        }

        if($user->kyc_back != "")
        {
            $image_path = public_path("assets/back/kyc/".$user->kyc_back);  // Value is not URL but directory file path
            if(File::exists($image_path)) {
                File::delete($image_path);
            }
        }

        if($user->kyc_selfie != "")
        {
            $image_path = public_path("assets/back/kyc/".$user->kyc_selfie);  // Value is not URL but directory file path
            if(File::exists($image_path)) {
                File::delete($image_path);
            }
        }

        $front = $request->file('kyc_front');
        $back = $request->file('kyc_back');
        $selfie = $request->file('kyc_selfie');

        $user->kyc_front = date('Ymd').time().$front->getClientOriginalName();
        $user->kyc_back = date('Ymd').time().$back->getClientOriginalName();
        $user->kyc_selfie = date('Ymd').time().$selfie->getClientOriginalName();
        $user->kyc_status = 3;

        $destinationPath = public_path("assets/back/kyc/");

       if($front->move($destinationPath,$user->kyc_front) && $back->move($destinationPath,$user->kyc_back) && $selfie->move($destinationPath,$user->kyc_selfie))
        {
        	$user->save();

        	$notification = array(
                'message' => 'Your kyc document is submited successfully. After Verification of Admin You Start deposit and withdrawal ', 
                'alert-type' => 'success'
            );

        	return redirect()->back()->with($notification);
        }
        else
        {
        	$notification = array(
                'message' => 'Sorry kyc document is not submited!', 
                'alert-type' => 'error'
            );

        	return redirect()->back()->with($notification);
        }
    }
}

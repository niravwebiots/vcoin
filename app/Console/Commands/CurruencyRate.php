<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CurruencyRate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:currency';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This Function use update currency rate.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ch = file_get_contents('https://v3.exchangerate-api.com/bulk/1398a271a0c617cd5f0f6672/USD');  
        $ch = (array)json_decode($ch);
    
        $ch = $ch['rates'];

        foreach ($ch as $key => $value) {

            // echo "{$key} => {$value} <br>";
           

           $currency = Currency::where('currency',$key)->first();
               if($currency){
                $currency->price = $value;
                $currency->list = 2;
                $currency->update();
            }

        }
        
    }

}

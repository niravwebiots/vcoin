<!-- latest jquery-->
<script src="{{ URL::asset('/assets/back/js/jquery-3.2.1.min.js') }}" ></script>
<!-- Bootstrap js-->
<script src="{{ URL::asset('/assets/back/js/bootstrap/popper.min.js') }}" ></script>
<script src="{{ URL::asset('/assets/back/js/bootstrap/bootstrap.js') }}" ></script>
<!-- Chart JS-->
<script src="{{ URL::asset('/assets/back/js/chart/Chart.min.js') }}"></script>
<!-- candile-chart js-->
<script src="{{ URL::asset('/assets/back/js/chart/candile-chart/amcharts.js') }}" ></script>
<!-- <script src="{{ URL::asset('/assets/back/js/chart/candile-chart/candile.all.js') }}" ></script> -->
<script src="{{ URL::asset('/assets/back/js/chart/candile-chart/serial.js') }}" ></script>
<script src="{{ URL::asset('/assets/back/js/chart/candile-chart/candile-script.js') }}" ></script>
<!-- Morris Chart JS-->
<script src="{{ URL::asset('/assets/back/js/chart/morris-chart/raphael.js') }}"></script>
<script src="{{ URL::asset('/assets/back/js/chart/morris-chart/morris.js') }}"></script>
<!-- owlcarousel js-->
<script src="{{ URL::asset('/assets/back/js/owlcarousel/owl.carousel.js') }}" ></script>
<!-- Sidebar jquery-->
<script src="{{ URL::asset('/assets/back/js/sidebar-menu.js') }}" ></script>
<!--Sparkline  Chart JS-->
<script src="{{ URL::asset('/assets/back/js/chart/sparkline/sparkline.js') }}"></script>
<!-- tour js -->
<!-- <script src="{{ URL::asset('/assets/back/js/tour/intro.js') }}" ></script> -->
<!-- <script src="../assets/js/tour/intro-init.js" ></script> -->
<!-- Theme js-->
<script src="{{ URL::asset('/assets/back/js/script.js') }}" ></script>
<!-- <script src="{{ URL::asset('/assets/back/js/dashboard-default.js') }}" ></script> -->
<!-- main theme customizer js -->
<!-- <script src="{{ URL::asset('/assets/back/js/theme-customizer/customizer.js') }}"></script> -->
<!-- Toastr Mesage -->
<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script> -->
<!-- datatable  -->
<script src="{{ URL::asset('/assets/back/js/datatables/jquery.dataTables.min.js') }}"></script>
<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script> -->
<!-- datatable  -->

<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src='{{ URL::asset("assets/back/js/toastr.min.js") }}' type='text/javascript'></script> 
<!-- InstantClick -->
<!-- <script src="{{ URL::asset('/assets/back/js/instantclick.js') }}" data-no-instant></script>
<script data-no-instant>InstantClick.init();</script> -->

<script>
  @if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
        
        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
  @endif
</script>

<script>
    // active link
for (var i = 0; i < document.links.length; i++) {
    if (document.links[i].href == document.URL) {
        document.links[i].className += ' active';
        document.links[i].parentElement.parentElement.parentElement.className += ' active';
    }
}
</script>
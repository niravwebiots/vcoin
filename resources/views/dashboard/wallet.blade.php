@extends('dashboard_layouts.master')


@section('title') Wallet | Vancoin @endsection

@section('content')

<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <h3>Wallet</h3>
                </div>
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                        <li class="breadcrumb-item active">Wallet</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Deposits & Withdrawals</h5>
                    </div>
                    <div class="card-body table-responsive" data-intro="This is the name of this site">
                        <div class="user-status">
                            <table id="dashboard-table" class="display">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th width="10%">Coin</th>
		                                <th width="20%">Name</th>
		                                <th width="40%">Total Balance</th>
		                                <th >Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td>BTC</td>
                                        <td class="text-uppercase">Bitcoin</td>
		                                <td>{{ Sentinel::getUser()->btc_balance }} BTC</td>
		                                <td>
		                                    <a href="{{ url('deposit/btc') }}" class="btn btn-success">Deposit</a>
                                            <a href="{{ url('withdraw/btc') }}" class="btn btn-success">Withdraw</a>
		                                </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>ETH</td>
                                        <td class="text-uppercase">Ethereum</td>
		                                <td>{{ Sentinel::getUser()->eth_balance }} ETH</td>
		                                <td>
                                            <a href="{{ url('deposit/eth') }}" class="btn btn-success">Deposit</a>
                                            <a href="{{ url('withdraw/eth') }}" class="btn btn-success">Withdraw</a>
		                                </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>LTC</td>
                                        <td class="text-uppercase">Litecoin</td>
                                        <td>{{ Sentinel::getUser()->ltc_balance }} LTC</td>
                                        <td>
                                            <a href="{{ url('deposit/ltc') }}" class="btn btn-success">Deposit</a>
                                            <a href="{{ url('withdraw/ltc') }}" class="btn btn-success">Withdraw</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>BCH</td>
                                        <td class="text-uppercase">Bitcoin Cash</td>
                                        <td>{{ Sentinel::getUser()->bch_balance }} BCH</td>
                                        <td>
                                            <a href="{{ url('deposit/bch') }}" class="btn btn-success">Deposit</a>
                                            <a href="{{ url('withdraw/bch') }}" class="btn btn-success">Withdraw</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>ETC</td>
                                        <td class="text-uppercase">Ethereum Classic</td>
                                        <td>{{ Sentinel::getUser()->etc_balance }} ETC</td>
                                        <td>
                                            <a href="{{ url('deposit/etc') }}" class="btn btn-success">Deposit</a>
                                            <a href="{{ url('withdraw/etc') }}" class="btn btn-success">Withdraw</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>VNJA</td>
                                        <td class="text-uppercase">VanCoin</td>
                                        <td>{{ Sentinel::getUser()->wallet_balance }} VNJA</td>
                                        <td>
                                            <a href="{{ url('withdraw/wallet') }}" class="btn btn-success">Withdraw</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Bank</td>
                                        <td class="text-uppercase">Bank</td>
                                        <td>0.0000000000</td>
                                        <td>
                                            <a href="{{ url('deposit-bank') }}" class="btn btn-success">Deposit</a><!-- 
                                            <a href="{{ url('withdraw-bank') }}" class="btn btn-success">Withdraw</a> -->
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    var t = $('#dashboard-table').DataTable({
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        
    });

    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    }   ).draw();

} );
</script>
@endsection
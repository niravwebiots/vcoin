<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="icon" href="{{ URL::asset('/assets/home/images/favicon.png') }}" type="image/x-icon"/>
	<link rel="shortcut icon" href="{{ URL::asset('/assets/home/images/favicon.png') }}" type="image/x-icon"/>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<style type="text/css">.gm-style-pbc{transition:opacity ease-in-out;background-color:rgba(0,0,0,0.45);text-align:center}.gm-style-pbt{font-size:22px;color:white;font-family:Roboto,Arial,sans-serif;position:relative;margin:0;top:50%;-webkit-transform:translateY(-50%);-ms-transform:translateY(-50%);transform:translateY(-50%)}
	</style>
	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title> @yield('title') </title>
	@include('home_layouts.head')

	@yield('style')
</head>
<body>
	@include('home_layouts.header')

	@yield('content')

	@include('home_layouts.footer')

	@include('home_layouts.footer_script')

	@yield('script')
</body>
</html>
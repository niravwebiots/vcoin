@extends('dashboard_layouts.master')


@section('title') Add New Phase | Vancoin @endsection

@section('style')
    <!-- DatePicker css -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/back/css/date-picker.css') }}">
@endsection()

@section('content')

<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <h3>Add New Phase
                        <!-- <small>Vancoin Admin panel</small> -->
                    </h3>
                </div>
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Add New Phase</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-xl-6">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <h5>Phase Information</h5>
                            </div>
                            <form method="post" action="{{ url('do-add-phase') }}" class="theme-form mega-form">
                                {{ csrf_field() }}
                                <div class="card-body">
                                    <div class="form-group">
                                        <label class="col-form-label">Phase Name</label>
                                        <input type="text" name="phase_name" class="form-control" placeholder="phase name">
                                        @if($errors->has('phase_name'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('phase_name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label  class="col-form-label">USD Price</label>
                                        <input type="text" name="usd_price" class="form-control" placeholder="Enter price in USD">
                                        @if($errors->has('usd_price'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('usd_price') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label">Vancoin</label>
                                        <input type="text" name="total_coins" class="form-control" placeholder="enter solding coins">
                                        @if($errors->has('total_coins'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('total_coins') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label">Bonus</label>
                                        <input type="text" name="bonus" class="form-control" placeholder="bonus coins">
                                        @if($errors->has('bonus'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('bonus') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label">Start Date</label>

                                        <input type="text" name="start_date" data-language="en" class="form-control datepicker-here digits" aria-describedby="basic-addon2" value="" autocomplete="off">

                                        @if($errors->has('start_date'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('start_date') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label">End Date</label>
                                        
                                        <input type="text" name="end_date" data-language="en" class="form-control datepicker-here digits" aria-describedby="basic-addon2" value="" autocomplete="off">

                                        @if($errors->has('end_date'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('end_date') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-primary">Submit</button>
                                    <a href="{{ url('phases') }}" class="btn btn-danger">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

<!--DateTime Picker js-->
<script src="{{ URL::asset('/assets/back/js/date-picker/datepicker.js') }}"></script>
<script src="{{ URL::asset('/assets/back/js/date-picker/datepicker.en.js') }}"></script>
<script src="{{ URL::asset('/assets/back/js/date-picker/datepicker.custom.js') }}"></script>

<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#dashboard-table').DataTable();
} );
</script>
@endsection
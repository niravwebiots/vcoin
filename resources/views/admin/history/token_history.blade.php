@extends('dashboard_layouts.master')


@section('title') Withdraw | History @endsection

@section('content')

<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <h3>Users Withdraw History</h3>
                </div>
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">History</li>
                        <li class="breadcrumb-item active">Withdraw</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5>Withdraw History</h5>
            </div>
            <div class="card-body table-responsive" data-intro="This is the name of this site">
                <div class="user-status">
                    <table id="withdraw-table" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>User Name</th>
                                <th>Coin / Currency</th>
                                <th>Tokens</th>
                                <th>Amount</th>
                                <th>Bonus</th>
                                <th>Transaction Id</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php $i=1; ?>
                        	@foreach($token as $tok)
                        		<tr>
                        			<td>{{ $i++ }}</td>
                        			<td>{{ $tok->buytoken_user->user_name }}</td>
                        			<td>{{ $tok->coin_type }}</td>
                        			<td>{{ $tok->tokens }}</td>
                        			<td>{{ $tok->amount }}</td>
                        			<td>{{ $tok->bomus }}</td>
                        			<td>{{ $tok->txid }}</td>
                        			<td>
                        				@if($tok->status == 0)
			                                <h6><span class="text-warning"><b><i class="fa fa-spinner fa-spin" aria-hidden="true"></i>  Pending </b></span></h6>
			                            @elseif($tok->status == 1)
			                                <span class="text-success"><b><i class="fa fa-check" aria-hidden="true"></i> Completed </b></span>
			                            @elseif($tok->status == 2)
			                                <span class="text-danger"><b><i class="fa fa-close" aria-hidden="true"></i> Cancelled </b></span>
			                            @else
			                            	<span></span>
			                            @endif
                        			</td>
                        		</tr>
                        	@endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#withdraw-table').DataTable();
} );
</script>
@endsection
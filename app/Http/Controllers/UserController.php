<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use App\User;
use App\CoinPaymentsAPI;
use App\Models\Setting;
use App\Models\CoinAddress;
use App\Models\Deposit;
use App\Models\Withdrawal;
use App\Models\Phase;
use App\Models\BuyToken;
use App\Models\BankDetails;
use DB;


class UserController extends Controller
{
    public function user_wallet()
    {
        $user_id = Sentinel::getUser()->id;
    	return view('dashboard.wallet',compact('coins_balance'));
    }

    public function withdraw($coin)
    {
    	
    	$user_id = Sentinel::getUser()->id;
        $coin_balance = $coin.'_balance';

			$user = Sentinel::getUser();
            $coin_balance = $user[$coin_balance];

			$history = Withdrawal::where('coin_type',$coin)->where('user_id',$user_id)->orderBy('id','desc')->get();

		return view('dashboard.withdraw',compact('coin_balance','history','coin','user'));
    }

    public function do_withdraw(Request $request)
    {
    	$this->validate($request,[
            'withdrawal_address'=>'required',
            'amount'=>'required',
        ]);

        $withdraw_amount = $request->amount;
        $coin = $request->coin;
        $coin_balance = $coin.'_balance';
        
        if($withdraw_amount > 0)
        {            
           $withdrawal = new Withdrawal();
            $withdrawal->user_id = Sentinel::getUser()->id;
            $withdrawal->coin_type = $coin;
            $withdrawal->address = $request->withdrawal_address;
            $withdrawal->amount = $withdraw_amount;
            $withdrawal->save();

            $withd_id = $withdrawal->user_id;
            
            $user = User::find($withd_id);
            $user[$coin_balance] = $user[$coin_balance] - $request->amount;
            $user->update();
           
        	$notification = array(
            'message' => 'Your Withdrawal Request is generated.', 
            'alert-type' => 'success'
        	);
        	
        	return redirect('user-wallet')->with($notification);
        }
        else{
            $notification = array(
            'message' => 'Your Balance is Low.', 
            'alert-type' => 'error'
            );

            return redirect()->back()->with($notification);
        }
    }

    public function withdrawal_IpnHandler(Request $request)
    {
        Storage::disk('local')->put($request->item_name.'-'.$request->item_number.'.txt', json_encode($request->all()));

        $withdrawal = Withdrawal::where('address',$request->address);
      
        if($request->status >= 100)
        {
            if($request->item_number)
            {
                if($withdrawal)
                {
                    if($request->status >=100)
                    {
                        $withdrawal = Withdrawal::where('id',$request->item_number);
                        $userid = $withdrawal->user_id;
                    
                        $dp = Withdrawal::find($request->item_number);
                        $dp->status = 100;
                        $dp->amount = $request->received_amount;
                        $dp->save();
                        
                        $this->sendWithdrawalEmail($user,$request->received_amount);
                        die('IPN OK');
                    }
                }
            }
        }else if($request->status == -1){
             Withdrawal::where('txid',$request->item_number)->update(['status' => -1]);
        }
        else
        {
            
        }
    }

    public function sendWithdrawalEmail($user,$withdraw_amount)
    {
        Mail::send('emails.withdraw_success', 
            ['user' => $user,
             'withdraw_amount' => $withdraw_amount,
            ], 
            function ($m) use ($user,$withdraw_amount) {
                $m->to($user->email, $user->user_name)->subject('Vancoin Withdrawal Successfully');
        });
    }

    public function ico_information()
    {
        $setting = Setting::first();
        $phases = Phase::where('is_delete','=','0')->get();
        $active_phase = Phase::where('is_delete','=','0')->where('status','=','1')->first();

        return view('dashboard.ico',compact('phases','setting','active_phase'));
    }

    public function network()
    {
        $user = Sentinel::getuser();
        $referral = BuyToken::with('buytoken_user')->where('refferral_id',$user->id)->get();
        $referrals = $referral->groupBy('user_id');

        return view('user.network',compact('referrals'));
    }

    public function getUserDataNetwork()
    {
        $i = 0;
        $j = 0;
        $user = Sentinel::getuser();
        $arr1 = array();
        $refData = User::where('status', 1)->where('refferral_id', $user->id)->get();
        foreach ($refData as $key => $value) {
            $arr1[$j]['name'] = $value['user_name'];
            $arr1[$j]['title'] = 'Ref. User';
            $arr1[$j]['className'] = 'middle-level';
            $j++;
        }
        print_r(json_encode($arr1));
    }

    public function bankDeposit()
    {
        $bank_details = BankDetails::where('status',1)->get();
        $usd_price = setting::first()->coin_rate;
        $deposit = Deposit::where('coin_type','bank')->orderBy('id','desc')->get();
        return view('user.bank.bank_deposit',compact('deposit','bank_details','usd_price'));
    }

    public function bankWithdrawal()
    {
        $withdraw = Withdrawal::where('coin_type','bank')->orderBy('id','desc')->get();
        $usd_price = setting::first()->coin_rate;
        $user_balance = Sentinel::getUser()->wallet_balance;
        $balance = $user_balance * $usd_price;
        return view('user.bank.bank_withdraw',compact('withdraw','balance'));
    }

    public function amountWithdrawalBank(Request $request)
    {
        $this->validate($request,[
            'amount'    =>  'required',
            'bank_name' =>  'required',
            'holder_name' => 'required',
            'swipt_code'   => 'required',
            'account_no'  => 'required',
        ]);
        $usd_amount = $request->amount;
        $user = Sentinel::getUser();
        $usd_price = setting::first()->coin_rate;
        $balance = $user->wallet_balance * $usd_price;

        if ($balance > $usd_amount ) {
            $token = $usd_amount / $usd_price;
            $user->wallet_balance = $user->wallet_balance - $token;
            $user->update();

            $withdraw = new Withdrawal;
            $withdraw->user_id = $user->id;
            $withdraw->coin_type = 'bank';
            $withdraw->amount = $request->amount;
            $withdraw->bank_name = $request->bank_name;
            $withdraw->holder_name = $request->holder_name;
            $withdraw->swipt_code = $request->swipt_code;
            $withdraw->account_no = $request->account_no;
            $withdraw->save();

            $notification = array(
            'message' => 'Withdrawal successfull. wating for admin confarmation.', 
            'alert-type' => 'success'
            );
        return redirect()->back()->with($notification);

        }
         $notification = array(
            'message' => 'Your Balance is Low.', 
            'alert-type' => 'error'
            );
        return redirect()->back()->with($notification);
    }

    public function amountDepositBank(Request $request)
    {
        $this->validate($request,[
            'amount'    =>  'required',
            'transaction_id'   =>   'required',
        ]);
        $user_id = Sentinel::getUser()->id;

        $deposit = new Deposit;
        $deposit->user_id = $user_id;
        $deposit->coin_type = 'bank';
        $deposit->amount = $request->amount;
        $deposit->txid = $request->transaction_id;
        $deposit->save();

         $notification = array(
            'message' => 'Deposit successfull. wating for admin confarmation.', 
            'alert-type' => 'success'
            );
        return redirect()->back()->with($notification);
    }
}
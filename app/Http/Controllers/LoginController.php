<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use Artisan;
use App\User;
use App\Models\Setting;
use App\Models\Phase;
use Session;

class LoginController extends Controller
{
    // user and admin login controller

	public function login()
	{
		return view('home_pages.login');
	}

    public function vcoinLogin(Request $request)
    {
        $this->validate($request,[
            'email' => 'required|email|max:255',
            'password' => 'required|min:6|max:255'
        ]);

        $credentials = [
            'email'    => $request->email,
            'password' => $request->password,
        ];
        
        $user = User::where('email',$request->email)->first();

        if ($user && $user->status == 0) {
            $notification = array(
                'message' => 'Your account is blocked. please contact customer service.', 
                'alert-type' => 'warning'
            );
            return redirect()->back()->with($notification);
        }
        elseif ($user && $user->is_delete == 1) {
            $notification = array(
                'message' => 'Your account is deleted.', 
                'alert-type' => 'error'
            );
            return redirect()->back()->with($notification);
        }
        elseif (!$user) {
            $notification = array(
                'message' => 'Your emails do not match account.', 
                'alert-type' => 'error'
            );
            return redirect()->back()->with($notification);
        }

            $user = Sentinel::authenticate($credentials);
            if ($user) {

                $slug = Sentinel::getUser()->roles()->first()->slug;
                if ($slug == 'admin') {
                    if (Sentinel::getUser()->google2fa_enable == 1) {

                        $request->session()->put('2fa:user:id', Sentinel::getUser()->id);
                        return redirect('2fa/validate');
                    }
                    return redirect('admin-dashboard');
                }
                elseif ($slug == 'user') {
                   if (Sentinel::getUser()->google2fa_enable == 1) {

                        $request->session()->put('2fa:user:id', Sentinel::getUser()->id);
                        return redirect('2fa/validate');
                    }
                    return redirect('user-dashboard');
                }

            }
            else{
                $notification = array(
                    'message' => 'Your password does not macth.', 
                    'alert-type' => 'error'
                );
            return redirect()->back()->with($notification);
            
            }

    }

    public function dologout()
    {
       Artisan::call('cache:clear');
      if(Sentinel::check())
      {
        if(Sentinel::getUser()->roles()->first()->slug == 'admin')
        {
          Sentinel::logout();
          return redirect('/login');
        }
        elseif(Sentinel::getUser()->roles()->first()->slug == 'user')
        {
          Sentinel::logout();
          return redirect('/');
        }
      }
      else
      {
      	Sentinel::logout();
        return redirect('/');
      }
    }

    public function forgot_password()
    {
    	return view('home_pages.check_email');
    }

    public function check_email(Request $request)
    {
    	$this->validate($request,[
    		'email' => 'required|email|max:255',
    	]);

    	$email = $request->email;

    	$user = User::where('email',$email)->first();

        if(!empty($user))
        {
            $random_token = str_random(40);

            $text = 'Vancoin Reset Password.';
    		$link = url('reset-password').'/'.$email.'/'.$random_token;

            Session::put('random_token',$random_token);
            Session::put('email',$email);
            Session::put('user_id',$user->id);
            
            return view('emails.reset_password',compact('text','user','link'));

            $this->sendResetpasswordMail($user,$link,$text);
        }
        else
        {
            $notification = array(
                'message' => 'Sorry this email is not found!', 
                'alert-type' => 'error'
            );
            return redirect('forgot-password')->with($notification);
        }
    }

    public function reset_password($user_email,$token)
    {
    	$random_token = Session::get('random_token');
    	$email = Session::get('email');
    	$user_id = Session::get('user_id');

    	if($random_token == $token && $email == $user_email && $user_id != "")
    	{
    		return view('home_pages.reset_password');
    	}
    	else
    	{
            $notification = array(
                'message' => 'Sorry this link not found!', 
                'alert-type' => 'error'
            );

    		return redirect('check-email')->with($notification);
    	}
    }

    public function sendResetpasswordMail($user,$link,$text)
    {
        Mail::send('emails.reset_password', 
            ['user' => $user,
             'link' => $link,
             'text' => $text,
            ], 
            function ($m) use ($user,$text) {
                $m->to($user->email, $user->user_name)->subject('Vcoin Reset Password Mail.');
        });
    }

    public function do_reset_password(Request $request)
    {
    	$this->validate($request, [
			'password' => 'required|min:6|max:254|confirmed'
		]);

		$user_id = Session::get('user_id');

		$user = new User;
        $user = User::where('id',$user_id)->first();
        $user->password = bcrypt($request->password);

        if($user->save())
        {
            $notification = array(
                'message' => 'Your Password is Reset Successfully', 
                'alert-type' => 'success'
            );

            return redirect('login')->with($notification);
        }
        else
        {
            $notification = array(
                'message' => 'Sorry Your Password is not Reset please try later!', 
                'alert-type' => 'error'
            );

            return redirect('forgot_password')->with($notification);
        }
    }
}

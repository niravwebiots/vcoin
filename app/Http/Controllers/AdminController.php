<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use App\User;
use Mail;

class AdminController extends Controller
{
    public function user_manage()
    {
    	$role = Sentinel::findRoleBySlug('user');

		$users = $role->users()->with('roles')->where('status', '!=' , 2)->orderBy('id','desc')->get();
    	return view('admin.user_manage', compact('users'));
    }

    public function userStatus(Request $request,$id)
    {

    	$user_status = User::find($id);
    	if($user_status->status == 0)
    	{
    		$user_status->status = 1;
    		$user_status->save();
    	}
    	elseif ($user_status->status == 1) {
    		$user_status->status = 0;
    		$user_status->save();	
    	}

    	$notification = array(
                'message' => 'Successfull updated '.$user_status->user_name.' status.', 
                'alert-type' => 'success'
            );

    	return redirect()->back()->with($notification);
    }

    public function userDelete(Request $request,$id)
    {
    	$user_delete = User::find($id);
    	$user_delete->is_delete = 1;
    	$user_delete->status = 2;
    	$user_delete->update();

    	$notification = array(
                'message' => 'Successfull delete user.', 
                'alert-type' => 'success'
            );
    	return redirect()->back()->with($notification);
    }

    public function userKyc($id)
    {
        $user = User::find($id);

        return view('admin.user_kyc',compact('user'));
    }

    public function kyc_approve($id)
    {
        $user = User::find($id);
        $user->kyc_status = 1;

        $link = url('login');
        $text = 'Vancoin KYC Approved Mail.';

        if($user->update())
        {
            // return view('emails.kyc_approve',compact('user','link','text'));

            $this->sendKycApprovedMail($user,$link,$text);

            $notification = array(
                'message' => 'User KYC status is Updated Successfully', 
                'alert-type' => 'success'
            );
        }

        return redirect()->back()->with($notification);
    }

    public function sendKycApprovedMail($user,$link,$text)
    {
        Mail::send('emails.kyc_approve', 
            ['user' => $user,
             'link' => $link,
             'text' => $text,
            ], 
            function ($m) use ($user,$text) {
                $m->to($user->email, $user->user_name)->subject('Vancoin KYC Approved');
        });
    }

    public function kyc_reject($id)
    {
        $user = User::find($id);
        $user->kyc_status = 2;

        $link = url('login');
        $text = 'Vancoin KYC Reject Mail.';

        if($user->update())
        {
            // return view('emails.kyc_reject',compact('user','link','text'));
            $this->sendKycRejectMail($user,$link,$text);

            $notification = array(
                'message' => 'User KYC status is Updated Successfully', 
                'alert-type' => 'success'
            );
        }

        return redirect()->back()->with($notification);
    }

    public function sendKycRejectMail($user,$link,$text)
    {
        Mail::send('emails.kyc_approve', 
            ['user' => $user,
             'link' => $link,
             'text' => $text,
            ], 
            function ($m) use ($user,$text) {
                $m->to($user->email, $user->user_name)->subject('Vancoin KYC Rejected');
        });
    }
}

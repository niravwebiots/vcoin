@extends('dashboard_layouts.master')


@section('title') Settings | Settings @endsection

@section('style')
	<!-- DatePicker css -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/back/css/date-picker.css') }}">
@endsection()

@section('content')

<div class="page-body">
	    <!-- Container-fluid starts -->
	    <div class="container-fluid">
	        <div class="page-header">
	            <div class="row">
	                <div class="col-lg-6" data-intro="This is the name of this site">
	                    <h3>settings</h3>
	                </div>
	                <div class="col-lg-6" data-intro="This is the name of this site">
	                    <ol class="breadcrumb pull-right">
	                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
	                        <li class="breadcrumb-item active">Settings</li>
	                        <li class="breadcrumb-item active">Settings</li>
	                    </ol>
	                </div>
	            </div>
	        </div>
	    </div>
		<div class="container-fluid">
		   <div class="row">
		        <div class="col-md-6">
		        	<div class="card">
		            	<div class="card-header p-3">
		                    <h5 class="card-title">settings</h5>
		                </div>

		                <div class="card-body buy-token">
		                	<form class="form-horizontal theme-form row" action="{{ url('update-settings') }}" method="post">
		                		{{ csrf_field() }}
		                        <div class="form-group col-md-12">
		                        	ICO Start Date
		                        	<?php
		                        		$start_date = $settings->ico_start_date;
										$new_start_date = date("d/m/Y", strtotime($start_date));
		                        	?>
		                            
		                            <input type="text" name="ico_start_date" data-language="en" class="form-control datepicker-here digits" aria-describedby="basic-addon2" value="{{ $new_start_date }}">

		                            @if($errors->has('ico_start_date'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('ico_start_date') }}</strong>
                                    </span>
                                	@endif
		                        </div>

		                        <div class="form-group col-md-12 mt-2">
		                        	ICO End Date
		                        	<?php
		                        		$end_date = $settings->ico_end_date;
										$new_end_date = date("d/m/Y", strtotime($end_date));
		                        	?>

		                            <input type="text" name="ico_end_date" data-language="en" class="form-control datepicker-here digits" aria-describedby="basic-addon2" value="{{ $new_end_date }}">

		                            @if($errors->has('ico_end_date'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('ico_end_date') }}</strong>
                                    </span>
                                	@endif
		                        </div>
		                        
		                        <div class="form-group col-md-12 mt-2">
		                        	Total Coin
		                            <input type="text" name="total_coins" class="form-control" id="number" value="{{ $settings->total_coins }}">
		                            @if($errors->has('total_coins'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('total_coins') }}</strong>
                                    </span>
                                	@endif
		                        </div>
		                        
		                        <div class="form-group col-md-12 mt-2">
		                        	Sold Coins
		                            <input type="text" name="sold_coins" class="form-control" id="number" value="{{ $settings->sold_coins }}" readonly="">
		                            @if($errors->has('sold_coins'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('sold_coins') }}</strong>
                                    </span>
                                	@endif
		                        </div>
		                        
		                        <div class="form-group col-md-12 mt-2">
		                        	Rate
		                            <input type="text" name="vcoin_rate" class="form-control" readonly="" value="{{ $settings->coin_rate }}">
		                            @if($errors->has('vcoin_rate'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('vcoin_rate') }}</strong>
                                    </span>
                                	@endif
		                        </div>
		                        
		                       <!--  <div class="form-group col-md-12 mt-2">
		                        	Min Buy Token
		                            <input type="text" name="min_buy_token" class="form-control" id="number" value="{{ $settings->min_buy_token }}">
		                            @if($errors->has('min_buy_token'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('min_buy_token') }}</strong>
                                    </span>
                                	@endif
		                        </div> -->
		                        
		                        <!-- <div class="form-group col-md-12 mt-2">
		                        	Max Buy Token
		                            <input type="text" name="max_buy_token" class="form-control" id="number" value="{{ $settings->max_buy_token }}">
		                            @if($errors->has('max_buy_token'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('max_buy_token') }}</strong>
                                    </span>
                                	@endif
		                        </div> -->
		                        
		                        <div class="form-group col-md-12 mt-2">
		                        	Bonus
		                            <input type="text" name="bonus" class="form-control" id="number" value="{{ $settings->bonus }}">
		                            @if($errors->has('bonus'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('bonus') }}</strong>
                                    </span>
                                	@endif
		                        </div>
		                        
		                        <!-- <div class="form-group col-md-12 mt-2">
		                        	Referral Bonus
		                            <input type="text" name="referral_bonus" class="form-control" value="{{ $settings->referral_bonus }}">
		                            @if($errors->has('referral_bonus'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('referral_bonus') }}</strong>
                                    </span>
                                	@endif
		                        </div> -->
		                        
		                        <div class="form-group col-md-12 mt-2">
		                        	Private Key
		                            <input type="text" class="form-control" value="{{ $settings->private_key }}" readonly="">
		                        </div>
		                        
		                        <div class="form-group col-md-12 mt-2">
		                        	Public Key
		                            <input type="text" class="form-control" value="{{ $settings->public_key }}" readonly="">
		                        </div>

		                        <div class="form-group col-md-12 mt-2">
		                        	Merchant ID
		                            <input type="text" class="form-control" value="{{ $settings->merchant_id }}" readonly="">
		                        </div>
		                		
		                        <div class="form-group col-md-12 mt-2">
		                        	Secret Pin
		                            <input type="text" class="form-control" value="{{ $settings->secret }}" readonly="">
		                        </div>
		                        
		                        <div class="form-group col-md-12">
		                            <button type="submit" class="btn btn-success mt-4" onclick="return confirm('Are You Sure. You Want to Update Setting Details!')">Update Settings</button>
		                        </div>
		                    </form>
		                </div>
		            </div>
		        </div>
		   </div>
		</div>

</div>

@endsection

@section('script')
<script>
    $('#number').keypress(function(event) {
      if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
      }
    });

</script>
<!--DateTime Picker js-->
<script src="{{ URL::asset('/assets/back/js/date-picker/datepicker.js') }}"></script>
<script src="{{ URL::asset('/assets/back/js/date-picker/datepicker.en.js') }}"></script>
<script src="{{ URL::asset('/assets/back/js/date-picker/datepicker.custom.js') }}"></script>


@endsection
@extends('home_layouts.master')

@section('title') Contact us | Vancoin @endsection

@section('content')

<!-- start-contact-us-section -->
<div id="contact" class="contact-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 text-center">
                <div class="section-title section-title-2">
                    <h3 class="title">Contact Us</h3>
                    <p class="sub-title">Vanjia Limited</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="address-sec">
                    <h4 class="title"><i class="fa fa-envelope"></i> Mail </h4>
                    <div class="email"><p>vanjiagroup@gmail.com</p>
                    </div>
                    <h4 class="title"><!--<i class="fa fa-phone"></i> Contact --></h4>
                    <div class="phone"><!-- <p>+123 4567 8910</p>-->
                    </div>
                    
                </div>
                <div class="address-sec">
                    <h3 class="title"><i class="fa fa-map-marker" aria-hidden="true"></i> Address </h3>
                    <p>Room 31, Bld D, 9/F, Wong Kong Ind. Bldg, </p>
                    <p>192-198 Choi Hung Rd, San Po Kong,</p>
                    <p>Zip Code: 99907, Hong Kong.</p>
                </div>
            </div>
            
            <div class="col-md-6 p-0">
                <div class="google-map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3690.3898246402764!2d114.19674871523209!3d22.338904947136974!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x340406da13621a57%3A0xc3f066580394ea5e!2sWong+King+Industrial+Building%2C+192-198+Choi+Hung+Rd%2C+San+Po+Kong%2C+Hong+Kong!5e0!3m2!1sen!2sin!4v1540464277416" width="100%" height="350px" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end-contract-us-section -->

@endsection
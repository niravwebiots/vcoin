@extends('dashboard_layouts.master')

@section('title') Blogs Manage | Vancoin @endsection

@section('content')

<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <h3>Blogs Manage</h3>
                </div>
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Setting</li>
                        <li class="breadcrumb-item active">Blogs Manage</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
            	<a href="{{ route('blog-manage.create') }}" title="Latest blog Add" class="btn btn-primary">Add Blogs</a>
            </div>
            <div class="card-body table-responsive" data-intro="This is the name of this site">
                <div class="user-status">
                    <table id="blog-table" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Sub Title</th>
                                <th>Content</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        	@php $i=1; @endphp
                        	@foreach($blog as $blo)
                        		<tr>
                        			<td>{{ $i++ }}</td>
                        			<td>{{ $blo->title }}</td>
                        			<td>{{ $blo->sub_title }}</td>
                        			<td>{!! substr($blo["content"], 0, 340) !!}</td>
                        			<td><img src="{{ URL::asset('assets/back/blog') }}/{{ $blo->img }}" height="40px" width="40px" alt=""></td>
                        			<td><a href="{{ route('blog-manage.edit',$blo->id) }}" title="Edit" class="btn btn-info">Edit</a>
                        				<form action="{{ route('blog-manage.destroy',$blo->id) }}" method="post" accept-charset="utf-8">
                        					{{ csrf_field() }}
                        					{{ method_field('DELETE') }}
                        					<button type="delete" class="btn btn-danger mt-2">Delete</button>
                        				</form>
                        			</td>
                        		</tr>
                        	@endforeach                        	
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
	<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
	    $('#blog-table').DataTable();
	} );
	</script>
@endsection
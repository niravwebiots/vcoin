<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="universal admin is super flexible, powerful, clean & modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, universal admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="{{ URL::asset('/assets/home/images/favicon.png') }}" type="image/x-icon"/>
    <link rel="shortcut icon" href="{{ URL::asset('/assets/home/images/favicon.png') }}" type="image/x-icon"/>
    <title> 2FA | Authentication </title>

    <!--Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/back/css/fontawesome.css') }}">
    <!-- Owl css -->
    <!-- <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/back/css/owlcarousel.css') }}"> -->
    <!-- Bootstrap css -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/back/css/bootstrap.css') }}">
    <!-- App css -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/back/css/style.css') }}">
    <!-- Responsive css -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/back/css/responsive.css') }}">
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
    <style type="text/css">
        .p-4 {
            padding: 5.5rem !important;
        }
    </style>

</head>

<body>

<!--page-wrapper Start-->
<div class="page-wrapper">
    <div class="container-fluid">
        <!--sign up page start-->
        <div class="authentication-main">
            <div class="row">
                <div class="col-md-4 p-0">
                    <div class="auth-innerleft">
                        <div class="text-center">
                            <a href="{{ url('/') }}"><img src="{{ URL::asset('/assets/home/images/logo.png') }}" alt="logo"></a>
                            <hr>
                            <div class="social-media">
                                <ul class="list-inline">
                                    <li class="list-inline-item"><i class="fa fa-facebook" aria-hidden="true"></i></li>
                                    <li class="list-inline-item"><i class="fa fa-google-plus" aria-hidden="true"></i></li>
                                    <li class="list-inline-item"><i class="fa fa-twitter" aria-hidden="true"></i></li>
                                    <li class="list-inline-item"><i class="fa fa-instagram" aria-hidden="true"></i></li>
                                    <li class="list-inline-item"><i class="fa fa-rss" aria-hidden="true"></i></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 p-0">
                    <div class="auth-innerright">
                        <div class="authentication-box">
                            <h4>Enter the 6-digit code below from application:</h4>
                            
                            <div class="card mt-6 p-4">
                                <h5> Enter 2FA Authentication code </h5>
                                    <form class="mt-4 theme-form" action="{{ url('2fa/validate') }}" method="post">
                                        {{ csrf_field() }}
                                        <div class="row margin12" id="match-otp-2fa_enable">
                                            <div class="form-group col-md-12 ">
                                                <input type="text" class="form-control " placeholder="Enter One-Time Password" name="totp" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success">Submit</button>
                                        </div>
                                    </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!--sign up page Ends-->
    </div>

</div>
<!--page-wrapper Ends-->

<!-- latest jquery-->
<script src="{{ URL::asset('/assets/back/js/jquery-3.2.1.min.js') }}" ></script>
<!-- Bootstrap js-->
<script src="{{ URL::asset('/assets/back/js/bootstrap/popper.min.js') }}" ></script>
<script src="{{ URL::asset('/assets/back/js/bootstrap/bootstrap.js') }}" ></script>
<!-- owlcarousel js-->
<script src="{{ URL::asset('/assets/back/js/owlcarousel/owl.carousel.js') }}" ></script>
<!-- Sidebar jquery-->
<script src="{{ URL::asset('/assets/back/js/sidebar-menu.js') }}" ></script>
<!-- Theme js-->
<script src="{{ URL::asset('/assets/back/js/script.js') }}" ></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script>
  @if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;

        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
  @endif
</script>

</body>

</html>
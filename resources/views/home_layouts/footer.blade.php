<!-- start-footer-section -->
	
<section class="footer-section">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="widget widget-about">
					<div class="footer-logo">
						<a href="#">
							<img src="{{ URL::asset('assets/home/images/logo.png') }}" alt="logo">
						</a>
					</div>
					<div class="footer-content">
						<!-- <h3>Vanjia Limited</h3> -->
						<p class="sub-title">Contact with us</p>
						<p><i class="fa fa-envelope"></i> vanjiagroup@gmail.com</p>
						<!-- <p><i class="fa fa-phone"></i> +123 4567 8910</p> -->
					</div>
					<!-- <div class="social-area">
						<i class="fa fa-facebook"></i>
						<i class="fa fa-twitter"></i>
						<i class="fa fa-pinterest-p"></i>
						<i class="fa fa-linkedin"></i>
					</div> -->
				</div>
			</div>
			<div class="col-md-4">
				<div class="widget widget-subscribe">
					<h3 class="footer-title">Quick Links</h3>
					<div class="col-md-6 quick_links">
						<li><a href="{{ url('/') }}">Home</a></li>
						<li><a href="{{ url('/') }}#about_us">About us</a></li>
						<li><a href="{{ url('contact-us') }}">Contact us</a></li>
						<li><a href="{{ url('/') }}#feature">FAQ</a></li>
						<li><a href="{{ url('/') }}#roadmap">Roadmap</a></li>
						<li><a href="{{ url('/') }}#safety">News</a></li>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="widget widget-subscribe">
					<h3 class="footer-title"><i class="fa fa-map-marker" aria-hidden="true"></i> Address</h3>
					<p class="content">Room 31, Bld D, 9/F, Wong Kong Ind. Bldg,<br>192-198 Choi Hung Rd, San Po Kong,<br>Zip Code: 99907, Hong Kong.</p>

					<div class="footer-search" id="mc_embed_signup">
						<form action="#" method="get" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Your Email Address" required="">
                                    <div class="input-group-addon">
                                        <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="mc-button btn">
                                    </div>
                                </div>
                                <div id="subscribe-result"></div>
                            </div>
                        </form>
                    </div>
				</div>
			</div>
			
		</div>
	</div>
	<div class="footer-bottom">
		<div class="container-fluid no-pad">
			<div class="col-sm-12">
				<div class="copyright">
					<p>@Copyright 2018-2019 Vancoin</p>
				</div>
			</div>
		</div>
	</div>
</section>
	

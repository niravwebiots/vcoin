<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Withdrawal;
use App\User;
use App\Models\Setting;
use App\CoinPaymentsAPI;
use Mail;

class WithdrawalController extends Controller
{
    public function withdraw_manage()
    {
        $withdrawals = Withdrawal::with('users')->orderBy('id','desc')->get();
        return view('admin.withdraw_manage',compact('withdrawals'));
    }

    public function withdraw_approval($wid)
    {
        $withdraw = Withdrawal::find($wid);
        if ($withdraw->coin_type == 'wallet') {
           $withdraw->status = 3;
           $withdraw->save();

           $notification = array(
                    'message' => 'This withdraw Approved Successfully.', 
                    'alert-type' => 'success'
                );
            return redirect()->back()->with($notification);
        }
        $setting = Setting::first();

        $cp_helper = new CoinPaymentsAPI();
        $setup = $cp_helper->Setup($setting->private_key,$setting->public_key);

        $ipnhandler=url('/ipn-handler');
        $result = $cp_helper->CreateWithdrawal($withdraw->amount, $withdraw->coin_type, $withdraw->address,$ipnhandler);

        if ($result['error'] == 'ok') 
        {
            Storage::disk('local')->put('withdraw - new.txt', json_encode($result));
              // $withdraw->callback_status=$result['result']['status'];
               $withdraw->callback_id=$result['result']['id'];   //user when IPN call match
              // $withdraw->withdraw_id=$result['result']['id'];   //user when IPN call match
               $withdraw->status=3;      //approve by admin
               $withdraw->save();

               $notification = array(
                    'message' => 'This withdraw Approved Successfully.', 
                    'alert-type' => 'success'
                );
            return redirect()->back()->with($notification);
        }
        else
        {
            $notification = array(
                    'message' => $result['error'], 
                    'alert-type' => 'error'
                );
            return redirect()->back()->with($notification);
        }
    }

    public function withdraw_reject($wid)
    {
        $withdraw = Withdrawal::where('id',$wid)->first();
        $coin = $withdraw->coin_type;
        $coin_balance = $coin.'_balance';

        if($withdraw->completed == 0 || $withdraw->status == 0)
        { 
            $user = User::find($withdraw->user_id);
            $user[$coin_balance] = $user[$coin_balance] + $withdraw->amount;
            $user->update();
            $withdraw->status = -1;
            $withdraw->update();
            
            $notification = array(
                'message' => 'This withdraw Rejected Successfully', 
                'alert-type' => 'success'
            );
            return redirect('withdraw-manage')->with($notification);
        }

        elseif($withdraw->completed == 1)
        {
            $notification = array(
                'message' => 'This withdraw Already Approved!', 
                'alert-type' => 'error'
            );

            return redirect('withdraw-manage')->with($notification);
        }
        
    }

    public function withdrawBank($id)
    {
        $withdraw = Withdrawal::find($id);
        return view('admin.withdraw_approve',compact('withdraw'));
    }

    public function withdrawApproveBank(Request $request,$id)
    {
        $this->validate($request,[
            'transaction_id'   =>  'required',
        ]);

        $withdraw = Withdrawal::find($id);
        $withdraw->txid = $request->transaction_id;
        $withdraw->status = 1;
        $withdraw->save();

        $user_id = $withdraw->user_id;
        $user = User::find($user_id);

        // return view('emails.withdraw_aaprove',compact('user','withdraw'));
        Mail::send('emails.withdraw_aaprove', 
            ['user' => $user,
             'withdraw' => $withdraw,
            ], 
            function ($m) use ($user,$withdraw) {
                $m->to($user->email, $user->user_name)->subject('VNJA Withdraw Approved Mail.');
        });

        $notification = array(
            'message' => 'This withdraw successfull.', 
            'alert-type' => 'success'
        );
        return redirect('withdraw-manage')->with($notification);
    }

    public function withdrawRejectBank($id)
    {
        $withdraw = Withdrawal::where('id',$id)->first();

        $usd_price = setting::first()->coin_rate;
        $user = User::find($withdraw->user_id);
        $token = $withdraw->amount / $usd_price;
        $user->wallet_balance = $user->wallet_balance + $token;
        $user->update();

        $withdraw->status = -1;
        $withdraw->update();

        // return view('emails.withdraw_reject',compact('user','withdraw'));
        Mail::send('emails.withdraw_reject', 
            ['user' => $user,
             'withdraw' => $withdraw,
            ], 
            function ($m) use ($user,$withdraw) {
                $m->to($user->email, $user->user_name)->subject('VNJA Withdraw Reject Mail.');
        });
        
        $notification = array(
            'message' => 'This withdraw Rejected.', 
            'alert-type' => 'success'
        );
        return redirect('withdraw-manage')->with($notification);
    }    
}

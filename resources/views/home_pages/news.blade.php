@extends('home_layouts.master')

@section('title') News | Vancoin @endsection

@section('style')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/home/css/news.css') }}">
@endsection

@section('content')
<div class="container">
    <div class="projcard-container ">

        <div class="col-md-12 ">
            <div class="news-page">
                <div class="row">
                    <div class="col-md-6">
                        <div class="post-module-1">
                            <div class="thumbnail">
                                <!-- <div class="date">{{ $news->created_at->format('d M y') }}
						<div class="day"></div>
						<div class="month"></div>
					</div> -->
                                <img src="{{ URL::asset('assets/back/news') }}/{{ $news->img }}" />
                            </div>

                        </div>
                    </div>
                    <div class="news-text">
                        <h3>{{ $news->title }}</h3>
                        <h5>{{ $news->sub_title }}</h5>
                        <p>
                            @php echo $news->content @endphp
                        </p>
                    </div>
                    <div class="text-center">
                        <a href="{{$news->link}}" class="btn btn-red">More Information</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 01, 2018 at 01:45 PM
-- Server version: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rapidcloudservices`
--

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `parent` varchar(255) NOT NULL,
  `priority` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `is_parent` int(2) NOT NULL,
  `is_admin` int(1) NOT NULL COMMENT '0- company, 1- Admin,2 customer',
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `parent`, `priority`, `url`, `icon`, `is_parent`, `is_admin`, `created_by`, `created_at`, `updated_at`) VALUES
(2, 'Branch', 'Settings', 1, 'branch-list', 'icon-settings', 0, 1, 1, NULL, NULL),
(3, 'User Details', 'Users', 4, 'myuser', '	\nicon-user', 0, 0, 1, NULL, NULL),
(4, 'User Leave', 'Users', 4, 'user-timing', 'icon-user', 0, 0, 1, NULL, NULL),
(5, 'User Salary', 'Users', 4, 'user-salary', 'icon-user', 0, 0, 1, NULL, NULL),
(6, 'Company', 'Companies', 2, 'company', 'icon-diamond', 0, 1, 1, NULL, NULL),
(7, 'Customer', '', 5, 'customer', 'icon-user', 0, 0, 1, NULL, NULL),
(9, 'Super User', 'Users', 3, 'superuser', 'icon-user', 1, 1, 1, NULL, NULL),
(10, 'Country', 'Other', 6, 'country', 'icon-paper-plane', 0, 1, 1, NULL, NULL),
(11, 'State', 'Other', 6, 'state', 'icon-paper-plane', 0, 1, 1, NULL, NULL),
(12, 'City', 'Other', 6, 'city', 'icon-paper-plane', 0, 1, 1, NULL, NULL),
(13, 'Frontend', 'Frontend', 5, 'front', 'icon-social-dribbble', 1, 1, 1, NULL, NULL),
(14, 'Reseller', 'Companies', 2, 'reseller', NULL, 0, 1, 1, NULL, NULL),
(15, 'Reseller User', 'Users', 3, 'reselleruser', 'icon-user', 0, 1, 1, NULL, NULL),
(16, 'Companies User', 'Users', 3, 'user', NULL, 0, 1, 1, NULL, NULL),
(21, 'Reminder', 'Reminder', 9, 'reminder', 'icon-bulb', 0, 0, 1, NULL, NULL),
(19, 'Appointment List', '', 7, 'appointment', 'icon-briefcase', 0, 0, 1, NULL, NULL),
(20, 'Schedule', '', 8, 'schedule', 'icon-bar-chart', 1, 0, 1, NULL, NULL),
(22, 'Announcement', '', 10, 'announcement', 'icon-layers', 0, 0, 1, NULL, NULL),
(23, 'Send SMS', 'Reminder', 9, 'send-sms', 'icon-bulb', 0, 0, 1, NULL, NULL),
(24, 'Department', 'Settings', 1, 'department-list', 'icon-settings', 0, 0, 1, '2017-03-15 23:46:20', '2017-03-16 13:14:23'),
(25, 'User Working Hours', 'Users', 4, 'cmp-timing', 'icon-user', 0, 0, 1, '2017-03-21 01:51:22', '2017-03-21 06:07:34'),
(26, 'Services', 'Settings', 1, 'service-list', 'icon-settings', 0, 0, 1, NULL, NULL),
(34, 'Insurance Reconciliation', 'Accounts', 12, 'insurance-reconciliation', 'icon-calculator', 0, 0, 1, NULL, NULL),
(28, 'Category', 'Settings', 1, 'category', 'icon-settings', 0, 0, 1, NULL, NULL),
(29, 'Invoice List', 'Accounts', 12, 'invoice', 'icon-calculator', 0, 0, 1, NULL, NULL),
(30, 'Outstanding List', 'Accounts', 12, 'outstanding-list', 'icon-calculator', 0, 0, 1, NULL, NULL),
(31, 'Materials', 'Settings', 1, 'material', 'icon-settings', 0, 0, 1, NULL, NULL),
(32, 'Medicine', 'Settings', 1, 'medicine', 'icon-settings', 0, 0, 1, NULL, NULL),
(33, 'Tests', 'Settings', 1, 'test', 'icon-settings', 0, 0, 1, NULL, NULL),
(37, 'Company Insurance', 'Insurance', 11, 'company-insurance', 'icon-briefcase', 0, 0, 1, NULL, NULL),
(35, 'Insurance Balance', 'Reports', 13, 'insurance-balance', 'icon-briefcase', 0, 0, 1, NULL, NULL),
(36, 'Test Report', 'Reports', 13, 'test-report', 'icon-briefcase', 0, 0, 1, NULL, NULL),
(38, 'Customer Insurance', 'Insurance', 11, 'customer-insurance', 'icon-briefcase', 0, 0, 1, NULL, NULL),
(39, 'Template', '', 14, 'template', 'icon-layers', 0, 0, 1, NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

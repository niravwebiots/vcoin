<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blog = Blog::orderBy('id','desc')->get();
        return view('admin.blogs.blog-index',compact('blog'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.blogs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request,[
        'title' =>  'required',
        'content' => 'required',
        'img' =>  'required|mimes:jpeg,png,jpg,gif,svg'  
       ]);

        $image = $request->file('img');
        $img_name = rand(11111, 99999);
        $path = public_path('/assets/back/blog/');
        $image->move($path, $img_name);

        $blog = new Blog;
        $blog->title = $request->title;
        $blog->sub_title = $request->sub_title;
        $blog->content = $request->content;
        $blog->img = $img_name;
        $blog->link = $request->link;
        $blog->save();

        $notification = array(
            'message' => 'Successfull created blog.', 
            'alert-type' => 'success'
        );
        return redirect('blog-manage')->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog,$id)
    {
        $blog = Blog::find($id);
        return view('admin.blogs.edit',compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Blog $blog, $id)
    {
        $this->validate($request,[
            'title' =>  'required',
            'content' =>  'required'
        ]);

        $blog = Blog::find($id);
        $blog->title = $request->title;
        $blog->sub_title = $request->sub_title;
        $blog->content = $request->content;
        $blog->link = $request->link;

        if ($request->file('img')) {
            try{
            $file_path = public_path().'/assets/back/blog/'.$blog->img;
            unlink($file_path);
                }
            catch (\ErrorException  $e) {
            // return $e->getMessage();
            }

            $image = $request->file('img');
            $img_name = rand(11111, 99999).'_'.$image->getClientOriginalName();
            $path = public_path('/assets/back/blog/');
            $image->move($path, $img_name);
            $blog->img = $img_name;
        }

        $blog->save();

        $notification = array(
            'message' => 'Successfull updated blog.', 
            'alert-type' => 'success'
        );
        return redirect('blog-manage')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog,$id)
    {
        $blog = Blog::find($id);
        $blog->delete();

        $notification = array(
            'message' => 'Successfull deleted blog.', 
            'alert-type' => 'success'
        );
        return redirect('blog-manage')->with($notification);
    }
}

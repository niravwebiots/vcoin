@extends('dashboard_layouts.master')


@section('title') Withdraw | History @endsection

@section('content')

<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <h3>Users Withdraw History</h3>
                </div>
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">History</li>
                        <li class="breadcrumb-item active">Withdraw</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5>Withdraw History</h5>
            </div>
            <div class="card-body table-responsive" data-intro="This is the name of this site">
                <div class="user-status">
                    <table id="withdraw-table" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>User Name</th>
                                <th>Coin Type</th>
                                <th>Address</th>
                                <th>Amount</th>
                                <th>Transaction id</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php $i=1; ?>
                        	@foreach($withdraw as $withd)
                            <tr>
                               <td>{{ $i++ }}</td>
                               <td>{{ $withd->users->user_name }}</td>
                               <td>@if($withd->coin_type == 'wallet')<span>VNJA</span>@else<span style="text-transform: uppercase;">{{$withd->coin_type}}</span>@endif</td>
                               <td>{{ $withd->address }}</td>
                               <td>{{ $withd->amount }}</td>
                               <td>@if($withd->txid)<span>{{ $withd->txid }}</span>@else<span>Null</span>@endif</td>
                               <td>
                                   @if($withd->status == 0)
                                        <span class="text-warning"><b><i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Pending </b></span>
                                    @elseif($withd->status == -1)
                                        <span class="text-danger"><b><i class="fa fa-ban" aria-hidden="true"></i> Rejected </b> </span>
                                    @elseif($withd->status == -2)
                                        <span class="text-danger"><b><i class="fa fa-close" aria-hidden="true"></i> Cancelled </b></span>
                                    @elseif($withd->status == 3)
                                        <span class="text-info"><b><i class="fa fa-check" aria-hidden="true"></i> Approveed  </b></span>
                                    @elseif($withd->status == 1)
                                        <span class="text-success"><b><i class="fa fa-check-circle" aria-hidden="true"></i> Completed </b></span>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#withdraw-table').DataTable();
} );
</script>
@endsection
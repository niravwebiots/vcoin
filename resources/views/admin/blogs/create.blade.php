@extends('dashboard_layouts.master')


@section('title') Blogs Manage | Vancoin @endsection

@section('content')
<div class="page-body">
<!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <h3>Add New Blogs
                        <!-- <small>Vancoin Admin panel</small> -->
                    </h3>
                </div>
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Add New Blogs</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Add Latest Blogs</h5>
                    </div>
                    <form method="post" action="{{ route('blog-manage.store') }}" class="theme-form mega-form" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="card-body">
                            <div class="form-group">
                                <label class="col-form-label">Title</label>
                                <input type="text" name="title" class="form-control" placeholder="Enter Blogs Title..." >
                                @if($errors->has('title'))
                                <span class="text-danger">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label  class="col-form-label">Sub Title</label>
                                <input type="text" name="sub_title" class="form-control" placeholder="Enter Blogs Sub Title...">
                                @if($errors->has('sub_title'))
                                <span class="text-danger">
                                    <strong>{{ $errors->first('sub_title') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="col-form-label">Content</label>
                                <textarea name="content" class="form-control" id="editor1" cols="20" rows="15" value="{{ old('content') }}">{{ old('content') }}</textarea>
                                @if($errors->has('content'))
                                <span class="text-danger">
                                    <strong>{{ $errors->first('content') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="col-form-label">Image</label>
                                <input type="file" name="img" class="form-control">
                                @if($errors->has('img'))
                                <span class="text-danger">
                                    <strong>{{ $errors->first('img') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="col-form-label">Link</label>
                                <input type="text" name="link" class="form-control">
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-primary">Submit</button>
                            <a href="{{ url('blog-manage') }}" class="btn btn-danger">Cancel</a>
                        </div>
                    </form>
                </div>                    
            </div>
        </div>
    </div>

 </div>

@endsection

@section('script')
<script src="{{ URL::asset('assets/back/js/ckeditor/ckeditor.js') }}"></script>
<script src="{{ URL::asset('assets/back/js/ckeditor/styles.js') }}"></script>
<script src="{{ URL::asset('assets/back/js/ckeditor/adapters/jquery.js') }}"></script>
<script src="{{ URL::asset('assets/back/js/ckeditor/ckeditor.custom.js') }}"></script>
@endsection
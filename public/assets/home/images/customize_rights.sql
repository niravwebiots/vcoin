-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 01, 2018 at 01:45 PM
-- Server version: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rapidcloudservices_04-03-2017`
--

-- --------------------------------------------------------

--
-- Table structure for table `customize_rights`
--

DROP TABLE IF EXISTS `customize_rights`;
CREATE TABLE IF NOT EXISTS `customize_rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `departments` varchar(200) NOT NULL,
  `modules` varchar(255) NOT NULL,
  `menus` varchar(200) NOT NULL,
  `can_add` varchar(150) DEFAULT NULL,
  `can_edit` varchar(150) DEFAULT NULL,
  `can_delete` varchar(150) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customize_rights`
--

INSERT INTO `customize_rights` (`id`, `user_id`, `departments`, `modules`, `menus`, `can_add`, `can_edit`, `can_delete`, `created_at`, `updated_at`) VALUES
(1, 1, '1', '1', '2,6,9,10,11,12,13,14,15,16,17', '2,6,9,10,11,12,13,14,15,16,17', '2,6,9,10,11,12,13,14,15,16,17', '2,6,9,10,11,12,13,14,15,16,17', '2017-02-09 00:00:00', '2017-02-09 00:00:00'),
(2, 2, '1', '1', '6,14,15,16,17', '6,14,15,17', '6,14,15,16,17', '6,14,15,16,17', '2017-02-20 12:22:35', '2017-02-20 12:22:35'),
(3, 3, '1', '1', '3,6', '3,6', '3,6', '6', '2017-02-20 12:24:11', '2017-02-27 05:33:49'),
(4, 4, '1', '1', '6,16,17', '6,16,17', '6,16,17', '16', '2017-02-20 12:31:34', '2017-02-20 12:31:34'),
(5, 5, '1', '1', '2,3,4,5,8,7,19,20', '2,3,4,5,8,7,19,20', '2,3,4,5,8,7,19', '2,3,4,5,8,7', '2017-02-20 12:37:23', '2017-02-22 08:32:20'),
(6, 6, '1', '1', '2,8,7', '2,8,7', '2,8,7', '2,8,7', '2017-02-20 12:48:34', '2017-02-20 12:48:34'),
(7, 7, '1', '1', '2,8,7', '2,8,7', '2,8,7', '2,8,7', '2017-02-20 12:49:31', '2017-02-20 12:49:31'),
(8, 8, '1', '1', '2,3,4,5,8,7,19,20', '2,3,4,5,8,7,19,20', '2,3,4,5,8,7', '2,3', '2017-02-23 13:17:05', '2017-02-23 13:17:05'),
(10, 10, '1', '1', '2,6,15,16,17', '2,6,15,16,17', '2,6,15,16,17', '6,15,16,17', '2017-02-23 13:42:33', '2017-02-23 13:42:33'),
(11, 11, '1', '1', '6,15,16,17', '6,15,16,17', '6,15,16,17', '6,15,16,17', '2017-02-23 13:48:19', '2017-02-23 13:48:19'),
(12, 12, '1', '1', '3', '3', '3', NULL, '2017-02-24 11:51:22', '2017-02-24 11:51:22');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

@extends('home_layouts.master')

@section('title') Vancoin @endsection

@section('style')
<style type="text/css">
    .team-image{
        width: 100%;
    }
</style>
@endsection

@section('content')

<!-- banner area -->
<div class="banner-area banner-area-2">
    <marquee behavior="scroll" direction="alternate" scrollamount="5"> <!-- <img src="{{ URL::asset('assets/home/images/ban.png') }}" alt="img"> --><h1 class="headding"><strong> VANCOIN:</strong> HOTTEST ICO: ACCEPTS &nbsp; ACCREDITED INVESTORS ( USA, ASIA, EUROPE...)</h1></marquee>

    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-6">
                <h1 class="entry-title">VanCoin <br></h1>
                <h2 class="entry-sub-title">World Most Secure &amp; Easy Way</h2>
                <div class="social-icon">
                    <a href="https://web.facebook.com/Vanjialimited-926193294242678/?modal=admin_todo_tour" class="fa fa-facebook" target="_blank"></a>
                    <a href="https://twitter.com/van_jia" class="fa fa-twitter" target="_blank"></a>
                    <a href="https://plus.google.com/u/7/105764014004511590086" class="fa fa-google" target="_blank"></a>
                    <a href="https://www.linkedin.com/in/van-jia-ab7579174/" class="fa fa-linkedin" target="_blank"></a>
                </div>
                <div class="social-icon">
                    <a href="https://www.instagram.com/vanjia_limited/" class="fa fa-instagram" target="_blank"></a>
                    <a href="https://www.pinterest.com/vanjialimited/" class="fa fa-pinterest" target="_blank"></a>
                    <a href="https://www.youtube.com/channel/UCMT7lI8YYuCh_MjiIUNJOrA/videos?disable_polymer=1" class="fa fa-youtube" target="_blank"></a>
                    <a href="https://vanjialimited.tumblr.com/" class="fa fa-tumblr" target="_blank"></a>
                    <!-- <a href="https://old.reddit.com/user/VanJia/" class="fa fa-tumblr" target="_blank"></a> -->

                </div>
                <div class="social-icon">
                    <a href="https://forum.bitcoin.com/vanjia-u83092/" title="Bitcoin" class="fa fa-bitcoin" target="_blank"></a>
                    <a href="https://vanjiacoin.slack.com/" class="fa fa-slack" target="_blank"></a>
                    <a href="https://github.com/vanJia?tab=repositories" class="fa fa-github" target="_blank"></a>
                    <a href="https://bitcointalk.org/index.php?action=profile" title="Bitcointalk" class="fa fa-btc" target="_blank"></a>
                </div>
                <div class="social-icon">
                    <a href="https://www.reddit.com/user/VanJia" title="Bitcoin" class="fa fa-reddit" target="_blank"></a>
                    <a href="https://www.quora.com/profile/Jia-Van-1#" class="fa fa-quora" target="_blank"></a>
                </div>
                <a class="btn btn-white" href="{{ url('/') }}#about_us">About us</a>
            </div>
            <div class="col-md-6  col-lg-6 ">
                <div class="register-form text-center">
                   <div class="card-body text-center">
                   <!--  <h1>{{$phase->start_date}}</h1>
                    <h1>{{strtotime($phase->start_date)}}</h1>
                    <h1>{{strtotime(date('Y-m-d'))}}</h1> -->


                    @php $date = date('d-m-Y'); $start_date = date('d-m-Y', strtotime($phase->start_date));
                    $end_date = date('d-m-Y', strtotime($phase->end_date))
                     @endphp
                    
                    @if($date < $start_date)
                        @if($phase->phase_name)
                            <h2 class="title">{{ $phase->phase_name }} Start </h2>
                        @endif
                    @elseif($date > $start_date &&  $date < $end_date)  
                    <h2 class="title">{{ $phase->phase_name }} End</h2>
                    @else
                    <h2 class="title">{{ $phase->phase_name }} </h2>
                   
                    @endif

                    
                        <hr class="hr-line">
                        <ul class="timer">
                            <li><span id="days"></span><span class="timer">days</span></li>
                            <li><span class="timer-dots">:</span></li>
                            <li><span id="hours"></span><span class="timer">Hours</span></li>
                            <li><span class="timer-dots">:</span></li>
                            <li><span id="minutes"></span><span class="timer">Minutes</span></li>
                            <li><span class="timer-dots">:</span></li>
                            <li><span id="seconds"></span><span class="timer">Seconds</span></li>
                        </ul>
                        <hr>
                        <!-- <h3>Total Tokens:   {{ number_format($total_tokens) }}</h3> -->
                        <div class="mt-5">
                            <div class="col-sm-12"></div>
                          
                        </div>
                        <div class="text-center">
                            <a class="btn btn-white" href="/">Buy Tokens</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="king-box">
                        <div class="mt-2">
                         <img src="{{ URL::asset('assets/home/images/crown.png') }}" alt="img">
                         <h2 class="title">"<span>K</span>ing of crypto is vancoin,</h2>
                         <h3 class="title">The future belongs to Vancoin"</h3>
                         <h4 class="title pull-right">- Tian Jia, CFO</h4>
                     </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- banner area End -->
<!-- about area -->
<div class="about-area about-area-2" id="about_us">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title section-title-2">
                    <h2 class="title">ABOUT US</h2>
                    <h4 class="sub-title">Vancoin</h4>
                </div>
            </div>
            <div class="col-lg-6 text-center">
                <div class="about-video">
                    <video width="555" height="auto" controls>
                      <source src="{{ URL::asset('assets/home/video/vancoin.mp4') }}" type="video/mp4">
                      <source src="movie.ogg" type="video/ogg">
                      Your browser does not support the video.
                    </video>
                </div>
            </div>
            <div class="col-lg-6">
                <h3 class="entry-title">Everything You Need To Know!</h3>
                <p>Vancoin is using cryptocurrency to <a href="{{ url('register') }}">create</a> new horizons for consumer experience for our products and services. The Vancoin connects our products and services utilizing Vancoin to pay for education, real estate, jewelry and nursing homes. Early entry to our initial coin offerings could be a right decision as Vancoin has a limited supply but the demand for Vancoin is very high globally. To bring about better understanding of Vancoin, we have translated our White Paper into twelve different  languages. Now, all nationalities around the globe have the same opportunities to acquire our Vancoin. Our conglomerate, Vanjia Group, also benefits from this Initial Coin Offering. our next ambitious plans will be listing our company in major exchanges such as NYSE, LSE, HKSE and Frankfurt Stock Exchanges.</p>
            </div>
        </div>
    </div>
</div>
<!-- about area End -->



<div class="roadmap-section text-center" id="roadmap">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 text-center">
                <div class="section-title section-title-2">
                    <h2 class="title">Road Map</h2>
                    <h4 class="sub-title">( 2018-2019)</h4>
                </div>
            </div>
        </div>
        <div class="single-map">
            <div class="icons">
                <img src="{{ URL::asset('assets/home/images/11.png') }}" alt="img">
            </div>
            <div class="bars mbg-red"></div>
            <div class="details">
                <h3 class="title">Q.3. 2018</h3>
                <p>Drafted White Paper.</p>
            </div>
        </div>
        <div class="single-map single-map-2">
            <div class="icons">
                <img src="{{ URL::asset('assets/home/images/14.png') }}" alt="img">
            </div>
            <div class="bars mbg-gray"></div>
            <div class="details">
                <h3 class="title">Q.4. 2018</h3>
                <p>Initial Coin Offering (Private Sale & Public Sale).</p>
            </div>
        </div>
        <div class="single-map">
            <div class="icons">
                <img src="{{ URL::asset('assets/home/images/12.png') }}" alt="img">
            </div>
            <div class="bars mbg-black"></div>
            <div class="details">
                <h3 class="title">Q.1. 2019</h3>
                <p>Stock begins to trade at London Stock Exchange.</p>
            </div>
        </div>
        <div class="single-map single-map-2">
            <div class="icons">
                <img src="{{ URL::asset('assets/home/images/15.png') }}" alt="img">
            </div>
            <div class="bars mbg-red"></div>
            <div class="details">
                <h3 class="title">Q.2. 2019</h3>
                <p>Stock begins to trade at Frankfurt Stock Exchange.</p>
            </div>
        </div>
        <div class="single-map">
            <div class="icons">
                <img src="{{ URL::asset('assets/home/images/13.png') }}" alt="img">
            </div>
            <div class="bars mbg-gray"></div>
            <div class="details">
                <h3 class="title">Q.3. 2019</h3>
                <p>Stock begins trading at NYSE.</p>
            </div>
        </div>
        <div class="single-map single-map-2">
            <div class="icons">
                <img src="{{ URL::asset('assets/home/images/16.png') }}" alt="img">
            </div>
            <div class="bars mbg-yellow"></div>
            <div class="details">
                <h3 class="title">Q.4. 2019</h3>
                <p>Stock begins trading at HKSE.</p>
            </div>
        </div>
    </div>
</div>

<!-- start-video-section -->
<!-- <div class="video-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <a class="btn-video play-video" href="#">
                    <i class="fa fa-play" aria-hidden="true"></i>
                </a>
                <h2 class="title">Review with Video</h2>
                <p class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore the aliqua.
                    <br> Ut enim ad minim veniam, quis nostrud exercitation ullamco consequat.</p>
            </div>
        </div>
    </div>
</div> -->
<!-- end-video-section -->

<!--Features area -->
<div id="feature" class="feature-section">
    <div class="container">
        <!-- <div class="row row-eq-rs-height">
            <div class="col-md-12 col-sm-12 text-center">
                <div class="section-title section-title-2">
                    <h2 class="title">CoinAge Features</h2>
                    <h4 class="sub-title">Luscious For Your Business</h4>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 text-center">
                <div class="single-element">
                    <div class="thumb">
                        <img src="{{ URL::asset('assets/home/images/2(1).png') }}" alt="work">
                    </div>
                    <h4 class="entry-title"><a href="#">Safe And Secure</a></h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 text-center">
                <div class="single-element">
                    <div class="thumb">
                        <img src="{{ URL::asset('assets/home/images/6.png') }}" alt="work">
                    </div>
                    <h4 class="entry-title"><a href="#">Awesome Mobile App</a></h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 text-center">
                <div class="single-element">
                    <div class="thumb">
                        <img src="{{ URL::asset('assets/home/images/7.png') }}" alt="work">
                    </div>
                    <h4 class="entry-title"><a href="#">Secure Wallet</a></h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 text-center">
                <div class="single-element">
                    <div class="thumb">
                        <img src="{{ URL::asset('assets/home/images/8.png') }}" alt="work">
                    </div>
                    <h4 class="entry-title"><a href="#">Covered By Insurance</a></h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 text-center">
                <div class="single-element">
                    <div class="thumb">
                        <img src="{{ URL::asset('assets/home/images/9.png') }}" alt="work">
                    </div>
                    <h4 class="entry-title"><a href="#">Recurring Buying</a></h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 text-center">
                <div class="single-element">
                    <div class="thumb">
                        <img src="{{ URL::asset('assets/home/images/10.png') }}" alt="work">
                    </div>
                    <h4 class="entry-title"><a href="#">Instant Exchange</a></h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
                </div>
            </div>
        </div> -->
         <div class="col-md-12 col-sm-12 text-center">
                <div class="section-title section-title-2">
                    <h2 class="title">FAQs</h2>
                </div>
            </div>
        <!-- <p>In this example we have added a "plus" sign to each button. When the user clicks on the button, the "plus" sign is replaced with a "minus" sign.</p> -->
        <button class="accordion">When is the VNJA token Private sale ?</button>
        <div class="panel">
          <p>The VNJA token Private Sale is being held on January 8, 2019 to June 8, 2019.</p>
        </div>

        <button class="accordion">When is the VNJA token Public Sale ?</button>
        <div class="panel">
          <p>The VNJA TOKEN Public Sale is being held on June 9, 2019 to December 19,2019.</p>
        </div>

        <button class="accordion">When should I register ?</button>
        <div class="panel">
          <p>As soon as possible and as soon as registration opens.</p>
        </div>
        <button class="accordion">What is the minimum purchase amount for Institutional or Accredited Investors ?</button>
        <div class="panel">
          <p>$50,000</p>
        </div>
        <button class="accordion">What is the Private Sale per token price ?</button>
        <div class="panel">
          <p>The Private Sale per VNJA token price is $0.30.</p>
        </div>
        <button class="accordion">What is the Public Sale Per token Price ?</button>
        <div class="panel">
          <p>The Public Sale per VNJA token Price is $0.70.</p>
        </div>
        
        <button class="accordion">Which crypto-currencies are supported ?</button>
        <div class="panel">
          <p>You can buy VNJA tokens with Bitcoin, Bitcoin Cash, Ethereum Classic and Litecoin.</p>
        </div>
        <button class="accordion">What other payment's method are supported ?</button>
        <div class="panel">
          <p>We accepted PAYPAL and Bank's wire transfer.</p>
        </div>
        <button class="accordion">Do we accept token purchasers of "Accredited Investors" ?</button>
        <div class="panel">
          <p>Yes, we do. We have posted a separate Icon described who are qualified as Accredited Investors.</p>

        </div>
        <button class="accordion">What type of currency are currently supported by Paypal ?</button>
        <div class="panel">
          <p> Canadian dollar, Euro, British Pound, US dollar, Japanese yen, Australian dollar, New Zealand dollar, Swiss Franc, Hong Kong dollar, Singapore dollar, Swedish Krona, Danish Krone, Polish Zloty, Norwegian Krone, Hungarian forint, Czech Koruna, Israeli new Shekel, Mexian peso, Brazilian Real, Malaysian Ringgit, Philippine Peso, New Taiwan Dollar, Thai Baht, Turkish Lira, Russian Rouble. </p>
        </div>
        <button class="accordion"> What type of credit cards/debit cards are currently supported by Paypal ?</button>
        <div class="panel">
          <p> PayPal supports a large number of credit cards, including Visa, MasterCard, American Express, Bank of China/ JCB, Diner's Club, Discover. Check cards or debit cards with either a Visa or MasterCard logo are supported and treated just like a credit card.</p>
        </div>
        <button class="accordion"> Do we accepted Bank wire transfer payments ?</button>
        <div class="panel">
          <p> Yes. Please contact us by email for wire instruction: Email address: vanjiagroup@gmail.com.</p>
        </div>
        <button class="accordion"> When is the timing of our tokens of delivery ? </button>
        <div class="panel">
          <p> To maintain an efficient and stable market prices of our tokens, we will deliver our tokens on May 19,2019 at the end of the ICO public sale.</p>
        </div>
        <button class="accordion"> Do you have Coin Exchanges that are willing to accept your tokens ? </button>
        <div class="panel">
          <p> Yes, we have at least 6 Coin Exchanges that will accept our tokens for trading. These Coin Exchanges will create liquidity for buyers and sellers.</p>
        </div>
    </div>
</div>

<!-- start-Team-section -->
<div class="team-section">
    <div class="container">
        <div class="row ">
            <div class="col-md-12  text-center">
                <div class="section-title section-title-2">
                    <h2 class="title">Our Team</h2>
                    <h4 class="sub-title">OUR EXPERT</h4>
                </div>
            </div>
            <!-- team-single -->
            <div class="col-md-4">
                <div class="team-single">
                    <div class="entry-thumbnail">
                        <div class="team-overlay"></div>
                        <img src="{{ URL::asset('assets/home/team/Tian.jpg') }}" class="img-responsive team-image" alt="image">
                    </div>
                    <div class="entry-social">
                        <span><a href="#"><i class="fa fa-facebook"></i></a></span>
                        <span><a href="#"><i class="fa fa-twitter"></i></a></span>
                        <span><a href="http://www.linkedin.com/in/tianjia888"><i class="fa fa-linkedin"></i></a></span>
                        <span><a href="#"><i class="fa fa-tumblr"></i></a></span>
                    </div>
                    <div class="entry-content">
                        <h3 class="entry-title"><a href="http://www.linkedin.com/in/tianjia888">Tian Jia, LLM</a></h3>
                        <p class="entry-designation">Chief Financial Officer</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 ">
                <div class="team-single">
                    <div class="entry-thumbnail">
                        <div class="team-overlay"></div>
                        <img src="{{ URL::asset('assets/home/team/Aura.jpg') }}" class="img-responsive team-image" alt="image">
                    </div>
                    <div class="entry-social">
                        <span><a href="#"><i class="fa fa-facebook"></i></a></span>
                        <span><a href="#"><i class="fa fa-twitter"></i></a></span>
                        <span><a href="http://www.linkedin.com/in/cpauraaa009"><i class="fa fa-linkedin"></i></a></span>
                        <span><a href="#"><i class="fa fa-tumblr"></i></a></span>
                    </div>
                    <div class="entry-content">
                        <h3 class="entry-title"><a href="http://www.linkedin.com/in/cpauraaa009"> Aura Jayne Marcos, C.P.A.</a></h3>
                        <p class="entry-designation">Asian Regional Director</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 ">
                <div class="team-single">
                    <div class="entry-thumbnail">
                        <div class="team-overlay"></div>
                        <img src="{{ URL::asset('assets/home/team/Carie.jpg') }}" class="img-responsive team-image" alt="image">
                    </div>
                    <div class="entry-social">
                        <span><a href="#"><i class="fa fa-facebook"></i></a></span>
                        <span><a href="#"><i class="fa fa-twitter"></i></a></span>
                        <span><a href="https://www.linkedin.com/in/carie-emerald-lim-37ab4216b"><i class="fa fa-linkedin"></i></a></span>
                        <span><a href="#"><i class="fa fa-tumblr"></i></a></span>
                    </div>
                    <div class="entry-content">
                        <h3 class="entry-title"><a href="https://www.linkedin.com/in/carie-emerald-lim-37ab4216b">Carie Emerald Pecaoco Lim, RPh, JD</a></h3>
                        <p class="entry-designation">Chief Legal Officer</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 ">
                <div class="team-single">
                    <div class="entry-thumbnail">
                        <div class="team-overlay"></div>
                        <img src="{{ URL::asset('assets/home/team/Jose.jpg') }}" class="img-responsive team-image" alt="image">
                    </div>
                    <div class="entry-social">
                        <span><a href="#"><i class="fa fa-facebook"></i></a></span>
                        <span><a href="#"><i class="fa fa-twitter"></i></a></span>
                        <span><a href="http:www.linkedin.com/in/jhcibils/"><i class="fa fa-linkedin"></i></a></span>
                        <span><a href="#"><i class="fa fa-tumblr"></i></a></span>
                    </div>
                    <div class="entry-content">
                        <h3 class="entry-title"><a href="http:www.linkedin.com/in/jhcibils/">Jose Hernan Cibils</a></h3>
                        <p class="entry-designation">(German, Spanish, English)</p>
                        <p class="entry-designation">European Regional Director</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 ">
                <div class="team-single">
                    <div class="entry-thumbnail">
                        <div class="team-overlay"></div>
                        <img src="{{ URL::asset('assets/home/team/Mohammed.jpg') }}" class="img-responsive team-image" alt="image">
                    </div>
                    <div class="entry-social">
                        <span><a href="#"><i class="fa fa-facebook"></i></a></span>
                        <span><a href="#"><i class="fa fa-twitter"></i></a></span>
                        <span><a href="http://linkedin.com/in/mohammed-mensan-405637170"><i class="fa fa-linkedin"></i></a></span>
                        <span><a href="#"><i class="fa fa-tumblr"></i></a></span>
                    </div>
                    <div class="entry-content">
                        <h3 class="entry-title"><a href="http://linkedin.com/in/mohammed-mensan-405637170">Mohammed Elsayed Elmensan</a></h3>
                        <p class="entry-designation">(Arabic, Hebrew, English)</p>
                        <p class="entry-designation">Middle East Regional Director</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 ">
                <div class="team-single">
                    <div class="entry-thumbnail">
                        <div class="team-overlay"></div>
                        <img src="{{ URL::asset('assets/home/team/Diego.jpg') }}" class="img-responsive team-image" alt="image">
                    </div>
                    <div class="entry-social">
                        <span><a href="#"><i class="fa fa-facebook"></i></a></span>
                        <span><a href="#"><i class="fa fa-twitter"></i></a></span>
                        <span><a href="http://www.linkedin.com/in/diego-armando-romero-garcia-0b6167125/"><i class="fa fa-linkedin"></i></a></span>
                        <span><a href="#"><i class="fa fa-tumblr"></i></a></span>
                    </div>
                    <div class="entry-content">
                        <h3 class="entry-title"><a href="http://www.linkedin.com/in/diego-armando-romero-garcia-0b6167125/">Diego Armando Romero Garcia</a></h3>
                        <p class="entry-designation">(Spanish, Portuguese, English) </p>
                        <p class="entry-designation">South America Regional Director</p>
                    </div>
                </div>
            </div>
            <!-- team-single-end -->
        </div>
    </div>
</div>
<!--end-Team-section -->



<!--start-fun-factor-section-->
<!-- <div class="factor-section">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 text-center">
                <div class="single-fact">
                    <div class="icon">
                        <img src="{{ URL::asset('assets/home/images/object1.png') }}" alt="icon">
                    </div>
                    <h2 class="title fact-count">4.9</h2>
                    <h3 class="sub-title">Followers</h3>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 text-center">
                <div class="single-fact">
                    <div class="icon">
                        <img src="{{ URL::asset('assets/home/images/object2.png') }}" alt="icon">
                    </div>
                    <h2 class="title fact-count">80</h2>
                    <h3 class="sub-title">ONLINE CONSULTANTS</h3>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 text-center">
                <div class="single-fact">
                    <div class="icon">
                        <img src="{{ URL::asset('assets/home/images/object3.png') }}" alt="icon">
                    </div>
                    <h2 class="title fact-count">34</h2>
                    <h3 class="sub-title">COUNTRIES SERVED</h3>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 text-center">
                <div class="single-fact">
                    <div class="icon">
                        <img src="{{ URL::asset('assets/home/images/object4.png') }}" alt="icon">
                    </div>
                    <h2 class="title fact-count">2.8</h2>
                    <h3 class="sub-title">MLN BITCOIN WALLETS</h3>
                </div>
            </div>
        </div>
    </div>
</div> -->
<!--end-fun-factor-section-->

<!-- start-security - section -->
@if($news)
<div id="news" class="news-section">
    <div class="container">
        <div class="row row-eq-rs-height">
            <div class="col-md-12 col-sm-12 text-center">
                <div class="section-title section-title-2">
                    <h2 class="title">News</h2>
                    <h4 class="sub-title">Latest News</h4>
                </div>
            </div>
            @foreach($news as $new)
                <div class="col-md-4">
                    <div class="post-module">
                        <div class="thumbnail">
                            <div class="date">
                                <div class="day">{{ $new->created_at->format('d') }}</div>
                                <div class="month">{{ $new->created_at->format('M') }}</div>
                            </div><img src="{{ URL::asset('assets/back/news') }}/{{ $new->img }}" />
                        </div>
                        <div class="post-content">
                            <div class="category">Latest</div>
                            <h1 class="title">{{ $new->title }}</h1>
                            <h2 class="sub_title">{{ $new->sub_title }}</h2>
                            <p class="description">{!! substr($new["content"], 0, 140) !!}</p>
                            <div class="post-meta"><span class="timestamp"><i class="fa fa-clock-"></i> {{ $new->created_at->format('d M y') }}</span></div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="news-btn text-center mt-3">
            <a href="{{ url('news') }}" class="btn btn-white-shadow">Show More</a>
        </div>
    </div>
</div>
@endif


@if($blogs)
<div id="blogs" class="news-section">
    <div class="container">
        <div class="row row-eq-rs-height">
            <div class="col-md-12 col-sm-12 text-center">
                <div class="section-title section-title-2">
                    <h2 class="title">Blogs</h2>
                    <h4 class="sub-title">Latest Blogs</h4>
                </div>
            </div>
            @foreach($blogs as $blog)
                <div class="col-md-4">
                    <div class="post-module">
                        <div class="thumbnail">
                            <div class="date">
                                <div class="day">{{ $blog->created_at->format('d') }}</div>
                                <div class="month">{{ $blog->created_at->format('M') }}</div>
                            </div><img src="{{ URL::asset('assets/back/blog') }}/{{ $blog->img }}" />
                        </div>
                        <div class="post-content">
                            <div class="category">Latest</div>
                            <h1 class="title">{{ $blog->title }}</h1>
                            <h2 class="sub_title">{{ $blog->sub_title }}</h2>
                            <p class="description">{!! substr($blog["content"], 0, 140) !!}</p>
                            <div class="post-meta"><span class="timestamp"><i class="fa fa-clock-"></i> {{ $blog->created_at->format('d M y') }}</span></div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="news-btn text-center mt-3">
            <a href="{{ url('blogs') }}" class="btn btn-white-shadow">Show More</a>
        </div>
    </div>
</div>
@endif
<!-- end -security section -->


<!-- start-security - section -->
<!-- <div id="safety" class="safe-section">
    <div class="container">
        <div class="row row-eq-rs-height">
            <div class="col-md-4 col-sm-6 text-center">
                <div class="safe-factor safe-factor-1">
                    <div class="icon">
                        <img src="{{ URL::asset('assets/home/images/ellipse1.png') }}" alt="icon">
                    </div>
                    <div class="safe-factor-details">
                        <h4 class="title">Security</h4>
                        <p class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore consectetur dolore aliqua.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 text-center">
                <div class="safe-factor safe-factor-2">
                    <div class="icon">
                        <img src="{{ URL::asset('assets/home/images/ellipse2.png') }}" alt="icon">
                    </div>
                    <div class="safe-factor-details">
                        <h4 class="title">License</h4>
                        <p class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore consectetur dolore aliqua.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 text-center">
                <div class="safe-factor">
                    <div class="icon">
                        <img src="{{ URL::asset('assets/home/images/ellipse3.png') }}" alt="icon">
                    </div>
                    <div class="safe-factor-details">
                        <h4 class="title">Result</h4>
                        <p class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore consectetur dolore aliqua.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->
<!-- end -security section -->





<!--start-partner-owl-carousel owl-themesection -->
<div class="partner-section">
    <div class="container">

        <div class="partner-slider owl-carousel">
                        <div class="item">
                            <div class="logo-area">
                                <a href="#"><img src="{{ URL::asset('assets/home/images/Logo-02.png') }}" alt="logo"></a>
                            </div>
                        </div>
                    
                    
                        <div class="item">
                            <div class="logo-area">
                                <a href="#"><img src="{{ URL::asset('assets/home/images/Logo-04.png') }}" alt="logo"></a>
                            </div>
                        </div>
                    
                    
                        <div class="item">
                            <div class="logo-area">
                                <a href="#"><img src="{{ URL::asset('assets/home/images/Logo-03.png') }}" alt="logo"></a>
                            </div>
                        </div>
                    
                        <div class="item">
                            <div class="logo-area">
                                <a href="#"><img src="{{ URL::asset('assets/home/images/Logo-02.png') }}" alt="logo"></a>
                            </div>
                        </div>
                    
                        <div class="item">
                            <div class="logo-area">
                                <a href="#"><img src="{{ URL::asset('assets/home/images/Logo-04.png') }}" alt="logo"></a>
                            </div>
                        </div>
                    
                        <div class="item">
                            <div class="logo-area">
                                <a href="#"><img src="{{ URL::asset('assets/home/images/Logo-01.png') }}" alt="logo"></a>
                            </div>
                        </div>
                    
                        <div class="item">
                            <div class="logo-area">
                                <a href="#"><img src="{{ URL::asset('assets/home/images/Logo-02.png') }}" alt="logo"></a>
                            </div>
                        </div>
                    
                        <div class="item">
                            <div class="logo-area">
                                <a href="#"><img src="{{ URL::asset('assets/home/images/Logo-03.png') }}" alt="logo"></a>
                            </div>
                        </div>
                    
                        <div class="item">
                            <div class="logo-area">
                                <a href="#"><img src="{{ URL::asset('assets/home/images/Logo-04.png') }}" alt="logo"></a>
                            </div>
                        </div>
                    
                        <div class="item">
                            <div class="logo-area">
                                <a href="#"><img src="{{ URL::asset('assets/home/images/Logo-01.png') }}" alt="logo"></a>
                            </div>
                        </div>
                   
                        <div class="item">
                            <div class="logo-area">
                                <a href="#"><img src="{{ URL::asset('assets/home/images/Logo-04.png') }}" alt="logo"></a>
                            </div>
                        </div>
                   
                        <div class="item">
                            <div class="logo-area">
                                <a href="#"><img src="{{ URL::asset('assets/home/images/Logo-01.png') }}" alt="logo"></a>
                            </div>
                        </div>
                    
                        <div class="item">
                            <div class="logo-area">
                                <a href="#"><img src="{{ URL::asset('assets/home/images/Logo-02.png') }}" alt="logo"></a>
                            </div>
                        </div>
                    
                        <div class="item">
                            <div class="logo-area">
                                <a href="#"><img src="{{ URL::asset('assets/home/images/Logo-03.png') }}" alt="logo"></a>
                            </div>
                        </div>
                    
                        <div class="item">
                            <div class="logo-area">
                                <a href="#"><img src="{{ URL::asset('assets/home/images/Logo-04.png') }}" alt="logo"></a>
                            </div>
                        </div>
                   
                        <div class="item">
                            <div class="logo-area">
                                <a href="#"><img src="{{ URL::asset('assets/home/images/Logo-01.png') }}" alt="logo"></a>
                            </div>
                        </div>
                   
            </div>
            <div class="owl-controls">
                <div class="owl-nav">
                    <div class="owl-prev" style="display: none;">prev</div>
                    <div class="owl-next" style="display: none;">next</div>
                </div>
                <div class="owl-dots" style="display: none;">



                </div>
            </div>

                    @php

                    $start_date = strtotime($phase->start_date);
                    
                    $end_date = strtotime($phase->end_date);
                    
                    $date = strtotime(date('Y-m-d H:i'));

                    if($date < $start_date)
                    {
                        $timer = $phase->start_date;
                    }
                    elseif($date > $start_date && $date < $end_date )
                    {
                        $timer = $phase->end_date;
                    }

                     @endphp

        </div>
    </div>
</div>

@endsection

@section('script')
<script>
    const second = 1000,
        minute = second * 60,
        hour = minute * 60,
        day = hour * 24;
         // var date = new Date();
         // var strDate = date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate();
        
        // console.log(date);
        // console.log(strDate);
            var start_date = "{{$phase->start_date}}";
            var end_date = "{{$phase->end_date}}";
        // console.log(end_date);
        // console.log(start_date);
    //let countDown = new Date('09/20/2018 23:59').getTime(),

        let  countDown = new Date('{{date("Y-m-d", strtotime(@$timer))}}').getTime();
        x = setInterval(function() {
            
            let now = new Date().getTime(),

                distance = countDown - now;

                if(distance < 0){
                    document.getElementById('days').innerText = Math.floor(0),
                    document.getElementById('hours').innerText = Math.floor(0),
                    document.getElementById('minutes').innerText = Math.floor(0),
                    document.getElementById('seconds').innerText = Math.floor(0);
                }else{
                    document.getElementById('days').innerText = Math.floor(distance / (day)),
                    document.getElementById('hours').innerText = Math.floor((distance % (day)) / (hour)),
                    document.getElementById('minutes').innerText = Math.floor((distance % (hour)) / (minute)),
                    document.getElementById('seconds').innerText = Math.floor((distance % (minute)) / second);
                }

            //do something later when date is reached


        }, second)
        
</script>

<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}
</script>

<script type="text/javascript">
    $(window).load(function() {
  $('.post-module').hover(function() {
    $(this).find('.description').stop().animate({
      height: "toggle",
      opacity: "toggle"
    }, 300);
  });
});
</script>
@endsection
<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Setting;
use Log;

class CoinRateUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:coinrates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Provide Coins Rates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        Log:info('Cron Running');
        $json = file_get_contents('https://api.coinmarketcap.com/v2/ticker/?limit');
        $coinrates = (array)json_decode($json);

        $setting = Setting::find(1);

        foreach($coinrates['data'] as $coin)
        {
            if($coin->id == 1)
            {
                $setting->btc_rate = $coin->quotes->USD->price;
            }
            elseif($coin->id == 1027)
            {
                $setting->eth_rate = $coin->quotes->USD->price;
            }
            elseif($coin->id == 2)
            {
                $setting->ltc_rate = $coin->quotes->USD->price;
            }
            elseif($coin->id == 1831)
            {
                $setting->bch_rate = $coin->quotes->USD->price;
            }
            elseif($coin->id == 1321)
            {
                $setting->etc_rate = $coin->quotes->USD->price;
            }
        }

        $setting->save();
    }
}

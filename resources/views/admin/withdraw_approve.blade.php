@extends('dashboard_layouts.master')


@section('title') Withdrawal Approve | Vancoin @endsection


@section('content')

<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <h3>Withdrawal Approve
                        <small>Vancoin Admin panel</small>
                    </h3>
                </div>
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Withdrawal Approve</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-xl-6">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <h5>Withdrawal Approve</h5>
                            </div>
                            <form method="post" action="{{ url('withdraw-approval-bank') }}/{{$withdraw->id}}" class="theme-form mega-form">
                                {{ csrf_field() }}
                                <div class="card-body">
                                    <div class="form-group">
                                        <label class="col-form-label">Bank Name : <strong>{{ $withdraw->bank_name }}</strong></label><br>
                                        <label class="col-form-label">Holder Name : <strong>{{ $withdraw->holder_name }}</strong></label><br>
                                        <label class="col-form-label">SWIPT Code : <strong>{{ $withdraw->swipt_code }}</strong></label><br>
                                        <label class="col-form-label">Account No : <strong>{{ $withdraw->account_no }}</strong></label><br>
                                        <label class="col-form-label">Amount : <strong>{{ $withdraw->amount }} USD</strong></label>
                                    </div>
                                    <div class="form-group">
                                        <label  class="col-form-label">Transaction Id</label>
                                        <input type="text" name="transaction_id" class="form-control" placeholder="Enter Transaction Id.">
                                        @if($errors->has('transaction_id'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('transaction_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-primary">Approved</button>
                                    <a href="{{ url('withdraw-manage') }}" class="btn btn-danger">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

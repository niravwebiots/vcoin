@extends('dashboard_layouts.master')


@section('title') KYC | Dashboard @endsection

@section('content')

<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h3>Kyc Document</h3>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item">Dashboard </li>
                        <li class="breadcrumb-item active">Kyc Document</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form method="post" action="{{ url('user-kyc-upload') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                <div class="card">
                    <div class="card-body p-body kyc-body-height">
                        @if(Sentinel::getUser()->kyc_front == "" && Sentinel::getUser()->kyc_back == "" && Sentinel::getUser()->kyc_selfie == "" && Sentinel::getUser()->kyc_status == 0)
                        <div class="alert alert-danger dark alert-dismissible fade show text-center" role="alert">
                            <strong>Pending :</strong>  Your KYC Document is Pending.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        @if(Sentinel::getUser()->kyc_front != "" && Sentinel::getUser()->kyc_back != "" && Sentinel::getUser()->kyc_selfie != "" && Sentinel::getUser()->kyc_status == 3)
                        <div class="alert alert-success dark alert-dismissible fade show text-center" role="alert">
                            <strong>Submited :</strong>  Your KYC Document is Submited. Please Wait for Admin Verification
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        @if(Sentinel::getUser()->kyc_front != "" && Sentinel::getUser()->kyc_back != "" && Sentinel::getUser()->kyc_selfie != "" && Sentinel::getUser()->kyc_status == 1)
                        <div class="alert alert-success dark alert-dismissible fade show text-center" role="alert">
                            <strong>Verified :</strong>  Your KYC Document is Verified by Admin
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        @if(Sentinel::getUser()->kyc_front != "" && Sentinel::getUser()->kyc_back != "" && Sentinel::getUser()->kyc_selfie != "" && Sentinel::getUser()->kyc_status == 2)
                        <div class="alert alert-danger dark alert-dismissible fade show text-center" role="alert">
                            <strong>Submited :</strong>  Your KYC Document is Canceled. Please Upload Valid Proofs.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        <div class="col-lg-4 profile  ">
                           
                            <div class="p-relative">
                                <div class="card-profile custom-css">
                                    @if(Sentinel::getUser()->kyc_front != "")
                                        <img src="{{ url('assets/back/kyc/'.Sentinel::getUser()->kyc_front) }}"  alt="" class="img-fluid" id="front_img">
                                    @else
                                        <img src="{{ URL::asset('/assets/back/demo_kyc/demo.jpg') }}" class="img-fluid"  alt="" id="front_img" />
                                    @endif

                                </div>
                                @if(Sentinel::getUser()->kyc_status == 0 || Sentinel::getUser()->kyc_status == 2)
                                <div class="image-upload">
                                    <label for="kyc_front">
                                        <div class="profile-edit">
                                            <img src="{{ URL::asset('/assets/back/images/edit.png') }}" class="img-fluid" id="front_img">
                                        </div>
                                    </label>
                                    <input id="kyc_front" type="file" accept="image/x-png,image/gif,image/jpeg" class="img-fluid" name="kyc_front">
                                </div>
                                @endif
                            </div>
                            <div class="col-md-12 text-center note-mt-28 note">
                                @if($errors->has('kyc_front'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('kyc_front') }}</strong>
                                    </span>
                                @endif
                            </div>
                            
                        </div>

                        <div class=" col-lg-4 profile  ">
                           
                            <div class="p-relative">
                                <div class="card-profile custom-css">
                                    @if(Sentinel::getUser()->kyc_back != "")
                                        <img src="{{ url('assets/back/kyc/'.Sentinel::getUser()->kyc_back) }}" class="img-fluid"  alt="" id="back_img" />
                                    @else
                                        <img src="{{ URL::asset('/assets/back/demo_kyc/demo.jpg') }}" class="img-fluid"  alt="" id="back_img"/>
                                    @endif
                                </div>
                                @if(Sentinel::getUser()->kyc_status == 0 || Sentinel::getUser()->kyc_status == 2)
                                <div class="image-upload">
                                    <label for="kyc_back">
                                        <div class="profile-edit">
                                            <img src="{{ URL::asset('/assets/back/images/edit.png') }}" class="img-fluid" id="back_img">
                                        </div>
                                    </label>
                                    <input id="kyc_back" type="file" accept="image/x-png,image/gif,image/jpeg"  name="kyc_back">
                                </div>
                                @endif
                            </div>
                            <div class="col-md-12 text-center note-mt-28 note">
                                @if($errors->has('kyc_back'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('kyc_back') }}</strong>
                                    </span>
                                @endif
                            </div>
                         
                        </div>

                        <div class="col-lg-4 profile ">
                            
                            <div class="p-relative">
                                <div class="card-profile custom-css">
                                    @if(Sentinel::getUser()->kyc_selfie != "")
                                        <img src="{{ url('assets/back/kyc/'.Sentinel::getUser()->kyc_selfie) }}"  alt="" id="selfie_img" />
                                    @else
                                        <img src="{{ URL::asset('/assets/back/demo_kyc/demo.jpg') }}" class="img-fluid"  alt="" id="selfie_img" />
                                    @endif
                                </div>
                                @if(Sentinel::getUser()->kyc_status == 0 || Sentinel::getUser()->kyc_status == 2)
                                <div class="image-upload">
                                    <label for="kyc_selfie">
                                        <div class="profile-edit">
                                            <img src="{{ URL::asset('/assets/back/images/edit.png') }}" class="img-fluid">
                                        </div>
                                    </label>
                                    <input id="kyc_selfie" type="file" accept="image/x-png,image/gif,image/jpeg"  class="img-fluid" name="kyc_selfie">
                                </div>
                                @endif
                            </div>
                            <div class="col-md-12 text-center note-mt-28 note">
                                @if($errors->has('kyc_selfie'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('kyc_selfie') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    @if(Sentinel::getUser()->kyc_status == 0 || Sentinel::getUser()->kyc_status == 2)
                    <div class=" col-sm-12 text-center btn-left">
                        <input type="submit" class="btn btn-success btn-lg" name="upload_kyc" value="Submit Kyc"/>
                    </div>
                    @endif
                </div>

                </form>
            </div>

                    
        </div>
    </div>

    
</div>


@endsection


@section('script')

<script type="text/javascript">
    function front(input)
    {
        if (input.files && input.files[0])
        {
            var reader = new FileReader();

            reader.onload = function (e)
                            {
                                $('#front_img').attr('src', e.target.result);
                            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    function back(input)
    {
        if (input.files && input.files[0])
        {
            var reader = new FileReader();

            reader.onload = function (e)
                            {
                                $('#back_img').attr('src', e.target.result);
                            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    function selfie(input)
    {
        if (input.files && input.files[0])
        {
            var reader = new FileReader();

            reader.onload = function (e)
                            {
                                $('#selfie_img').attr('src', e.target.result);
                            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#kyc_front").change(function () {
        front(this);
    });

    $("#kyc_back").change(function () {
        back(this);
    });

    $("#kyc_selfie").change(function () {
        selfie(this);
    });
</script>

@endsection
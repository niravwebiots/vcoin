@extends('dashboard_layouts.master')

@section('title') User KYC | Vancoin @endsection

@section('content')

<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h3>User kyc
                        <!-- <small>Universal Admin panel</small> -->
                    </h3>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                        <li class="breadcrumb-item">User Kyc </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form method="post" action="" enctype="multipart/form-data">
                    {{ csrf_field() }}
                <div class="card">
                    <div class="card-body p-body kyc-body-height">
                        @if($user->kyc_front == "" && $user->kyc_back == "" && $user->kyc_selfie == "" && $user->kyc_status == 0)
                        <div class="alert alert-danger dark alert-dismissible fade show text-center" role="alert">
                            <strong>Pending :</strong>  User KYC Document is Pending.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        @if($user->kyc_front != "" && $user->kyc_back != "" && $user->kyc_selfie != "" && $user->kyc_status == 0)
                        <div class="alert alert-success dark alert-dismissible fade show text-center" role="alert">
                            <strong>Submited :</strong>  User KYC Document is Submited.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        @if($user->kyc_front != "" && $user->kyc_back != "" && $user->kyc_selfie != "" && $user->kyc_status == 1)
                        <div class="alert alert-success dark alert-dismissible fade show text-center" role="alert">
                            <strong>Verified :</strong>  User KYC Document is Verified
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        @if($user->kyc_front != "" && $user->kyc_back != "" && $user->kyc_selfie != "" && $user->kyc_status == 2)
                        <div class="alert alert-danger dark alert-dismissible fade show text-center" role="alert">
                            <strong>Submited :</strong>  User KYC Document is Rejected.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        <div class="col-lg-4 profile">
                            <div class="p-relative">
                                <div class="card-profile custom-css">
                                    @if($user->kyc_front != "")
                                        <img src="{{ url('assets/back/kyc/'.$user->kyc_front) }}"  alt="" />
                                    @else
                                        <img src="{{ URL::asset('/assets/back/images/avtar/3.jpg') }}"  alt="" />
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 profile">
                            <div class="p-relative">
                                <div class="card-profile custom-css">
                                    @if($user->kyc_back != "")
                                        <img src="{{ url('assets/back/kyc/'.$user->kyc_back) }}" class="img-fluid"  alt="" />
                                    @else
                                        <img src="{{ URL::asset('/assets/back/images/avtar/3.jpg') }}" class="img-fluid"  alt="" />
                                    @endif
                                </div>
                            </div>                         
                        </div>

                        <div class="col-lg-4 profile ">
                            <div class="p-relative">
                                <div class="card-profile custom-css">
                                    @if($user->kyc_selfie != "")
                                        <img src="{{ url('assets/back/kyc/'.$user->kyc_selfie) }}"  alt="" />
                                    @else
                                        <img src="{{ URL::asset('/assets/back/images/avtar/3.jpg') }}"  alt="" />
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    @if($user->kyc_status == 3)
                    <div class=" col-sm-12 text-center btn-left">
                        <a href="{{ url('kyc-approve/'.$user->id) }}" class="btn btn-success btn-lg">Approve KYC</a>
                        <a href="{{ url('kyc-reject/'.$user->id) }}" class="btn btn-danger btn-lg">Reject KYC</a>
                    </div>
                    @endif
                </div>

                </form>
            </div>

                    
        </div>
    </div>

    
</div>


@endsection
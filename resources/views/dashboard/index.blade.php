@extends('dashboard_layouts.master')


@section('title') Dashboard  @endsection

@section('content')

<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <h3>dashboard</h3>
                </div>
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item">Dashboard</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->
    <div class="container-fluid">
        <div class="row">

            @if(Sentinel::getUser()->roles()->first()->slug == 'admin')

                <div class="col-sm-6 col-md-6">
                    <div class="card">
                        <div class="card-body" data-intro="This is the name of this site">
                            <div class="stat-widget-dashboard">
                                <div class="media">
                                    <img class="mr-3" src="{{ URL::asset('/assets/back/images/coin/vcoin.png') }}" alt="Generic placeholder image">
                                    <div class="media-body text-right">
                                        <h3 class="mt-0 counter font-secondary">{{ @$settings->total_coins }}</h3>
                                        <h6>Total VNJA Balance</h6>
                                    </div>
                                </div>
                                <div class="dashboard-chart-container">
                                    <div class="flot-chart-placeholder line-chart-sparkline"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-6">
                    <div class="card">
                        <div class="card-body" data-intro="This is the name of this site">
                            <div class="stat-widget-dashboard">
                                <div class="media">
                                    <img class="mr-3" src="{{ URL::asset('/assets/back/images/coin/vcoin.png') }}" alt="Generic placeholder image">
                                    <div class="media-body text-right">
                                        <h3 class="mt-0 counter font-secondary">{{ @$settings->sold_coins }}</h3>
                                        <h6>Total Sold VNJA</h6>
                                    </div>
                                </div>
                                <div class="dashboard-chart-container">
                                    <div class="flot-chart-placeholder line-chart-sparkline"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="card">
                        <div class="card-body" data-intro="This is the name of this site">
                            <div class="stat-widget-dashboard">
                                <div class="media">
                                    <i class="icon-user"></i>
                                    <div class="media-body text-right">
                                        <h3 class="mt-0 counter font-secondary">{{ $users }}</h3>
                                        <h6>Total Users</h6>
                                    </div>
                                </div>
                                <div class="dashboard-chart-container">
                                    <div class="flot-chart-placeholder line-chart-sparkline"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="card">
                        <div class="card-body" data-intro="This is the name of this site">
                            <div class="stat-widget-dashboard">
                                <div class="media">
                                    <i class="icon-user"></i>
                                    <div class="media-body text-right">
                                        <h3 class="mt-0 counter font-secondary">{{ $active_users }}</h3>
                                        <h6>Active Users</h6>
                                    </div>
                                </div>
                                <div class="dashboard-chart-container">
                                    <div class="flot-chart-placeholder line-chart-sparkline"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @if(!empty($total_deposits))

                @foreach($total_deposits as $coin)

                <div class="col-sm-6 col-md-4">
                    <div class="card">
                        <div class="card-body" data-intro="This is the name of this site">
                            <div class="stat-widget-dashboard">
                                <div class="media">
                                    <img class="mr-3" src="{{ URL::asset('/assets/back/images/coin/'.$coin->coin_type.'.png') }}" alt="Generic placeholder image">
                                    <div class="media-body text-right">
                                        <h3 class="mt-0 counter font-secondary">{{ $coin->deposit }} USD</h3>
                                        <h6>Total {{ $coin->coin_type }} Deposit</h6>
                                    </div>
                                </div>
                                <div class="dashboard-chart-container">
                                    <div class="flot-chart-placeholder line-chart-sparkline"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @endforeach

                @endif

            @endif

            @if(Sentinel::getUser()->roles()->first()->slug == 'user')
                <div class="col-sm-6 col-md-3">
                    <div class="card">
                        <div class="card-header"><h6>BTC Price <strong class="pull-right">{{ $settings->btc_rate }} USD</strong></h6>
                        </div>
                        <div class="card-body" data-intro="This is the name of this site">
                            <div class="stat-widget-dashboard">
                                <div class="media">
                                    <img class="mr-3" src="{{ URL::asset('/assets/back/images/coin/btc.png') }}" alt="Generic placeholder image">
                                    <div class="media-body text-right">
                                        <h3 class="mt-0 counter font-secondary">{{ Sentinel::getUser()->btc_balance }} USD</h3>
                                        <h6>Total BTC Balance</h6>
                                    </div>
                                </div>
                                <div class="dashboard-chart-container">
                                    <div class="flot-chart-placeholder line-chart-sparkline"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3">
                    <div class="card">
                        <div class="card-header"><h6>ETH Price <strong class="pull-right">{{ $settings->eth_rate }} USD</strong></h6>
                        </div>
                        <div class="card-body" data-intro="This is the name of this site">
                            <div class="stat-widget-dashboard">
                                <div class="media">
                                    <img class="mr-3" src="{{ URL::asset('/assets/back/images/coin/eth.png') }}" alt="Generic placeholder image">
                                    <div class="media-body text-right">
                                        <h3 class="mt-0 counter font-secondary">{{ Sentinel::getUser()->eth_balance }} USD</h3>
                                        <h6>Total ETH Balance</h6>
                                    </div>
                                </div>
                                <div class="dashboard-chart-container">
                                    <div class="flot-chart-placeholder line-chart-sparkline"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3">
                    <div class="card">
                        <div class="card-header"><h6>LTC Price <strong class="pull-right">{{ $settings->ltc_rate }} USD</strong></h6>
                        </div>
                        <div class="card-body" data-intro="This is the name of this site">
                            <div class="stat-widget-dashboard">
                                <div class="media">
                                    <img class="mr-3" src="{{ URL::asset('/assets/back/images/coin/ltc.png') }}" alt="Generic placeholder image">
                                    <div class="media-body text-right">
                                        <h3 class="mt-0 counter font-secondary">{{ Sentinel::getUser()->ltc_balance }} USD</h3>
                                        <h6>Total LTC Balance</h6>
                                    </div>
                                </div>
                                <div class="dashboard-chart-container">
                                    <div class="flot-chart-placeholder line-chart-sparkline"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3">
                    <div class="card">
                        <div class="card-header"><h6>BCH Price <strong class="pull-right">{{ $settings->bch_rate }} USD</strong></h6>
                        </div>
                        <div class="card-body" data-intro="This is the name of this site">
                            <div class="stat-widget-dashboard">
                                <div class="media">
                                    <img class="mr-3" src="{{ URL::asset('/assets/back/images/coin/bch.png') }}" alt="Generic placeholder image">
                                    <div class="media-body text-right">
                                        <h3 class="mt-0 counter font-secondary">{{ Sentinel::getUser()->bch_balance }} USD</h3>
                                        <h6>Total BCH Balance</h6>
                                    </div>
                                </div>
                                <div class="dashboard-chart-container">
                                    <div class="flot-chart-placeholder line-chart-sparkline"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="card">
                        <div class="card-header"><h6>ETC Price <strong class="pull-right">{{ $settings->etc_rate }} USD</strong></h6>
                        </div>
                        <div class="card-body" data-intro="This is the name of this site">
                            <div class="stat-widget-dashboard">
                                <div class="media">
                                    <img class="mr-3" src="{{ URL::asset('/assets/back/images/coin/etc.png') }}" alt="Generic placeholder image">
                                    <div class="media-body text-right">
                                        <h3 class="mt-0 counter font-secondary">{{ Sentinel::getUser()->etc_balance }} USD</h3>
                                        <h6>Total ETC Balance</h6>
                                    </div>
                                </div>
                                <div class="dashboard-chart-container">
                                    <div class="flot-chart-placeholder line-chart-sparkline"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="card">
                        <div class="card-header"><h6>VNJA Balance </h6>
                        </div>
                        <div class="card-body" data-intro="This is the name of this site">
                            <div class="stat-widget-dashboard">
                                <div class="media">
                                    <img class="mr-3" src="{{ URL::asset('/assets/home/images/favicon.png') }}" alt="Generic placeholder image">
                                    <div class="media-body text-right">
                                        <h3 class="mt-0 counter font-secondary">{{ Sentinel::getUser()->wallet_balance / $settings->coin_rate }} USD</h3>
                                        <h6>Total VNJA Balance</h6>
                                    </div>
                                </div>
                                <div class="dashboard-chart-container">
                                    <div class="flot-chart-placeholder line-chart-sparkline"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="card">
                        <div class="card-header"><h6>Coin Rate</h6>
                        </div>
                        <div class="card-body" data-intro="This is the name of this site">
                            <div class="stat-widget-dashboard">
                                <div class="media">
                                    <img class="mr-3" src="{{ URL::asset('/assets/home/images/favicon.png') }}" alt="Generic placeholder image">
                                    <div class="media-body text-right">
                                        <h3 class="mt-0 counter font-secondary">{{ $settings->coin_rate }} USD</h3>
                                        <h6>Coin Rate</h6>
                                    </div>
                                </div>
                                <div class="dashboard-chart-container">
                                    <div class="flot-chart-placeholder line-chart-sparkline"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- <div class="col-sm-6 col-md-6">
                    <div class="card">
                        <div class="card-header text-center"><h6><img class="mr-3" style="width: 35px;" src="{{ URL::asset('/assets/back/images/coin/gift.png') }}" alt="Generic placeholder image"> Referral Bonus</h6>
                        </div>
                        <div class="card-body" data-intro="This is the name of this site">
                            <div class="stat-widget-dashboard">
                                <div class="media">
                                    
                                    <div class="media-body text-center">
                                        <h3 class="mt-0 counter font-secondary">{{ $settings->referral_bonus }} %</h3>
                                        <h6>Referral Bonus</h6>
                                    </div>
                                </div>
                                <div class="dashboard-chart-container">
                                    <div class="flot-chart-placeholder line-chart-sparkline"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                <div class="col-sm-6 col-md-6">
                    <div class="card">
                        <div class="card-header text-center"><h6><img class="mr-3" style="width: 35px;" src="{{ URL::asset('/assets/back/images/coin/gift.png') }}" alt="Generic placeholder image"> Token Bonus</h6>
                        </div>
                        <div class="card-body" data-intro="This is the name of this site">
                            <div class="stat-widget-dashboard">
                                <div class="media">
                                    
                                    <div class="media-body text-center">
                                        <h3 class="mt-0 counter font-secondary">{{ $settings->bonus }} %</h3>
                                        <h6>Token Bonus</h6>
                                    </div>
                                </div>
                                <div class="dashboard-chart-container">
                                    <div class="flot-chart-placeholder line-chart-sparkline"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-6">
                    <div class="card">
                        <div class="card-header text-center"><h6><img class="mr-3" style="width: 35px;" src="{{ URL::asset('/assets/home/images/favicon.png') }}" alt="Generic placeholder image"> Total VNJA Coins</h6>
                        </div>
                        <div class="card-body" data-intro="This is the name of this site">
                            <div class="stat-widget-dashboard">
                                <div class="media">
                                    
                                    <div class="media-body text-center">
                                        <h3 class="mt-0 counter font-secondary">{{ $settings->total_coins }}</h3>
                                        <h6>Coins</h6>
                                    </div>
                                </div>
                                <div class="dashboard-chart-container">
                                    <div class="flot-chart-placeholder line-chart-sparkline"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            @if(Sentinel::getUser()->roles()->first()->slug == 'admin')
            <div class="col-sm-12">

            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>User Transaction</h5>
                    </div>
                    <div class="card-body table-responsive" data-intro="This is the name of this site">
                        <div class="user-status">
                            <table id="dashboard-table" class="display" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>User Name</th>
                                        <th>Tokens</th>
                                        <th>Coin</th>
                                        <th>Price(USD)</th>
                                        <th>Bonus</th>
                                        <th>Status</th>
                                        <th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php($i=1)
                                @foreach($buy_history as $h)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{ $h->buytoken_user->user_name }}</td>
                                        <td>{{ $h->tokens }}</td>
                                        <td>{{ $h->coin_type }}</td>
                                        <td>{{ $h->amount }}</td>
                                        <td>{{ $h->bonus }}</td>
                                        <td>
                                            @if($h->status == 0)<span class="text-warning"><i class = "fa fa-spinner fa-spin"></i><b> Pending </b></span>
                                            @elseif($h->status == 1)<span class="text-success"><i class="fa fa-check" aria-hidden="true"></i><b> Completed </b></span>
                                            @elseif($h->status == 2)<span class="text-danger"><i class="fa fa-close" aria-hidden="true"></i><b> Cancelled </b></span>
                                            @endif
                                        </td>
                                        <td><?php $date = $h->created_at;
                                           echo $newDate = date("d-m-Y", strtotime($date));  ?></td>
                                    </tr>
                                @php($i++)
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
           

            </div>
             @endif

        </div>
    </div>
</div>

@endsection

@section('script')
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#dashboard-table').DataTable();
} );
</script>
@endsection
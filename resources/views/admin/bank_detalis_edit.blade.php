@extends('dashboard_layouts.master')

@section('title') Bank Details | Vancoin @endsection

@section('style')
<style type="text/css" media="screen">
.error {
    margin: 0px!important;
    color: #ff2b2b!important;
}
p {
    font-size: 16px!important;
}   
</style>
@endsection

@section('content')

<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <h3>Bank Details</h3>
                </div>
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item">Dashboard</li>
                        <li class="breadcrumb-item active">Bank Details</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">  
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header p-3">
                        <h5 class="card-title">Bank Details</h5>
                    </div>
                    <div class="card-body buy-token">
                        <form class="form-horizontal theme-form row" id="W-form" action="{{ route('bank-details.update',$bank_details->id) }}" method="post">

                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            <div class="form-group col-md-12">
                                <label for="paypal_token">Bank Name</label>
                                <input type="text" name="bank_name" class="form-control" autocomplete="off" placeholder="Enter Bank Name" value="{{ $bank_details->name }}">
                                @if($errors->has('bank_name'))
                                    <span class="text-danger">{{$errors->first('bank_name')}}</span>
                                @endif
                            </div>
                            <div class="form-group col-md-12">
                                <label for="paypal_token">Account No</label>
                                <input type="text" name="account_no" class="form-control" value="{{ $bank_details->account_no }}" autocomplete="off" placeholder="Enter Account No" maxlength="25">
                                @if($errors->has('account_no'))
                                    <span class="text-danger">{{$errors->first('account_no')}}</span>
                                @endif
                                <br>
                                <span class="max-min-tokens"></span>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="currency_price">SWIPT Code</label>
                                <input type="text" name="swipt_code" class="form-control" value="{{ $bank_details->swipt_code }}" id="swipt_code" placeholder="Enter SWIPT Code">
                                @if($errors->has('swipt_code'))
                                    <span class="text-danger">{{$errors->first('swipt_code')}}</span>
                                @endif
                            </div>
                            <div class="media">
                                <label class="col-form-label m-r-10">Bank Details Active</label>
                                <div class="media-body text-right">
                                    <label class="switch">
                                        <input type="hidden" name="status" value="off">
                                        @if($bank_details->status == 1)
                                        <input type="checkbox" checked="" name="status" valus="on">
                                        @else
                                        <input type="checkbox" name="status" valus="off">
                                        @endif
                                        <span class="switch-state"></span>
                                    </label>
                                </div>
                            </div>
                            <!-- <div class="form-group col-md-12">
                                <p class="text-info"><b>Note:</b> Please give correct transaction id details.</p>
                            </div> -->
                            <div class="form-group col-md-12 text-right">
                                <button type="submit" class="btn btn-success mt-4" id="paypal-btn">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection

@section('script')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
// just for the demos, avoids form submit

$( "#W-form" ).validate({
  rules: {
    account_no: {
      required: true,
      digits: true
    }
  }
});
</script>

@endsection
@extends('dashboard_layouts.master')


@section('title') News Manage | Vancoin @endsection

@section('content')
<div class="page-body">
<!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <h3>Edit News
                        <!-- <small>Vancoin Admin panel</small> -->
                    </h3>
                </div>
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Edit News</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Edit News</h5>
                    </div>
                  <form method="POST" action="{{ route('news-manage.update',$news->id) }}" class="theme-form mega-form" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                        <div class="card-body">
                            <div class="form-group">
                                <label class="col-form-label">Title</label>
                                <input type="text" name="title" class="form-control" value="{{ $news->title }}" placeholder="Enter News Title..." >
                                @if($errors->has('title'))
                                <span class="text-danger">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label  class="col-form-label">Sub Title</label>
                                <input type="text" name="sub_title" class="form-control" value="{{ $news->sub_title }}" placeholder="Enter News Sub Title...">
                                @if($errors->has('sub_title'))
                                <span class="text-danger">
                                    <strong>{{ $errors->first('sub_title') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="col-form-label">Content</label>
                                <textarea name="content" class="form-control" id="editor1" cols="20" rows="15" value="{{ $news->content }}">{{ $news->content }}</textarea>
                                @if($errors->has('content'))
                                <span class="text-danger">
                                    <strong>{{ $errors->first('content') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="col-form-label">Image</label>
                                <img src="{{ URL::asset('assets/back/news') }}/{{ $news->img }}" alt="" height="100px" width="100px">
                                <input type="file" name="img">
                            </div>
                            <div class="form-group">
                                <label class="col-form-label">Link</label>
                                <input type="text" name="link" class="form-control" value="{{ $news->link }}">
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-primary">Submit</button>
                            <a href="{{ url('news-manage') }}" class="btn btn-danger">Cancel</a>
                        </div>
                    </form>
                </div>                    
            </div>
        </div>
    </div>

 </div>

@endsection

@section('script')
<script src="{{ URL::asset('assets/back/js/ckeditor/ckeditor.js') }}"></script>
<script src="{{ URL::asset('assets/back/js/ckeditor/styles.js') }}"></script>
<script src="{{ URL::asset('assets/back/js/ckeditor/adapters/jquery.js') }}"></script>
<script src="{{ URL::asset('assets/back/js/ckeditor/ckeditor.custom.js') }}"></script>
@endsection